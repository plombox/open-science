# PlomBox

## Contents

- Box3DPrinted: 3D printer STL files for the box
- biology: .docx file with information on the biological part
- firmware: ESP-32 firmware code and python operation code
- led_ring: PCB for the LED ring operation
- paper_data_and_code: Example data files, code, and documentation which allows to analyse data from the PlomBox
- pcb: main PCB for PlomBox
- plomapp: code of (and additional information on) the PlomApp, the phone application to control the PlomBox data acquisition


