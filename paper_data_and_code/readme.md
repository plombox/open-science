# Contents

[[_TOC_]]

# Code Explanation

## PlomBox data analysis

### Step 0: Requirements

The following packages have been used to test the PlomBox data analysis code:

    matplotlib==3.9.2
    mpmath==1.3.0
    numpy==2.1.1
    pandas==2.2.2
    Pillow==10.4.0
    opencv-python==4.10.0.84
    pyserial==3.5
    qrcode==7.4.2
    scipy==1.14.1
    serial==0.0.97
    scikit-image==0.24.0
    sympy==1.13.2

They, as well as their dependencies, are needed.


### Step 1: Checking HSV data analysis script works

For the analysis we mainly need the `esp32Serial.py` script in the `code` directory. All examples of commands assume that you are in the same directory as this very readme. For a quick check run:

```bash
python code/esp32Serial.py --help
```

Which should print the documentation of the `esp32Serial.py` analysis macro. To check that the analysis is actually working, we actually need to analyse an image, which we will do in the next section. 

If you take the time to go through the full documentation, you will notice that it has several options related to the PlomBOX data taking, and (if you read further) more analysis options, which we did not all use for the paper. Here, we will only explain what is relevant for the data analysis presented in the paper:

The `esp32Serial.py` can be used to retrieve RGB and HSV information from an image. To do so, the image needs to have a QR code, so have a QR code next to the samples when you take a photo, or add a QR code onto the image on MS Paint or a similar software. 


### Step 2: Analysis of one image

To analyse the image, you use the following command (a working example follows after the explanation):

`python SCRIPT_DIRECTORY/esp32Serial.py -a --QR -o IMAGE_DIRECTORY/final-flatfielded_00.jpg --shift 2.2 0.5 --shift 2.05 1.1 --draw-axes -R 0.07 --save-jpg final_flatfielded_analysed_00.jpg`

Please make sure you insert the correct file location for the python script (`SCRIPT_DIRECTORY`) and the images (`IMAGE_DIRECTORY`) to be analysed. The part `--shift 2.2 0.5` adds a region of interest (ROI) at those x/y coordinates and can be manually adjusted until the ROI matches the sample location on the photo. As many `--shift x y` ROIs can be added as necessary. `x` and `y` are in QR code coordinate system, where the numbers are in units given by the length of the QR code.  The `-R 0.07` part indicates the size of the ROI square. `--save-jpg final_flatfielded_analysed_00.jpg` ensures that the image with ROIs is saved on the same directory as the original image. The name of the analysed image needs to be changed manually. 

The command returns a table in the command line with the RGB, HSV and uncertainty values for each ROI in the image. 

After this general example, now for some specific: The step below will perform the same analysis (but for different images) as has been used to create **Figure A1** and **Table A1** in the paper. 

```bash
python code/esp32Serial.py -a --QR -o data/20221201_flat-fielded_data/final-flatfielded_50.jpg --shift -1.1 1.20 --shift -1.2 0.50 --shift -1.1 -0.15 --shift -0.65 -0.65 --shift -0.1 -0.95 --shift 0.5 -1.2 --shift 1.1 -1.0 --shift 1.7 -0.7 --shift 2. -0.15 --shift 2.2 0.5 --shift 2.05 1.1 --shift 1.65 1.6 --shift 1.15 2 --shift 0.50 2.2 --shift -0.15 2.1  --shift -0.7 1.7 --draw-axes -R 0.07 --save-jpg final_flatfielded_analysed_50.jpg --no-show
```

The command returns a table with RGB and HSV results. The output on the terminal should look like this:

```bash
#rNum	red	green	blue	intens	rStd	gStd	bStd	rMix	gMix	bMix	H	S	V	hMix	SMix	vMix	hStd	sStd	vStd
0	123.07	124.60	111.85	207.80	0.24	0.33	0.43	116-132	114-134	102-122	67.22	10.23	48.86	33-120	4-18	47-53	2.27	0.43	0.13
0.0 1.5512716364188752 0.0
1	107.11	121.80	111.64	196.91	0.22	0.22	0.48	99-114	116-127	101-122	138.50	12.06	47.77	103-360	7-19	45-50	1.55	0.25	0.08
0.0 1.1545461259502852 0.0
2	109.24	122.85	112.51	199.21	0.26	0.30	0.24	102-119	115-132	106-121	134.38	11.08	48.18	84-360	4-19	45-52	1.15	0.33	0.12
0.0 1.7474592381947378 0.0
3	108.21	126.30	116.47	203.04	0.48	0.35	0.50	100-118	116-134	102-130	147.37	14.32	49.53	103-360	8-21	45-53	1.75	0.47	0.14
0.0 1.3209468107255393 0.0
4	109.56	129.36	122.70	209.27	0.34	0.26	0.42	101-117	123-136	110-131	159.82	15.30	50.73	124-360	8-22	48-53	1.32	0.33	0.10
0.0 1.3883188375686801 0.0
```

But with the results of 15 ROIs. In addition you should now see a file called `final_flatfielded_analysed_50.jpg` in the directory where you have run the above command. It should look like this:

![Analysis result of the `python code/esp32Serial.py` detailed in the example, Step 1.](documentation_material/HSV_analysis_check_sample_holder_images/20221201_final_flatfielded_analysed_50.jpg){width=600px}

Add and adjust the ROIs, which are given by the `--shift` commands, as you see fit. I propose that you add ROIs at the top of the QR code and then move clockwise around the sample holder. You can use the images shown in the `documentation_material/HSV_analysis_check_sample_holder_images` directory as a guide. The relevant figure for the data in `data/20221201_flat-fielded_data/` is the following:

![Explanation of the content of each well for the data in `data/20221201_flat-fielded_data/`](documentation_material/HSV_analysis_check_sample_holder_images/01122022.png){width=600px}


### Step 3: Analyse a full set of images

We propose you run the analysis on the data shown on everything in the `data/20221201_flat-fielded_data` folder. You cannot run the above command for multiple images, it only analyses one image at a time. If `bash` is your friend, you may do something like this:

```bash
IMAGE_COUNTER=0
TIME=40

echo "time	imageID	#rNum	red	green	blue	intens	rStd	gStd	bStd	rMix	gMix	bMix	H	S	V	hMix	SMix	vMix	hStd	sStd	vStd"

for file in data/20221201_flat-fielded_data/*.jpg; do 
	if [ -f "$file" ]; then 
		python3 code/esp32Serial.py -a --QR -o $file --shift -0.7 1.7 --shift -0.15 2.1  --shift 0.50 2.2 --shift 1.15 2 --shift 1.65 1.6 --shift 2.05 1.1 --shift 2.2 0.5 --shift 2. -0.15 --shift 1.7 -0.7 --shift 1.1 -1.0 --shift 0.5 -1.2 --shift -0.1 -0.95 --shift -0.65 -0.65 --shift -1.1 -0.15 --shift -1.2 0.50 --shift -1.1 1.20 --draw-axes -R 0.07 --no-show | tail -n +2 | awk 'NR % 2 == 0 {print $0}' | awk -v a="$TIME\t$IMAGE_COUNTER\t" '{print a,$0}'
		let IMAGE_COUNTER=IMAGE_COUNTER+1
		let TIME=TIME+20
	fi 
done
```

When piping this to a file and comparing the resulting table with the information shown on the `data/data-used-for-publication/20221201_INA_Assay_lead_curve.csv` and the `data/data-used-for-publication/20221201_INA_Assay_samples.csv` files, you should find the same results. ROI 6 to 14 correspond to "#82" to "#90" in the `data/data-used-for-publication/20221201_INA_Assay_samples.csv` file, and ROI "0" to "5" correspond to "0" and "100" in `data/data-used-for-publication/20221201_INA_Assay_lead_curve.csv` as you can see from `documentation_material/HSV_analysis_check_sample_holder_images/01122022.png` which was shown above.


## Generating the plots inside the publication

Generating the plots for the paper (and the corresponding analysis) is the next step. As we do not use the `esp32Serial.py` macro here any-more, we granted "**Step 4**" its independent section. Note that the macros here are specialised and need specific input files as stated below.

### `code/14122022_INA_analysis_LS_version.py` 

Produces Figure 2 on the paper. It uses the `data/data-used-for-publication/20221214_INA_Assay_samples.csv` file to obtain the **timelapse fit** of a sample

### `code/01122022_INA_analysis_LS_version.py`

Produces Figure 3 on the paper. It uses the `data/data-used-for-publication/20221201_INA_Assay_samples.csv` and the `data/data-used-for-publication/20221201_INA_Assay_lead_curve.csv` files to obtain the **timelapse fits of all elements of the lead curve and all samples**. Following this, it uses the saturation values returned from the timelapse fits to **produce a lead curve**, from which the **lead concentrations of the samples can be inferred**. 

### `code/real_colour_vs_obtained_colour.py`

Produces Figure A2 on the paper. It uses data from `data/data-used-for-publication/16122022_INA_Assay_samples.csv` and `data/data-used-for-publication/16122022_INA_Assay_lead_curve.csv` to **give an example of how measured saturation is converted to digital saturation**


# Data Explanation

* `data/20221201_flat-fielded_data` and `data/20221214_flat-fielded_data` - these folders contain all the raw data used to produce Figures 2 and 3 of the paper. These are images of the sample holder, inside the PlomBOX, obtained every 20 minutes during the timelapse. The `code/esp32Serial.py` is used to retrieve the saturation values for each well in the sample holder. These saturation values are then used in the `code/01122022_INA_analysis_LS_version.py` and `code/14122022_INA_analysis_LS_version.py` scripts, respectively, to obtain a timelapse fit and a lead curve fit. 

* `HSV_analysis_check_sample_holder_images` - this folder contains two images detailing the locations and concentrations of the samples analysed on the 01/12/2022 and 14/12/2022.

* `data/data-used-for-publication` - this folder contains all the data, in its final form, that was used in the scripts highlighted in the **Code Explanation** section, to obtain the Figures and Tables presented on the paper:

    * `04042024_2200minutes_saturation.csv` and `04042024_2200minutes_fit_parameters.csv` - miliQ water Saturation data used to obtain the confidence intervals shown in Figure 4 and Table A2. 

    * `16122022_INA_Assay_lead_curve.csv` and `16122022_INA_Assay_samples.csv` - Saturation data used to conduct the exercise shown in Figure A2, of converting measured Saturation inside the PlomBOX to digital Saturation, which can than be used in the timelapse fit and lead curve analysis. 

    * `20221214_INA_Assay_samples.csv`, `20221201_INA_Assay_samples.csv` and `20221201_INA_Assay_lead_curve.csv` - spreadsheets with final saturation values obtained using the `esp32Serial.py` script on the raw data from the `20221201_flat-fielded_data` and `20221214_flat-fielded_data` folders, and after calibration. 

    * `INA_AAS_data_results.xlsx` simple tabulation of the INA AAS values used in Figure 4

    * `confidence_intervals_deionised.xlsx` - Table detailing the confidence interval results for the deionised water samples, which are then shown in Figure 4 and Table A3 

    * `confidence_intervals_milliQ_1.csv` and `confidence_intervals_milliQ_2.csv` - Data used to infer the confidence intervals for the milliQ water samples, which are then shown in Figure 4 and Table A2. 

    * `correlation_calculations.xlsx` - Table summarising the correlation between the PlomBOX and INA lead concentrations, which are shown in Table A4

    * `real_colour_vs_obtained_colour.csv` - Standardised table used to convert measured PlomBOX saturation values to digital saturation values. This is used to obtain Figure A2.
