import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.odr import * # ORD fit
from scipy.optimize import least_squares # least squares fit
from itertools import product # used to build up error-bands
from mpmath import mp
from scipy import signal
from plombox_analysis_functions import reduced_chi_square, get_cmap_and_fonts, \
                                       fullSigmoid, error_prop_timelapse_no_offset_sigmoid

#editing fonts
size_of_font = 7
cmap, CB_color_cycle, font1, font2 = get_cmap_and_fonts(size_of_font = size_of_font)


def timelapse_fit(variable,variable_error, date, method, curve_type): 
    if(curve_type == 'curve'):
        curve_type_values = lead_curve
    else:
        curve_type_values = sample_curve
    
    for ppb in curve_type_values:    
        if(method == 3 and curve_type == "sample"):
            df1_roi = df5.query("ROI == " + str(ppb))
        if(method == 4 and curve_type == "curve"):
            df1_roi = df4.query("ROI == " + str(ppb))        
        axis_header = "Saturation"
 
        #determining y axis for fit
        y=np.array(df1_roi[variable], dtype=float)
        ye= np.array(df1_roi[variable_error], dtype=float)
        time = np.array(df1_roi['time'], dtype=float)
       
        len_y = np.shape(y)[0] #length of y array
        #time for curve with real data
        timeMin = 0
        timeMax = data_time_max #990 for 10th, 16th, 1050 for 30th, 1070 for 1st and 7th, 1150 for 14th
        
        x=np.array(np.arange(timeMin, timeMax, (timeMax-timeMin)/len_y), dtype=float)
       

        #do the fit

        ###########
        # least squares ->
        ###########
      
        # fit function:
        # we just use your fullSigmoid(...)

        # The cost function: Most other fit engines do the cost function "under the
        # hood". Usually it is something like . Where c would be 
        # the cost function, x,y are the data you give it and f(x) the function you
        # would like to fit. The fit engine then minimizes c(x,y). For 
        # least_squares we build this ourselves


        # Define the cost function. 
        # The most simple case would be:
        #    costFunction = lambda p, x, y: y - fullSigmoid(p, x)
        # However, we would like now some parameter constraints
        
        # "a" constraint: only positive values between 0 and 500.
        p0Constraint = lambda p: 1 if p > 0. and p < 600. else 1E+6
        # "b" constraint: lets allow this shift to be only between 200 and 1200
        p1Constraint = lambda p: 1 if p > 200. and p < 1200. else 1E+6

        # cost function:
        costFunction = lambda p, x, y, ye: (y - fullSigmoid(p, x) * p0Constraint(p[0]) * p1Constraint(p[1]))/ye

        # as the fitter tries to minimise the cost function, our constrain
        # functions will lead to the cost function exploding, as soon as the 
        # parameters go out of bounds


        # using your start parameters:
        startValues = [ 50., 980., 0.004 ]


        # The actual fit:
        # (Right now I do not remember how to include the y-errors. That could 
        # profit from some binging)
        leastSquareFitResult = least_squares(costFunction, startValues, 
                                             args=(x, y, ye), ftol=1e-8, xtol=1e-8, 
                                             gtol=1e-8, max_nfev=5000)
        
        #covariance matrix calculation
        cov = np.linalg.inv(leastSquareFitResult.jac.transpose().dot(leastSquareFitResult.jac))
        final_cov = cov * reduced_chi_square(y, fullSigmoid(leastSquareFitResult.x, time), ye, len(y), len(leastSquareFitResult.x))
       
        # print the full result:
        # similar print out as for ODR:
        print(str(ppb))
        print(leastSquareFitResult.x[0], leastSquareFitResult.x[1], 
              leastSquareFitResult.x[2])
        print(np.sqrt(np.diag(final_cov)))
        # Have a look at the output - I guess you'll need to have a yahoo search to
        # get the cov matrix from the Jacobian or such. But this is a thing chatGPT
        # should know how to do....          
        ###########
        # <- least squares
        ###########


    
       
        #prepare fit to be plotted
        xModel = np.linspace(min(x), max(x))
        yModel_LS = np.array(fullSigmoid(leastSquareFitResult.x, xModel), dtype = float)
                       
        #error propagation for least squares
        error_LS = error_prop_timelapse_no_offset_sigmoid(final_cov, xModel,leastSquareFitResult.x ,variable) #extension

        #least squares error bands
        fit_up_ls = yModel_LS + error_LS
        fit_dw_ls = yModel_LS - error_LS
  
        #STORAGE FOR NORMAL TIMELAPSE RANGE
        #store values in arrays to use later
        for i in range(0, len(yModel_LS)):
            if i == (len(yModel_LS)-1):
                final_value_y_ls = yModel_LS[i]        
        for i in range(0, len(xModel)):
            if i == (len(xModel)-1):
                final_value_x = xModel[i]
      
  
        #final error for saturation
        final_value_y_error_ls = error_prop_timelapse_no_offset_sigmoid(final_cov, final_value_x, leastSquareFitResult.x,"saturation")
      
        #least_squares
        if(curve_type == "curve"):
            lead_curve_saturation.append(final_value_y_ls)
            lead_curve_saturation_e.append(float(final_value_y_error_ls))
        else:
            lead_interpolated_value.append(final_value_y_ls)
            lead_interpolated_value_error.append(float(final_value_y_error_ls))
        
        print("final chi square")
        print(reduced_chi_square(y, fullSigmoid(leastSquareFitResult.x, time), ye, len(y), len(leastSquareFitResult.x)))
        
        t = np.array(df1_roi['time'])
        s = np.array(df1_roi[variable])
        s_std = np.array(df1_roi[variable_error])
        
        #plot data
        plt.errorbar(df1_roi['time'], df1_roi[variable], df1_roi[variable_error], color = 'black', fmt='.', markersize=5, label = "Data")
        #plt.errorbar(t[0], s[0],s_std[0], color = 'aqua', fmt='.', markersize=5)
        
        #two sigmoids - normal timelapse
        #plt.errorbar(xModel, np.array(fullSigmoid(leastSquareFitResult.x, xModel), dtype = float),  None, None, fmt=':', color = CB_color_cycle[1], label  = 'Chi-square = ' + str(round(reduced_chi_square(y, fullSigmoid(leastSquareFitResult.x, time), ye, len(y), len(leastSquareFitResult.x)), 2))) # fit
        plt.errorbar(xModel, np.array(fullSigmoid(leastSquareFitResult.x, xModel), dtype = float),  None, None, fmt=':', color = CB_color_cycle[1], label  = 'Data Fit') # fit
          
        
        # plot the error bands
        plt.fill_between(xModel, fit_up_ls, fit_dw_ls, color = CB_color_cycle[0], alpha = 0.3, label = "1\u03C3 error band")

        plt.xlabel('t (minutes)', fontname = 'Arial', fontsize = size_of_font)
        plt.ylabel(axis_header, fontname = 'Arial', fontsize = size_of_font)
        plt.legend(prop=font1,loc='best')
        plt.tick_params(axis='both', which='major', labelsize=size_of_font)
        
        if(curve_type == "curve"):
            plt.title(date +' ' + axis_header +' over Time - ' + str(ppb) + " ppb")
            #plt.savefig(date+ '_' + axis_header +'_timelapse_fit_'+ str(ppb) +'ppb', dpi = 1200)
           
        else:
            #plt.title(date +' ' + axis_header +' over Time - sample ' + str(ppb))
            plt.savefig(date+ '_' + axis_header +'_timelapse_fit_'+'_sample_'+str(ppb)+'_sub_paper', dpi = 350)
        plt.clf() 


if __name__ == '__main__':
    fig = plt.figure()
    date = "20221214"
    data_time_max = 1150

    df5 = pd.read_csv('../data/data-used-for-publication/{}_INA_Assay_samples.csv'.format(date)) #file with shv and rgb data 

    #INITIAL VARIABLES

    #for lead curve
    lead_curve = [0,10,15,20,50,100]
    sample_curve = [103]

    lead_curve_saturation = []
    lead_curve_saturation_e = []
    lead_interpolated_value = []
    lead_interpolated_value_error = []

    #ARRAYS FOR SUB VALUES
    final_lead_curve = []
    final_lead_curve_e = []
    reduced_chi = 0.0

    lead_curve_saturation = []
    lead_curve_saturation_e = []
    lead_interpolated_value = []
    lead_interpolated_value_error = []
    #timelapse_fit('S_sub', 'sStd_sub', date,4,"curve")
    timelapse_fit('S_sub', 'sStd_sub', date,3,"sample")
    #timelapse_fit('S_led', 'sStd_led', date,3,"sample")
