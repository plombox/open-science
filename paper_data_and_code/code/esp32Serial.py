#!/usr/bin/python3
from matplotlib.pyplot import flag
import serial
import time
import json
import ast #as literal eval
import sys
from myLibs.redMisc import *
from myLibs.serMisc import *
from myLibs.parsing import *
from myLibs.helpFunct import *
from myLibs.colorDeconv import *

def main(argv):
    myOptDict = par_getMyOptDict(argv)

    #Making sure the preferred option form is in the dictionary
    myOptDict = par_getPrefOptDict(myOptDict, preAccOpts)

    if '-h' in myOptDict or len(myOptDict) == 0:
        printHelp(argv)
        return

    #Note: accOpts is inside parsing.py
    if not par_checkIfValidOpts(myOptDict, accOpts):
        printHelp(argv)
        return

    myOptDict = par_pOD(myOptDict)

    rgb_from_cus=whatIsTheMatrix(myOptDict)

    if '--S1' in myOptDict:
        dList=np.array([255,255,255])
        if '-D' in myOptDict:
            dList=np.array(myOptDict['-D'])
        s1RGB=np.array(myOptDict['--S1'])
        s2RGB=np.array(myOptDict['--S2'])
        s3RGB=np.array(myOptDict['--S3'])
        deconvMatrix=getDeconvMatrix(s1RGB,s2RGB,s3RGB,dList)

        outTsv='matrix.tsv'
        if '-O' in myOptDict:
            outTsv = myOptDict['-O'][0]
        np.savetxt(outTsv, deconvMatrix)
        print("#saved the matrix in \"%s\"" %(outTsv))

        sys.exit()

    if '--average' in myOptDict:
        np_img = rM_getAveImgArr(myOptDict['--average'])
        out=Image.fromarray(np_img,mode="RGB")

    if '-p' in myOptDict or '-c' in myOptDict:
        port = '/dev/ttyUSB0'
        baudrate = 115200
        timeout = 2

        if '-p' in myOptDict:
            port = myOptDict['-p']

        if '-b' in myOptDict:
            baudrate = myOptDict['-b']

        if '-t' in myOptDict:
            timeout = myOptDict['-t']

        print("The serial communication")

        ser = serial.Serial(port, baudrate, timeout = timeout)

    if '-c' in myOptDict:
        myStr = myOptDict['-c'][0]
        ser.write(myStr.encode())
        printAll = False
        if '--print-raw' in myOptDict:
            printAll=True
        jsonStr=sm_getStrJsonSerial(ser, sleep = 10,  printAll=printAll)
        #Update this please
        if is_json(jsonStr):
            print("There is something to be saved")
            jsonObject = json.loads(jsonStr) #Trailing strings will be truncated?
            if '--print-json' in myOptDict:
                print(jsonObject)
            if '--print-json-p' in myOptDict:
                for e in jsonObject:
                    print(e, jsonObject[e])
            if '-a' in myOptDict:
                jsonObj = json.loads(jsonStr)
                np_img = rM_getImgFromJson(jsonObj)
            if '-s' in myOptDict:
                fName = myOptDict['-s']
                print("Saving the data")
                with open(fName, 'w') as f:
                    json.dump(jsonObject, f)

    if '-o' in myOptDict:
        myFile = myOptDict['-o'][0]
        np_img=rM_getNpFromFile(myFile, myOptDict)

    if '--refImg' in myOptDict:
        myRefFile = myOptDict['--refImg'][0]
        refImg=rM_getNpFromFile(myRefFile, myOptDict)

    if '--rgb' in myOptDict:
        np_img=rM_getMergedNpRGB(myOptDict['--rgb'], myOptDict)

    if '--flatfield' in myOptDict:
        myFile=myOptDict['--flatfield']

        np_flatfield = rM_getNpFromFile(myFile, myOptDict)

        if np_img.shape != np_flatfield.shape:
            print("error: flatfield image needs the same shape as the main")
            sys.exit()
        #external parsing must secure that np_img exists up to this
        #point!
        img16bit=np.array(200*np.divide(np_img,np_flatfield)).astype(int)
        img16bit[img16bit>255]=255
        #Overriding the previous array
        np_img = np.array(Image.fromarray(img16bit.astype('uint8'), 'RGB'))

    qrCodeData=None

    #######Testing part############
    if '-C' in myOptDict:
        # print("Used the -C option")
        # sys.exit()
        pass

    #####End of testing part######

    if any(v in myOptDict for v in ['-a', '--QR', '-d']):
        try:
            x = np_img
        except:
            print("error: need an image to continue.")
            sys.exit()

    if '--QR' in myOptDict:
        if '--refImg' in myOptDict:
            qrCodeData = getQRCodeData(refImg)
            qrCodeData[0]=np_img #Using the right image
        else:
            qrCodeData = getQRCodeData(np_img)

        if qrCodeData is None:
            print("Unable to locate QR code.")
            print("Rotation strategy failed.")
            sys.exit()

    if '-d' in myOptDict:
        angle=0

        if qrCodeData is not None:
            angle = qrCodeData[1]

        if '--QR' not in myOptDict:
            #Note: listOfResultDicts was not implemented here, this is
            #not critical at the moment
            np_img, img_1, img_2, img_3 = doRectColorDeconvStuff(np_img,\
                                                                 qrCodeData,\
                                                                 myOptDict,\
                                                                 rgb_from_cus)
        else:
            np_imgR, img_1R, img_2R, img_3R, listOfResultDicts = doQRColorDeconvStuff(np_img,\
                                                                   myOptDict,\
                                                                   qrCodeData,\
                                                                   rgb_from_cus)

        if '--QR' in myOptDict:
            fig, ax = deconvMultiplotCus(np_imgR, img_1R, img_2R, img_3R)
        else:
            fig, ax = deconvMultiplotCus(np_img, img_1, img_2, img_3)

        if '--no-show' not in myOptDict:
            plt.show()

    if '-a' in myOptDict:
        if '--QR' not in myOptDict:
            np_img, listOfResultDicts = doRectAnalysisStuff(np_img, myOptDict)
        else:
            np_img, listOfResultDicts = doQRAnalysisStuff(np_img, myOptDict, qrCodeData)

        if '-Q' in myOptDict:
            rgbVect=getRGBVect(listOfResultDicts[0])
            tsvFName = myOptDict['-Q'][0]
            saveQuickMatrix(tsvFName, rgbVect)
            print("#-Q saved the matrix in %s" %(tsvFName))

        if '--no-show' not in myOptDict and '-d' not in myOptDict:
            try:
                imgplot = plt.imshow(np_img)
                plt.show()
            except:
                if '-c' not in myOptDict:
                    print("error: --no-show no image to display")
                    sys.exit()

    #Why is this option undocumented?!!
    if '-i' in myOptDict:
        intensity = imageIntensity(np_img)
        imgplot = plt.imshow(intensity)
        plt.show()

    if '--save-jpg' in myOptDict:
        if '-d' in myOptDict:
            fig.savefig(myOptDict['--save-jpg'], dpi=200)
        else:
            try:
                #directly saving the numpy array
                im = Image.fromarray(np_img)
                im.save(myOptDict['--save-jpg'])
            except:
                print("error: either -c, -o or -f need to be used for --save-jpg")
  
        

if __name__ == "__main__":
    main(sys.argv)
