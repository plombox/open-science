import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.odr import *
from itertools import product # used to build up error-bands
from mpmath import mp
from sympy import symbols, diff
from plombox_analysis_functions import get_cmap_and_fonts, \
                                       expo_func, error_prop_expo_func, error_prop_interpolation
                                       

#editing fonts
size_of_font = 7
cmap, CB_color_cycle, font1, font2 = get_cmap_and_fonts(size_of_font = size_of_font)


def expo_fit_LED(samples, samples_error):
  
    #-------------exponential fit---------------
    # alias data to match previous example
    xData = np.array(df1_original['S'], dtype=float)
    #determining y axis for fit
    y = np.array(df1_200['S'], dtype=float)
    ye = np.array(df1_200['sStd'], dtype=float)
    
    #do the fit
    expo_model = Model(expo_func)
    # Create a RealData object using our initiated data from above.
    data = RealData(xData,y,sy = ye)
    # Set up ODR with the model and data.
    odr = ODR(data, expo_model, beta0=[5, 0.05,2])
    # Run the regression.
    out = odr.run()
    # Use the in-built pprint method to give us results.
    print("All data LED")
    out.pprint()
     
    
    xModel = np.linspace(min(xData), max(xData))
    yModel = expo_func(out.beta, xModel)
     
    #calculate error bands
    error = error_prop_expo_func(out.cov_beta, xModel, out.beta)
    
    fit_up = yModel + error
    fit_dw = yModel - error
      
      
    #Interpolation calculation
    sample = np.array(samples, dtype=float)
    sample_error = np.array(samples_error, dtype=float)
    expected_value = np.interp(sample, yModel,xModel)
    
    #Propagate error for interpolated lead value
    interpolated_value_error = error_prop_interpolation(sample, sample_error, out.cov_beta, out.beta)
    
    #Final array
    interpolated_value = expected_value
    
    #print("LED_value" + str(interpolated_value))
    #print("LED_error" + str(interpolated_value_error))
    
    #plotting data
    plt.errorbar(df1_original['S'], y, df1_200['sStd'], color = 'black', fmt='.', markersize=5, label = "Samples")
    
    
    #exponential
    plt.errorbar(xModel, yModel,  None, None, fmt='--', color = CB_color_cycle[1], label  = 'Data Fit') # fit
     
    # plot the error bands
    plt.fill_between(xModel,fit_up ,fit_dw, color = CB_color_cycle[0], alpha = 0.3, label = '1\u03C3 error band')
    
    #interpolated data
    plt.errorbar(interpolated_value, sample, sample_error, interpolated_value_error, fmt='.', color = CB_color_cycle[2], label  = 'Interpolated Values') # fit
     
    
    
    plt.xlabel('Digital Saturation', fontname = 'Arial', fontsize = size_of_font)
    plt.ylabel('Measured Saturation', fontname = 'Arial', fontsize = size_of_font)
    plt.legend(prop=font1,loc='best')
    plt.tick_params(axis='both', which='major', labelsize=size_of_font)
    #plt.title('Real vs Measured Saturation')
    #plt.ylim(0,20)
    #plt.xlim(-5,5)
    plt.savefig(date+"_real_vs_measured_saturation_fit_interpolation_paper", dpi = 350)
    plt.clf() 
    
    return interpolated_value, interpolated_value_error


if __name__ == '__main__':
    fig = plt.figure()

    date = "16122022"
    df1 = pd.read_csv('../data/data-used-for-publication/real_colour_vs_obtained_colour.csv') #file with shv and rgb data 
    df2 = pd.read_csv('../data/data-used-for-publication/{}_INA_Assay_lead_curve.csv'.format(date)) #file with shv and rgb data 
    df3 = pd.read_csv('../data/data-used-for-publication/{}_INA_Assay_samples.csv'.format(date)) #file with shv and rgb data 


    df1_200 = df1.query("brightness == 201") #200 is using the old SHV sticky paper measurements with a smaller ROI. 201 was made with a larger ROI
    df1_original = df1.query("brightness == 0")


    #INITIAL VARIABLES

    lead_curve = [0,10,15,20,50,100]

    lead_curve_saturation = []
    lead_curve_saturation_e = []

    sample_saturation = []
    sample_saturation_e = []


    lead_interpolated_value = []
    lead_interpolated_value_error = []

    #ARRAYS FOR SUB VALUES
    final_lead_curve = []
    final_lead_curve_e = []


    #Getting ideal Saturation Values from ratio with Adobe Colour and data taken with LED = 200
    new_lead_curve_S, new_lead_curve_sStd = expo_fit_LED(df2['S'], df2['sStd'])   
    #new_sample_S, new_sample_sStd = expo_fit_LED(df3['S'], df3['sStd']) 

    """
    # reading the csv file to include Saturation values with ratio account for
    df = pd.read_csv(date+"_INA_Assay_lead_curve.csv") 
    # updating the column value/data
    for x in range(len(new_lead_curve_S)):
        df.loc[x, 'S_led'] = new_lead_curve_S[x]
        df.loc[x, 'sStd_led'] = new_lead_curve_sStd[x]
        # writing into the file
        df.to_csv(date+"_INA_Assay_lead_curve.csv", index=False)


    # reading the csv file to include Saturation values with ratio account for
    df = pd.read_csv(date+"_INA_Assay_samples.csv") 
    # updating the column value/data
    for x in range(len(new_sample_S)):
        df.loc[x, 'S_led'] = new_sample_S[x]
        df.loc[x, 'sStd_led'] = new_sample_sStd[x]
        # writing into the file
        df.to_csv(date+"_INA_Assay_samples.csv", index=False)
    """
