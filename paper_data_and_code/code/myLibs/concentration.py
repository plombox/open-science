import sys

def getConcentration(params, myOptDict, server = False):
    if len(params) == 0:
        # catch for previously failed analysis
        if not server:
            defaultFunction()
            sys.exit()
        else:
            myOptDict = {}
            
    if '--C0' in myOptDict and '-a' in myOptDict:
        s, s_stddev = params["S"], params["sStd"]
        return getConcentration_HSV_0(s, s_stddev)
    if '--C0' in myOptDict and '-d' in myOptDict\
       and '--QR' in myOptDict:
        blueStain, bS_stddev = params[0]*100, params[1]*100
        return getConcentration_Blue_Stain_maca0(blueStain, bS_stddev)
    if '--C0' in myOptDict and '-d' in myOptDict:
        blueStain, bS_stddev = params[0]*100, params[3]*100
        return getConcentration_Blue_Stain_maca0(blueStain, bS_stddev)

    if '--C1' in myOptDict and '-a' in myOptDict:
        s, s_stddev = params["S"], params["sStd"]
        return getConcentration_HSV_cell23Nov(s, s_stddev)
    if '--C1' in myOptDict and '-d' in myOptDict\
       and '--QR' in myOptDict:
        blueStain, bS_stddev = params[0]*100, params[1]*100
        return getConcentration_Stain_cell23Nov(blueStain, bS_stddev)
    if '--C1' in myOptDict and '-d' in myOptDict:
        blueStain, bS_stddev = params[0]*100, params[3]*100
        return getConcentration_Stain_cell23Nov(blueStain, bS_stddev)

    if '--C2' in myOptDict and '-a' in myOptDict:
        s, s_stddev = params["S"], params["sStd"]
        return getConcentration_HSV_esp23Nov(s, s_stddev)
    if '--C2' in myOptDict and '-d' in myOptDict\
       and '--QR' in myOptDict:
        getConcentration_Stain_esp23Nov()
    if '--C2' in myOptDict and '-d' in myOptDict:
        getConcentration_Stain_esp23Nov()

    if '--C3' in myOptDict and '-a' in myOptDict:
        s, s_stddev = params["S"], params["sStd"]
        return getConcentration_HSV_esp25Nov(s, s_stddev)
    if '--C3' in myOptDict and '-d' in myOptDict\
       and '--QR' in myOptDict:
        s, s_stddev = params[0]*100, params[1]*100
        return getConcentration_Stain_esp25Nov(s, s_stddev)
    if '--C3' in myOptDict and '-d' in myOptDict:
        s, s_stddev = params[0]*100, params[3]*100
        return getConcentration_Stain_esp25Nov(s, s_stddev)

    if '--CDummy' in myOptDict and '-a' in myOptDict:
        s, s_stddev = params["S"], params["sStd"]
        return getConcentration_HSV_dummy(s, s_stddev)
    if '--CDummy' in myOptDict and '-d' in myOptDict\
       and '--QR' in myOptDict:
        s, s_stddev = params[0]*100, params[1]*100
        return getConcentration_Stain_dummy(s, s_stddev)
    if '--CDummy' in myOptDict and '-d' in myOptDict:
        s, s_stddev = params[0]*100, params[3]*100
        return getConcentration_Stain_dummy(s, s_stddev)

    else:
        defaultFunction()
        if not server:
            sys.exit()

def defaultFunction():
    print("Just the default function")
    print("Please use a sub-option of -C")

def getConcentration_HSV_0(s, s_stddev):
    """From the first successful construction see macaOct27_2021"""
    if(s<5.6):
        m = 0.29
        c = 2.7
    else:
        m = 0.05479705
        c = 4.93763838

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_Blue_Stain_maca0(blueStain, bS_stddev):
    """From the first successful construction see macaOct27_2021 be sure
to use it along with the --maca0 matrix when using the color
deconvolution

    """
    if(blueStain<5.02):
        m = 0.057
        c = 4.45
    else:
        m = 0.005
        c = 5.036

    mean_concentration = (blueStain-c)/m
    conc_error = bS_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_HSV_cell23Nov(s, s_stddev):
    """For nov 23 2021 construction"""
    if(s<46.2):
        m = 1.97
        c = 23.5
    else:
        m = 0.228
        c = 43.2

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_Stain_cell23Nov(s, s_stddev):
    """For nov 23 2021 construction"""
    if(s<20.13):
        m = 0.076
        c = 17.37
    else:
        m = 0.0162
        c = 18.13

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_HSV_esp23Nov(s, s_stddev):
    """For nov 23 2021 construction"""
    if(s<29.2):
        m = 0.34
        c = 25.8
    else:
        m = 0.13
        c = 29.2

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_HSV_esp25Nov(s, s_stddev):
    """For nov 25 2021 construction"""
    if(s<20):
        m = 1.29
        c = 5.0
    elif(s<35):
        m = 0.417
        c = 17.9
    else:
        m = 0.104
        c = 33.8

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_Stain_esp25Nov(s, s_stddev):
    """For nov 25 2021 construction"""
    if(s<5.7):
        m = 0.097
        c = 4.58
    elif(s<6.7):
        m = 0.0245
        c = 5.55
    else:
        m = 0.019
        c = 6.52

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error


def getConcentration_Stain_esp23Nov():
    """For nov 23 2021 construction"""
    print("Not implemented")
    sys.exit()

def getConcentration_HSV_dummy(s, s_stddev):
    """For nov 23 2021 construction"""
    if(s<46.2):
        m = 1.97
        c = 23.5
    else:
        m = 0.228
        c = 43.2

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error

def getConcentration_Stain_dummy(s, s_stddev):
    """For testing"""
    if(s<20.13):
        m = 0.076
        c = 17.37
    else:
        m = 0.0162
        c = 18.13

    mean_concentration = (s-c)/m
    conc_error = s_stddev * 1/m
    return mean_concentration, conc_error
