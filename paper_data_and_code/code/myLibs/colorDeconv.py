import numpy as np
# from collections.abc import Iterable
from skimage.color import rgb2hed, hed2rgb,\
    separate_stains, combine_stains
from skimage import data

#Obtained matrix from imageJ
# rgb_from_cus = np.array([[0.56248593, 0.5642921, 0.6043046],
#                          [0.5135564, 0.5899113, 0.62310874],
#                          [0.47075787, 0.6321325, 0.61546373]])

#Maybe better with reduced digits
# rgb_from_cus = np.array([[0.56, 0.56, 0.60],
#                          [0.51, 0.58, 0.62],
#                          [0.47, 0.63, 0.61]])

#Setting THE MATRIX!!
def whatIsTheMatrix(myOptDict):
    # print("Entered the matrix")
    if '--hed' in myOptDict:
        #Blue pill
        #This is the hed color base
        rgb_from_cus = np.array([[0.65, 0.70, 0.29],
                                 [0.07, 0.99, 0.11],
                                 [0.27, 0.57, 0.78]])
    elif '--mau' in myOptDict:
        #... green pill?
        rgb_from_cus = np.array([[-.176, 0.667, 0.722],
                                 [0.994, 0.067, 0.074],
                                 [0.991, 0.122, 0.038]])
    elif '--mau2' in myOptDict:
        #... orange pill?
        rgb_from_cus = np.array([[0.032, 0.084, 0.995],
                                 [0.054, 0.611, 0.789],
                                 [0.688, 0.688, 0.230]])
    elif '--maca0' in myOptDict:
        #Maca's blue (good) pill?
        rgb_from_cus = np.array([[0.705, 0.503, 0.499],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--10Hnov9' in myOptDict:
        #A new blue pill?
        rgb_from_cus = np.array([[0.876, 0.415, 0.244],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--ONnov9' in myOptDict:
        #It has come to this... yet another pill
        rgb_from_cus = np.array([[0.889, 0.361, 0.280],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--cellNov23' in myOptDict:
        # another pill
        rgb_from_cus = np.array([[0.924, 0.296, 0.239],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--espNov23' in myOptDict:
        # market is saturated
        rgb_from_cus = np.array([[0.873, 0.368, 0.316],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--espNov25' in myOptDict:
        # more saturation!
        rgb_from_cus = np.array([[0.841, 0.405, 0.356],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--sampleDec24' in myOptDict:
        # MORE SATURATION!
        rgb_from_cus = np.array([[0.737, 0.470, 0.484],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--dMatrix' in myOptDict:
        # A dummy matrix for making quick tests, make sure that
        # whatever you put here is properly normalized (see the
        # spreadsheet "miscellaneousDir/mausBase.ods").
        rgb_from_cus = np.array([[0.810, 0.478, 0.337],
                                 [0.164, 0.164, 0.972],
                                 [0.784, 0.100, 0.611]])
    elif '--mFile' in myOptDict:
        # Loading the matrix directly from a file
        myFile = myOptDict['--mFile'][0]
        rgb_from_cus = np.loadtxt(myFile, dtype=float)
    else:
        #Red pill
        rgb_from_cus = np.array([[0.92, 0.33, 0.16],
                                 [0.3, 0.94, 0.15],
                                 [0.18, 0.67, 0.7]])

    return rgb_from_cus

def rgb2cus(rgb, cus_from_rgb, *, channel_axis=-1):
    return separate_stains(rgb, cus_from_rgb)

def cus2rgb(hed, rgb_from_cus, *, channel_axis=-1):
    return combine_stains(hed, rgb_from_cus)

def get3StainsArrCus(img_rgb, cus_from_rgb):
    img_cus = rgb2cus(img_rgb, cus_from_rgb)

    stain_1 = img_cus[:, :, 0]
    stain_2 = img_cus[:, :, 1]
    stain_3 = img_cus[:, :, 2]

    return stain_1, stain_2, stain_3

def getNPImgFromStainsCus(np_img,\
                          stain_1, stain_2, stain_3,\
                          rgb_from_cus):
    null = np.zeros_like(np_img[:, :, 0])

    img_1 = (255*cus2rgb(np.stack((stain_1, null, null), axis=-1),\
                         rgb_from_cus)).astype('uint8')
    img_2 = (255*cus2rgb(np.stack((null,stain_2, null), axis=-1),\
                         rgb_from_cus)).astype('uint8')
    img_3 = (255*cus2rgb(np.stack((null, null, stain_3), axis=-1),\
                         rgb_from_cus)).astype('uint8')

    return img_1, img_2, img_3

def getRGBVect(dictEle):
    return np.array([dictEle['red'], dictEle['green'], dictEle['blue']])

def getDivVect(rgbArr, arr4Div=np.array([255, 255, 255])):
    return rgbArr/arr4Div

def getLog10Op(rgbArr):
    return -np.log10(rgbArr)

def getNormVect(rgbArr):
    return rgbArr/np.linalg.norm(rgbArr)

def getMatrixRow(rgbVect, arr4Div=np.array([255, 255, 255])):
    divVect=getDivVect(rgbVect)
    log10Vect=getLog10Op(divVect)
    normVect=getNormVect(log10Vect)
    return normVect

def get2Dummies():
    #yellowish
    s2 = [0.16430, 0.16430, 0.97262]
    #dummy
    s3 = [0.78473, 0.10021, 0.61167]
    return s2, s3

def getQuickMatrix(rgbVect):
    s1 = getMatrixRow(rgbVect)
    s2, s3 = get2Dummies()
    return np.array([s1, s2, s3])

def saveQuickMatrix(fName, rgbVect):
    mRow=getMatrixRow(rgbVect)
    matrix2Use = getQuickMatrix(rgbVect)
    np.savetxt(fName, matrix2Use)
 
def getDeconvMatrix(s1RGB, s2RGB, s3RGB, dList):
    row1=getMatrixRow(s1RGB, dList)
    row2=getMatrixRow(s2RGB, dList)
    row3=getMatrixRow(s3RGB, dList)
    return np.array([row1, row2, row3])
