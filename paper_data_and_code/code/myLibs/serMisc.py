import time

def sm_getStrJsonSerial(ser, sleep = 0.8, printAll = False):
    """Reads from the serial port until a timeout is reached or something
that may be a json is read, in which case it returns that. A careful
check is not done since it takes too long and it may miss input from
the serial port. A thorough check is later given by the "is_json"
function.

    """
    maxLoop=100
    x = ser.readline()
    if printAll:
        print(x)

    time.sleep(sleep) #Giving a bit of time to the camera
    while True:
        if printAll:
            print(x)
        if len(x) < 4:
            #Ignoring empty strings and stuff is most definetively a json.
            continue

        if chr(x[0]) == '{':
            #Maybe a json, there are quicker ways but here timing is
            #everything!
            break

        x = ser.readline()

        maxLoop -= 0
        if maxLoop == 0:
            x = None
            break
    return x
