import sys
import numpy as np
import numpy.ma as ma
import scipy.signal as sig
from colorsys import rgb_to_hsv
from PIL import Image
import json
import ast #as literal eval
import base64
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib
#matplotlib.use('agg') # disable tinker (?) (i.e., plt.show() won't show anything)
from io import BytesIO
import cv2
from scipy.stats import iqr
import qrcode as qr
from scipy import ndimage
import random
from math import sqrt
from myLibs.colorDeconv import *
from myLibs.concentration import *

sampleMaxSize = 200
fastHSV = False
useRectangle = True


"""Just a set of miscellaneous functions"""
def isFloat(myStr):
    try:
        float(myStr)
    except ValueError:
        return False
    return True

def getNewIndex(oldI, maxOldI,maxNewI):
    return maxNewI*oldI//maxOldI

def getRectIdx(mySize,fracHalf,offsetX=0,offsetY=0):
    #for drawing a rectangle
    middleX=mySize[1]//2
    middleY=mySize[0]//2

    deltaX=int(fracHalf*mySize[1])
    deltaY=int(fracHalf*mySize[0])

    xMin=middleX-deltaX + offsetX
    xMax=middleX+deltaX + offsetX

    yMin=middleY-deltaY + offsetY
    yMax=middleY+deltaY + offsetY
    rectIdx=[xMin, xMax, yMin, yMax]
    return rectIdx

def getRectIdxFromNP(np_img,fracHalf,offsetX=0,offsetY=0):
    # mySize = np_img.shape[:2]
    mySize = np.shape(np_img)[:2]
    middleX=mySize[1]//2
    middleY=mySize[0]//2

    deltaX=int(fracHalf*mySize[1])
    deltaY=int(fracHalf*mySize[0])

    xMin=middleX-deltaX + offsetX
    xMax=middleX+deltaX + offsetX

    yMin=middleY-deltaY + offsetY
    yMax=middleY+deltaY + offsetY
    rectIdx=(xMin, xMax, yMin, yMax)
    return rectIdx

def getNewIdxRect(oldIdxRect, oldShape,\
                  newShape):
    # getting the indices of the shapes
    oldMaxY,oldMaxX=oldShape[:2]
    newMaxY,newMaxX=newShape[:2]

    # unpacking the old rectangle indices
    oldXMin,oldXMax,oldYMin,oldYMax=oldIdxRect

    # calculating the new indices
    newYMin=getNewIndex(oldYMin,oldMaxY,newMaxY)
    newYMax=getNewIndex(oldYMax,oldMaxY,newMaxY)

    newXMin=getNewIndex(oldXMin,oldMaxX,newMaxX)
    newXMax=getNewIndex(oldXMax,oldMaxX,newMaxX)

    return (newXMin, newXMax, newYMin, newYMax)

def getCList(newImage,rectIdx,colorFmt='bgr',bitFmt=8):
    #unpacking the ranges
    xMin,xMax,yMin,yMax=rectIdx
    #getting the corresponding channels from the region
    blue_channel = newImage[yMin:yMax,xMin:xMax,0]
    green_channel = newImage[yMin:yMax,xMin:xMax,1]
    red_channel = newImage[yMin:yMax,xMin:xMax,2]

    if colorFmt == 'rgb':
        #just swapping b and r
        blue_channel,red_channel=red_channel,blue_channel

    #flattening them for doing averages
    blueFlat=blue_channel.flatten()
    greenFlat=green_channel.flatten()
    redFlat=red_channel.flatten()

    bAv=np.average(blueFlat)
    gAv=np.average(greenFlat)
    rAv=np.average(redFlat)

    hAv,sAv,vAv=avg_rgb_to_hsv(rAv, gAv, bAv,bitFmt)

    bStd=np.std(blueFlat)
    gStd=np.std(greenFlat)
    rStd=np.std(redFlat)

    cRangeL=getColorRanges(rAv,gAv,bAv,rStd,gStd,bStd,bitFmt)
    hsvMin,hsvMax=getHSVRanges(cRangeL,bitFmt)

    blueMin=np.amin(blueFlat)
    greenMin=np.amin(greenFlat)
    redMin=np.amin(redFlat)

    blueMax=np.amax(blueFlat)
    greenMax=np.amax(greenFlat)
    redMax=np.amax(redFlat)

    rMix=str(redMin)+'-'+str(redMax)
    gMix=str(greenMin)+'-'+str(greenMax)
    bMix=str(blueMin)+'-'+str(blueMax)

    # print(blueMax, greenMax, redMax)

    intens=np.sqrt(bAv**2+gAv**2+rAv**2)

    cList=[bAv, gAv, rAv, intens,\
           rStd,gStd,bStd,rMix,gMix,bMix,\
           hAv,sAv,vAv,hsvMin,hsvMax]

    return cList

# takes the output from getCList and turns it into a simple dictionary
def dictFromCList(cList):
    keys = ["bAv", "gAv", "rAv", "intens", "rStd", \
            "gStd", "bStd", "rMix", "gMix", "bMix", \
            "hAv", "sAv", "vAv", "hsvMin", "hsvMax"]

    # create the dictionary
    clistDict = dict(zip(keys, cList))
    # flatten it
    clistDict = flattenDict(clistDict)

    return clistDict

# flatten lists in a dictionary
def flattenDict(data):
    """ if one encounters a list in a dictionary and desires the dictionary list to be flat: use this function. Returns the dictionary with flat "lists"

    data: the dictionary
    """
    newDict = {}

    for key in data:
        # not a list? great
        if type(data[key]) is not list:
            newDict[key] = data[key]
            # not a list - lets flatten this
        else:
            for index, value in enumerate(data[key]):
                if type(value) is list:
                    # list of lists? -- we don't cover this yet
                    print("\n\nWARNING: LIST OF LISTS\n\n")
                else:
                    # was just a standard list: all good
                    newDict[key+"_%s" %index] = value

    return newDict

def avg_rgb_to_hsv(r,g,b,bitFmt=8):
    """Converts rgb to hsv, by default it expects values to from 0 to 255
(floats) . In case they are 10 bit for example then div should be
2**10

    """
    div=2**bitFmt
    # renormalizing average rgb values
    r /= div
    g /= div
    b /= div
    h,s,v = rgb_to_hsv(r,g,b)
    return (360*h,100*s,100*v)

def getHSVRanges(cRangeL,bitFmt):
    """Gets the HSV ranges from ranges defined in RGB"""
    #unpacking first the range values
    rMin,rMax,gMin,gMax,bMin,bMax=cRangeL
    hMin,sMin,vMin=avg_rgb_to_hsv(rMin,gMin,bMin,bitFmt)
    hMax,sMax,vMax=avg_rgb_to_hsv(rMax,gMax,bMax,bitFmt)

    #This color base is weird just swapping values just in case
    if hMin > hMax:
        hMin,hMax = hMax,hMin
    if sMin > sMax:
        sMin,sMax = sMax,sMin
    if vMin > vMax:
        vMin,vMax = vMax,vMin

    return [[hMin,sMin,vMin],[hMax,sMax,vMax]]

def getColorRanges(r,g,b,rStd,gStd,bStd,bitFmt):
    """The color ranges without going over the rgb limits, useful for HSV
range calculations"""
    maxVal=2**bitFmt

    rMin,rMax=getSingleCRange(r,rStd,maxVal)
    gMin,gMax=getSingleCRange(g,gStd,maxVal)
    bMin,bMax=getSingleCRange(b,bStd,maxVal)

    return rMin,rMax,gMin,gMax,bMin,bMax

def getSingleCRange(c,cStd,maxC):
    cMin=c-cStd
    if cMin < 0:
        cMin=0

    cMax=c+cStd
    if cMax < 0:
        cMax=maxC

    return cMin,cMax

def getCListFromFName(fName):
    img= Image.open(fName)
    np_img = np.array(img)

    fracHalf=0.15
    mySize=np_img.shape[:2]
    rectIdx=getRectIdx(mySize,fracHalf)

    #Slicing is done inside getCList
    cList=getCList(np_img,rectIdx,colorFmt='rgb',bitFmt=8)

    return cList

def plotFName(fName, mask=False):
    img= Image.open(fName)
    np_img = np.array(img)

    fracHalf=0.02
    mySize=np_img.shape[:2]
    offsetL = [[0, 0]]
    if fName == "Sensar_dryed_papers.jpeg":
        offsetL = [[-290, -138], [-190, -138], [-90, -138], [0, -138], [110, -138], [210, -135],\
                   [-290, -30], [-190, -30], [-90, -30], [0, -30], [110, -30], [210, -30],\
                   [-290, 65], [-190, 65], [-90, 65], [0, 65], [110, 65], [210, 65],\
                   [-290, 170], [-190, 170], [-90, 170], [0, 170], [110, 170], [210, 170]
        ]
    if fName == "Sensar_fresh_papers.jpeg":
        offsetL = [[-325, -138], [-210, -138], [-90, -138], [0, -138], [120, -138], [230, -135],\
                   [-325, -20], [-210, -20], [-90, -20], [0, -20], [120, -30], [230, -30],\
                   [-325, 95], [-210, 95], [-90, 95], [0, 95], [120, 95], [230, 95],\
                   [-325, 210], [-210, 210], [-90, 210], [0, 210], [120, 210], [230, 210]
        ]


    for i, off in enumerate(offsetL):
        offX, offY = off
        rectIdx=getRectIdx(mySize,fracHalf, offX,offY)
        xMin, xMax, yMin, yMax = rectIdx
        subImage = np_img[yMin:yMax,xMin:xMax]
        myMask=np.zeros(np.prod(np.shape(subImage))).\
            reshape(len(subImage),len(subImage[0]),3)
        if mask:
            np_img[yMin:yMax,xMin:xMax]=myMask

        print("i = ", i)

        printCList(getCList(np_img,rectIdx,colorFmt='rgb',bitFmt=8))

    imgplot = plt.imshow(np_img)
    plt.show()

def getCListFromImg(img, cDict={}):
    np_img = np.array(img)

    fracHalf=0.15
    mySize=np_img.shape[:2]
    rectIdx=getRectIdx(mySize,fracHalf)

    #Slicing is done inside getCList
    cList=getCList(np_img,rectIdx,colorFmt='rgb',bitFmt=8)

    return cList

def getCListFromB64String(b64Str, cDict={}):
    img = Image.open(BytesIO(base64.b64decode(b64Str)))
    cList = getCListFromImg(img, cDict)
    return cList

def getCListFromDict(newDict, cDict={}):
    st = json.dumps(newDict,ensure_ascii=False)
    bi = st.encode('utf8')
    stringFromDict = json.dumps(newDict, ensure_ascii=False)
    payload = stringFromDict.encode('utf8')

    data = json.loads(payload.decode())

    imgFName="tempImgs/dataimage.png"
    if 'imageFName' in cDict:
        imgFName = cDict['imageFName']

    with open(imgFName,"wb") as f:
        if "measurement" in data:
            if "image" in data["measurement"]:
                f.write(base64.b64decode(data["measurement"]["image"]))
            elif "data" in data["measurement"]:
                f.write(base64.b64decode(data["measurement"]["data"]))
        # if "phone" in data:
        #     f.write(base64.b64decode(data["phone"]["measurement"]["image"])

    # img = mpimg.imread('tempImgs/dataimage.png')
    # imgplot = plt.imshow(img)

    # plt.show()

    cList = getCListFromFName(imgFName)

    return cList

# def getCListFromDict2(newDict, cDict={}):
#     print("Inside the new funct")
#     st = json.dumps(newDict,ensure_ascii=False)
#     bi = st.encode('utf8')
#     # stringFromDict = json.dumps(newDict, ensure_ascii=False)
#     # payload = stringFromDict.encode('utf8')

#     # data = json.loads(payload.decode())

#     b64Str = newDict["phone"]["measurement"]["image"]

#     cList = getCListFromB64String(b64Str)

#     # imgFName="tempImgs/dataimage.png"
#     # if 'imageFName' in cDict:
#     #     imgFName = cDict['imageFName']


#     # cList = getCListFromFName(imgFName)

#     return cList

def getCListFromJsonStr(jsonStr, cDict={}):
    newDict=json.loads(jsonStr)
    cList=getCListFromDict(newDict, cDict)
    return cList

def printCList(cList, hBool=True, xTraParam = None):
    hStr="#red\tgreen\tblue\tintens\trStd\tgStd\tbStd\trMix\tgMix\tbMix"
    Hhsv="H\tS\tV\tHmin\tSMin\tVmin\tHmax\tSMax\tVmax"

    fmtStr="%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%s\t%s\t%s"
    hsvFmt="%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f"

    if xTraParam != None:
        hStr="#rNum\tred\tgreen\tblue\tintens\trStd\tgStd\tbStd\trMix\tgMix\tbMix"
        fmtStr="%d\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%s\t%s\t%s"

    hStr+='\t'+Hhsv
    fmtStr+='\t'+hsvFmt

    #unpacking cList
    bAv,gAv,rAv,\
        intens,rStd,\
        gStd,bStd,rMix,\
        gMix,bMix,\
        hAv,sAv,vAv,\
        hsvMin,hsvMax=cList

    bTup=(rAv,gAv,bAv,intens,rStd,gStd,bStd,\
          rMix,gMix,bMix)

    if xTraParam != None:
        bTup=(xTraParam,rAv,gAv,bAv,intens,rStd,gStd,bStd,\
              rMix,gMix,bMix)
    fTup=()

    fTup+=bTup
    hMin,sMin,vMin=hsvMin
    hMax,sMax,vMax=hsvMax
    hsvTup=(hAv,sAv,vAv,hMin,sMin,vMin,hMax,sMax,vMax)
    fTup+=hsvTup
    if hBool:
        print(hStr)
    print(fmtStr %(fTup))

def is_json(myjson):
  try:
    json_object = json.loads(myjson)
  except ValueError as e:
    return False
  return True

def plotB64Str(b64Str):
    img = Image.open(BytesIO(base64.b64decode(b64Str)))
    imgplot = plt.imshow(img)
    plt.show()

def getNPArrFromB64(b64Str):
    img = Image.open(BytesIO(base64.b64decode(b64Str)))
    np_img = np.array(img)
    return np_img

def plotNP(np_img):
    imgplot = plt.imshow(np_img)
    plt.show()

def getMaskedNP(np_img, fracHalf = 0.05,offsetX=0,offsetY=0):
    rectIdx = getRectIdxFromNP(np_img,fracHalf,offsetX,offsetY)
    xMin, xMax, yMin, yMax = rectIdx
    subImage = np_img[yMin:yMax,xMin:xMax]
    myMask=np.zeros(np.prod(np.shape(subImage))).\
        reshape(len(subImage),len(subImage[0]),3)
    np_img[yMin:yMax,xMin:xMax]=myMask
    return np_img

def drawNPRect(np_img, rectIdx, color = (0, 0, 255), thick = 2):
    xMin, xMax, yMin, yMax = rectIdx
    img_mod= cv2.rectangle(np_img,(xMin,yMin),\
                           (xMax,yMax),color,thick)
    return img_mod

def maskedStatistics(npArray,n = 4,mask=None,tol=0.1):
    mean = float(np.ma.average(npArray))
    stdDev = float(np.ma.std(npArray))
    Count = 0
    MaxIter = 20
    flag = True
    meanPre = mean
    if np.any(mask) != None:
        Mask = mask
    while (Count < MaxIter) and (flag == True):
        Lrange = mean - n * stdDev
        Hrange = mean + n * stdDev
        Mask = np.logical_or(np.less_equal(npArray,Lrange), np.greater_equal(npArray,Hrange))
        npArrayMasked = np.ma.array(npArray, mask=Mask)
        mean = float(np.ma.average(npArrayMasked))
        stdDev = float(np.ma.std(npArrayMasked)/np.sqrt(len(npArrayMasked))) #Adriana changed this 02122022 
        Count += 1

        if np.abs(mean - meanPre) < tol:
            flag = False
        else:
            meanPre = mean
    return mean, stdDev, npArrayMasked, Mask

def maskedStatisticsRGB(npArray,n = 2*10,mask=None):
    npArraySize = np.ma.shape(npArray)
    meanRGB = []
    stdDevRGB = []
    npArrayMaskedRGB = np.ma.copy(npArray*0)


    for ps in range(npArraySize[2]):
        mean, stdDev, npArrayMasked, Mask = maskedStatistics(npArray[:,:,ps],n = n)
        meanRGB.append(mean); stdDevRGB.append(stdDev); npArrayMaskedRGB[:,:,ps] = npArrayMasked;# MaskRGB[:,:,ps] = Mask

    return np.array(meanRGB), np.array(stdDevRGB), npArrayMaskedRGB, npArrayMaskedRGB.mask

def plotMaskedImage(fName, wiener=False, winSize=5,file=False,showPic=True):
    img= Image.open(fName)
    np_img = np.array(img)
    np_img_ma = np.ma.array(np_img)

    ResultsD = {}
    # if wiener:
    #     np_img_ma.mask

    mySize=np_img.shape[:2]
    offsetL = [[0, 0, 0, 0]]
    if fName == "Sensar_dryed_papers.jpeg":
        imgCoordD = {'A1':[318,198,379,240],'A2':[420,198,477,232],'A3':[514,204,572,245],'A4':[620,199,675,242],\
            'A5':[723,203,782,246],'A6':[816,209,872,247],'B1':[315,304,373,345],'B2':[418,304,478,343],'B3':[519,304,578,343],\
            'B4':[619,305,677,348],'B5':[717,305,778,348],'B6':[815,302,876,346],'C1':[319,400,378,443],'C2':[420,408,479,446],\
            'C3':[521,408,578,453],'C4':[617,408,675,450],'C5':[719,404,778,445],'C6':[820,408,878,450],'D1':[320,511,380,550],\
            'D2':[419,504,478,548],'D3':[519,503,579,547],'D4':[616,512,676,552],'D5':[718,510,775,554],'D6':[814,512,873,556]}\

    if fName == "Sensar_fresh_papers.jpeg":
        imgCoordD = {'A1':[272,196,342,247],'A2':[388,196,455,243],'A3':[506,198,576,237],'A4':[617,192,686,240],\
            'A5':[727,193,797,241],'A6':[850,194,920,242],'B1':[273,317,344,363],'B2':[393,319,462,362],'B3':[504,320,574,366],\
            'B4':[618,312,687,358],'B5':[731,313,803,358],'B6':[848,309,921,352],'C1':[279,439,348,484],'C2':[388,435,461,476],\
            'C3':[510,431,579,477],'C4':[619,426,689,473],'C5':[730,429,802,473],'C6':[846,428,918,473],'D1':[276,552,345,600],\
            'D2':[390,557,460,604],'D3':[509,551,576,601],'D4':[620,550,690,600],'D5':[736,551,804,598],'D6':[850,551,921,594]}\

    i = 0
    for imgCoord, name in zip(imgCoordD.values(),imgCoordD.keys()):
        subImage = np_img_ma[imgCoord[1]:imgCoord[3],imgCoord[0]:imgCoord[2],:]

        if wiener:
            for ps in range(0,np_img.shape[2]):
                subImage[:,:,ps] = sig.wiener(subImage[:,:,ps],(winSize,winSize))

        meanRGB, stdDevRGB, subImage, Mask = maskedStatisticsRGB(subImage)
        auxMask = np.logical_or(Mask[:,:,0],Mask[:,:,1],Mask[:,:,2])
        subImage[:,:,0].mask = auxMask
        subImage[:,:,1].mask = auxMask
        subImage[:,:,2].mask = auxMask
        print("i = ", i)
        str2print, result2save = resultsMaskedStats(meanRGB, stdDevRGB, subImage)
        print(str2print)
        ResultsD[i] = (result2save)
        # if fileF == True:
        #     file = open(fName+'.results.txt','w+')
        #     strFile = str(i)+' '+name+'\n'+str2print+'\n'
        #     file.write(strFile)
        i += 1

    if showPic:
        imgplot = plt.imshow(np_img)
        plt.show()

    return ResultsD

def RGBImg2HSVImg(subImage,bitFmt=8):
    div=2**bitFmt
    hsvImage = np.ma.copy(subImage)*0
    hsvImage = hsvImage.astype('float64')
    imgSize = subImage.shape
    for ps in range(0,imgSize[0],1):
        for qs in range(0,imgSize[1],1):
            if subImage.mask[ps,qs,0] == False and subImage.mask[ps,qs,1] == False and subImage.mask[ps,qs,2] == False:
                h,s,v = rgb_to_hsv(subImage.data[ps,qs,0]/div, subImage.data[ps,qs,1]/div, subImage.data[ps,qs,2]/div)
                hsvImage.data[ps,qs,0] = 360*h; hsvImage.data[ps,qs,1] = 100*s; hsvImage.data[ps,qs,2] = 100*v
    return hsvImage

def RGBImg2HSVImg_nomask(subImage,bitFmt=8):
    div=2**bitFmt
    hsvImage = np.copy(subImage)*0
    hsvImage = hsvImage.astype('float64')
    imgSize = subImage.shape
    for ps in range(0,imgSize[0],1):
        for qs in range(0,imgSize[1],1):
            h,s,v = rgb_to_hsv(subImage.data[ps,qs,0]/div, subImage.data[ps,qs,1]/div, subImage.data[ps,qs,2]/div)
            hsvImage.data[ps,qs,0] = 360*h; hsvImage.data[ps,qs,1] = 100*s; hsvImage.data[ps,qs,2] = 100*v
    return hsvImage

def resultsMaskedStats(meanRGB, stdDevRGB, subImage):
    str2print = ''; result2save = {}

    hStr="#red\tgreen\tblue\tintens\trStd\tgStd\tbStd\trMix\tgMix\tbMix\tH\tS\tV\thMix\tSMix\tvMix\thStd\tsStd\tvStd\n"

    fmtStr="%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%s\t%s\t%s\t%0.1f\t%0.1f\t%0.1f\t%s\t%s\t%s\t%0.1f\t%0.1f\t%0.1f"
    intens = np.sqrt(meanRGB[0]**2+meanRGB[1]**2+meanRGB[2]**2)
    minRGB = (ma.min(subImage[:,:,0]),ma.min(subImage[:,:,1]),ma.min(subImage[:,:,2]))
    maxRGB = (ma.max(subImage[:,:,0]),ma.max(subImage[:,:,1]),ma.max(subImage[:,:,2]))
    rMix=str(minRGB[0])+'-'+str(maxRGB[0])
    gMix=str(minRGB[1])+'-'+str(maxRGB[1])
    bMix=str(minRGB[2])+'-'+str(maxRGB[2])

    hsvImage = rgb_to_hsv_array(subImage)
    hsvImage = np.ma.array(hsvImage,mask=subImage.mask)

    hsvMean = (ma.mean(hsvImage[:,:,0]),ma.mean(hsvImage[:,:,1]),ma.mean(hsvImage[:,:,2]))
    hsvStd = (ma.std(hsvImage[:,:,0]),ma.std(hsvImage[:,:,1]),ma.std(hsvImage[:,:,2]))
    minHSV = (ma.min(hsvImage[:,:,0]),ma.min(hsvImage[:,:,1]),ma.min(hsvImage[:,:,2]))
    maxHSV = (ma.max(hsvImage[:,:,0]),ma.max(hsvImage[:,:,1]),ma.max(hsvImage[:,:,2]))
    hMix="%.0f-%.0f"%(minHSV[0],maxHSV[0])
    sMix="%.0f-%.0f"%(minHSV[1],maxHSV[1])
    vMix="%.0f-%.0f"%(minHSV[2],maxHSV[2])

    dataTup = (meanRGB[0],meanRGB[1],meanRGB[2],intens,stdDevRGB[0],stdDevRGB[1],stdDevRGB[2],rMix,gMix,bMix,\
        hsvMean[0],hsvMean[1],hsvMean[2],hMix,sMix,vMix,hsvStd[0],hsvStd[1],hsvStd[2])
    str2print = hStr + fmtStr %dataTup
    keysTup = ('meanR','meanG','meanB','Intens','stdR','stdG','stdB','minR','minG','minB','maxR','maxG','maxB','meanH',\
        'meanS','meanV','minH','minS','minV','maxH','maxS','maxV','stdH','stdS','stdV')
    dataTup2 = (meanRGB[0],meanRGB[1],meanRGB[2],intens,stdDevRGB[0],stdDevRGB[1],stdDevRGB[2],minRGB[0],minRGB[1],minRGB[2],maxRGB[0],maxRGB[1],maxRGB[2],\
        hsvMean[0],hsvMean[1],hsvMean[2],minHSV[0],minHSV[1],minHSV[2],maxHSV[0],maxHSV[1],maxHSV[2],hsvStd[0],hsvStd[1],hsvStd[2])

    for datum,key in zip(dataTup2,keysTup):
        result2save[key] = datum

    return str2print, result2save

def flatDict(Dict,key):
    Result = []
    for ele in Dict.keys():
        try:
            Result.append(Dict[ele][key])
        except:
            print('Not %s found.' %key)

    return Result

def divideList(List, n):
    lenList = len(List)
    if lenList % n == 0:
        numList = int(lenList/n)
        LList = [List[i*n:((i+1)*n)] for i in range(0,numList,1)]

    return LList

def fitfunlin(xdata,a,b,c,d):
    ydata = a*xdata[0,:] + b*xdata[1,:] + c*xdata[2,:] + d
    return ydata

def fitfunRational(xdata,a,b,c):
    ydata = a*((183.9*xdata[0]+1196.0)/(xdata[0]+6.981)) + b*((45.88*xdata[1]+254.6)/(xdata[1]+12.95)) + c*((62.2*xdata[2]+947)/(xdata[2]+12.68))
    return ydata

def Rsquared(fun,xdata,ydata,param):
    if fun.__name__ == 'fitfunlin':
        yfun = fun(xdata,param[0],param[1],param[2],param[3])
    elif fun.__name__ == 'fitfunRational':
        yfun = fun(xdata,param[0],param[1],param[2])
    yMean = np.average(ydata)
    SStot = np.sum((ydata-yMean)**2)
    SSres = np.sum((ydata-yfun)**2)
    R_2 = 1 - (SSres/SStot)
    return R_2

def imageIntensity(RGBpic,bits=8):
    norm = 2**bits
    redNorm =  RGBpic[:,:,0].astype('float')/norm
    greenNorm =  RGBpic[:,:,1].astype('float')/norm
    blueNorm =  RGBpic[:,:,2].astype('float')/norm
    intensity = (norm/np.sqrt(3))*np.sqrt(redNorm**2+greenNorm**2+blueNorm**2)
    return intensity

def wiener2pic(RGBpic,winerSize=5):
    wienerPic = RGBpic.astype('float')
    for ps in range(0,RGBpic.shape[2]):
        wienerPic[:,:,ps] = sig.wiener(wienerPic[:,:,ps],(winerSize,winerSize))
    return wienerPic

def gradient(pic,format='gray'):
    if format == 'gray':
        gradient = np.gradient(pic)
    elif format == 'rgb':
        gradientRed = np.gradient(pic[:,:,0])
        gradientGreen = np.gradient(pic[:,:,1])
        gradientBlue = np.gradient(pic[:,:,2])
        gradient = [gradientRed, gradientGreen, gradientBlue]

    return gradient

def saveArray2PicFile(pic,filename):
    if pic.dtype.name != 'uint8':
        picAux = pic.astype('uint8')
    else:
        picAux = pic
    im = Image.fromarray(picAux)
    im.save(filename)

def readImg2Array(filename):
    img= Image.open(filename)
    np_img = np.array(img)
    return np_img

def measureColorImage(fName, mask=None, coord=None, wiener=False, winSize=5,file=False,showPic=True):
    img= Image.open(fName)
    np_img = np.array(img)
    if wiener:
        for ps in range(0,np_img.shape[2],1):
            np_img[:,:,ps] = sig.wiener(np_img[:,:,ps],(winSize,winSize))
    np_img_ma = np.ma.array(np_img)

    ResultsD = {}
    if mask is not None:
        if mask.any() != None:
            maskRGB = np.zeros(np_img_ma.shape,dtype='bool')

            for ps in range(0,np_img.shape[2],1):
                maskRGB[:,:,ps] = mask
            np_img_ma.mask = maskRGB.copy()
            subImage = np_img_ma.copy()
    else:
        if coord == None:
            subImage = np_img_ma.copy()
        else:
            try:
                subImage = np_img_ma[coord[1]:coord[3],coord[0]:coord[2],:]
            except:
                return None

    # if wiener:
    #     for ps in range(0,subImage.shape[2],1):
    #         subImage[:,:,ps] = sig.wiener(subImage[:,:,ps],(winSize,winSize))

    meanRGB, stdDevRGB, subImage, Mask = maskedStatisticsRGB(subImage)
    auxMask = np.logical_or(Mask[:,:,0],Mask[:,:,1],Mask[:,:,2])
    subImage[:,:,0].mask = auxMask
    subImage[:,:,1].mask = auxMask
    subImage[:,:,2].mask = auxMask
    print("File: ", fName)
    str2print, result2save = resultsMaskedStats(meanRGB, stdDevRGB, subImage)
    ResultsD[fName.split("/")[-1]] = (result2save)
    print(str2print, "\n")
    if showPic:
        imgplot = plt.imshow(subImage)
        plt.show()

    return ResultsD

def mergeDicts(dict1, dict2):
    return dict2.update(dict1)

def plotFileName(fName, coord=None, mask=False, showPic=True ):
    img= Image.open(fName)
    np_img = np.array(img)
    if coord == None:
        shape = np_img.shape
        xMin = 0
        yMin = 0
        xMax = shape[0] - 1
        yMax = shape[1] - 1

    elif type(coord) is list:
        xMin = coord[0]
        yMin = coord[1]
        xMax = coord[2]
        yMax = coord[3]
    else:
        print("Coord argument has not the proper format")
        return None

    rectIdx = [xMin, xMax, yMin, yMax]
    subImage = np_img[yMin:yMax,xMin:xMax]
    myMask=np.zeros(np.prod(np.shape(subImage))).\
        reshape(len(subImage),len(subImage[0]),3)
    if mask:
        np_img[yMin:yMax,xMin:xMax]=myMask

    print("File: ", fName.split("/")[-1])

    printCList(getCList(np_img,rectIdx,colorFmt='rgb',bitFmt=8))

    if showPic == True:
        imgplot = plt.imshow(np_img)
        plt.show()

def rM_getAveImgArr(filenameL):
    # Assuming all images are the same size, get dimensions of first image
    w,h=Image.open(filenameL[0]).size
    N=len(filenameL)

    # Create a numpy array of floats to store the average (assumimg RGB images)
    aveImgArr=np.zeros((h,w,3),np.float)

    # Build up average pixel intensities, casting each image as an array of floats
    for filename in filenameL:
        imarr=np.array(Image.open(filename),dtype=np.float)
        aveImgArr+=imarr/N

    # Round values in array and cast as 8-bit integer
    aveImgArr=np.array(np.round(aveImgArr),dtype=np.uint8)

    # Generate and save the final image
    # out=Image.fromarray(arr,mode="RGB")
    # out.save("Average.jpg")
    # out.show()
    return aveImgArr

def rM_getImgFromJson(jsonObject):
    """Tries out the known formats"""
    b64Str = ""
    if 'measurement' in jsonObject:
        if 'image' in jsonObject['measurement']:
            b64Str = jsonObject['measurement']['image']
    if "phone" in jsonObject:
        if "measurement" in jsonObject["phone"]:
            if "image" in jsonObject["phone"]["measurement"]:
                b64Str = jsonObject["phone"]["measurement"]["image"]

    if b64Str == "":
        print("error: json fomat not currently suported")
        sys.exit()

    if b64Str == None:
        print("error: image had a None value.")
        sys.exit()

    np_img = getNPArrFromB64(b64Str)
    return np_img

# def regionIdentification(fileName, showMask=False, showInt=False, percentage=0.1):
#     img= Image.open(fileName)
#     np_img = np.array(img)
#     #ma_img = ma.array(img)
#     imgInt = imageIntensity(np_img,bits=8)
#     imgMin = ma.min(imgInt)
#     imgMax = ma.max(imgInt)

#     if imgMax - imgMin > 1:
#         imgIntSize = imgInt.size
#         h = 2*iqr(imgInt)/(imgIntSize**(1/3))
#         numBins = (imgMax - imgMin)/h
#         numBins = np.round(numBins)
#         bins = imgMin + h * np.linspace(0,int(numBins),int(numBins)+1)
#         maskList = []
#         numberMaskedPixels = []
#         estimatedMean = 0.0
#         maxIndex = 0
#         maxPixels = 0
#         numberRanges = len(bins)-1
#         for ps in range (0, numberRanges, 1):
#             maskList.append((imgInt<=bins[ps+1]) * (imgInt>bins[ps]))
#             numberMaskedPixels.append(np.sum(maskList[ps]))
#             estimatedMean += numberMaskedPixels[ps]*(bins[ps+1]+bins[ps])/2
#         numberMaskedPixels = np.array(numberMaskedPixels)
#         estimatedMean /= imgIntSize

#         if False:
#             oneSigma = np.round(percentage*imgIntSize)
#             maxPixels = np.max(numberMaskedPixels)
#             maxIndex = np.where(numberMaskedPixels == maxPixels)
#             maxIndex = int(maxIndex[0])
#             events = numberMaskedPixels[maxIndex]
#             indexList = [maxIndex]
#             counter = 1
#             while events <= oneSigma:
#                 ps = maxIndex+counter; qs = maxIndex-counter
#                 if ps < numberRanges and qs>=0:
#                     events += numberMaskedPixels[ps] + numberMaskedPixels[qs]
#                     if ps not in indexList:
#                         indexList.append(ps)
#                     if qs not in indexList:
#                         indexList.append(qs)
#                     counter += 1
#                 elif ps >= numberRanges and qs>=0:
#                     events += numberMaskedPixels[qs]
#                     if qs not in indexList:
#                         indexList.append(qs)
#                     counter += 1
#                 elif ps < numberRanges and qs<0:
#                     events += numberMaskedPixels[ps]
#                     if ps not in indexList:
#                         indexList.append(ps)
#                     counter += 1
#             indexList.sort()
#             Mask = np.zeros(imgInt.shape,dtype='bool')
#             for index in indexList:
#                 Mask = np.logical_or(Mask,maskList[index])
#             Mask = np.logical_not(Mask)
#         else:
#             maxIndex = len(maskList)-1
#             Mask = maskList[maxIndex]
#             Mask = np.logical_not(Mask)

#         if showMask:
#             imgplot = plt.imshow(Mask)
#             plt.show()

#         if showInt:
#             imgplot = plt.imshow(imgInt)
#             plt.show()

#         return Mask

#     else:
#         print("ERROR: Image has the same intensity all around.")
#         return None

def measureColorNpArr(np_img, mask=None, coord=None, wiener=False,\
                      winSize=5,file=False,showPic=True,counter=0,hBool=False):
    if wiener:
        for ps in range(0,np_img.shape[2],1):
            np_img[:,:,ps] = sig.wiener(np_img[:,:,ps],(winSize,winSize))

    np_img_ma = ma.array(np_img)

    if mask is not None:
        stackMask = np.stack([mask,mask,mask],axis=2)
        np_img_ma.mask = stackMask

    if coord == None:
        subImage = np_img_ma.copy()
    else:
        try:
            subImage = np_img_ma[coord[1]:coord[3],coord[0]:coord[2],:]
        except:
            return None

    meanRGB, stdDevRGB, subImage, Mask = maskedStatisticsRGB(subImage,mask=mask)
    auxMask = np.logical_or(Mask[:,:,0],Mask[:,:,1],Mask[:,:,2])
    subImage[:,:,0].mask = auxMask
    subImage[:,:,1].mask = auxMask
    subImage[:,:,2].mask = auxMask
    ## Problem: subImage can be empty after the masking - is that something which can happen
    ##          if things work properly ? (Right now we catch that in resultsMaskedString.
    ##          (This we added today, because of the problem here)
    ## Solution ?
    str2print, resultDict = resultsMaskedString(meanRGB, stdDevRGB, subImage, counter, hBool)

    return str2print, resultDict


def measureColor(np_img, coord=None,counter=0,hBool=False):

    if coord == None:
        subImage = np_img.copy()
    else:
        try:
            subImage = np_img[coord[1]:coord[3],coord[0]:coord[2],:]
        except:
            return None

    str2print, resultDict = resultsString(subImage, counter, hBool)

    return str2print, resultDict


def resultsString(subImage, counter,hBool):
    str2print = ''; result2save = {}
    maxRangeHSV = [360,100,100]
    if hBool:
        hStr="#rNum\tred\tgreen\tblue\tintens\trStd\tgStd\tbStd\trMix\tgMix\tbMix\tH\tS\tV\thMix\tSMix\tvMix\thStd\tsStd\tvStd\n"

    if len(subImage.shape) >=3:
        flattenImageR = subImage[:,:,0].flatten()
        flattenImageG = subImage[:,:,1].flatten()
        flattenImageB = subImage[:,:,2].flatten()
    else:
        flattenImageR = subImage[:,0].flatten()
        flattenImageG = subImage[:,1].flatten()
        flattenImageB = subImage[:,2].flatten()

    meanRGB = np.array([flattenImageR.mean(), flattenImageG.mean(), flattenImageB.mean()])
    stdDevRGB = np.array([flattenImageR.std(),flattenImageG.std(),flattenImageB.std()])
    minRGB = np.array([flattenImageR.min(),flattenImageG.min(),flattenImageB.min()])
    maxRGB = np.array([flattenImageR.max(),flattenImageG.max(),flattenImageB.max()])
    # meanRGB = [flattenImageR[:,0].mean(),flattenImageG[:,1].mean(),flattenImageB[:,2].mean()]
    # stdDevRGB = [flattenImageR[:,0].std(),flattenImageG[:,1].std(),flattenImageB[:,2].std()]
    # minRGB = [flattenImageR[:,0].min(),flattenImageG[:,1].min(),flattenImageB[:,2].min()]
    # maxRGB = [flattenImageR[:,0].max(),flattenImageG[:,1].max(),flattenImageB[:,2].max()]
    fmtStr="%d\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%0.1f\t%s\t%s\t%s\t%0.1f\t%0.1f\t%0.1f\t%s\t%s\t%s\t%0.1f\t%0.1f\t%0.1f"
    intens = np.sqrt(meanRGB[0]**2+meanRGB[1]**2+meanRGB[2]**2)
    rMix=str(minRGB[0])+'-'+str(maxRGB[0])
    gMix=str(minRGB[1])+'-'+str(maxRGB[1])
    bMix=str(minRGB[2])+'-'+str(maxRGB[2])


    lenflatten = len(flattenImageR)
    if lenflatten <= sampleMaxSize:
        sampleSize = lenflatten
    else:
        sampleSize = sampleMaxSize

    index = np.arange(0,lenflatten,1)
    sample = np.random.choice(index,sampleSize)
    vector = np.array([flattenImageR[sample],flattenImageG[sample],flattenImageB[sample]])
    hsvImage = rgb_to_hsv_vector(vector)
    hsvMean = (hsvImage[0,:].mean(),hsvImage[1,:].mean(),hsvImage[2,:].mean())
    factor = sampleSize/(sampleSize-1)
    hsvStd = (hsvImage[0,:].std()*factor,hsvImage[1,:].std()*factor,hsvImage[2,:].std()*factor)
    minHSV = (max(hsvMean[0]-1.96*hsvStd[0],0),max(hsvMean[1]-1.96*hsvStd[1],0),max(hsvMean[2]-1.96*hsvStd[2],0))
    maxHSV = (min(hsvMean[0]+1.96*hsvStd[0],maxRangeHSV[0]),min(hsvMean[1]+1.96*hsvStd[1],maxRangeHSV[1]),min(hsvMean[2]+1.96*hsvStd[2],maxRangeHSV[2]))
    hMix="%.0f-%.0f"%(minHSV[0],maxHSV[0])
    sMix="%.0f-%.0f"%(minHSV[1],maxHSV[1])
    vMix="%.0f-%.0f"%(minHSV[2],maxHSV[2])

    dataTup = (counter,meanRGB[0],meanRGB[1],meanRGB[2],intens,stdDevRGB[0],stdDevRGB[1],stdDevRGB[2],rMix,gMix,bMix,\
        hsvMean[0],hsvMean[1],hsvMean[2],hMix,sMix,vMix,hsvStd[0],hsvStd[1],hsvStd[2])

    if hBool:
        str2print = hStr + fmtStr %dataTup
    else:
        str2print = fmtStr %dataTup

    resultDict = { "rNum":counter, "red":meanRGB[0], "green":meanRGB[1], \
        "blue":meanRGB[2], "intens":intens, "rStd":stdDevRGB[0], \
        "gStd":stdDevRGB[1], "bStd":stdDevRGB[2], "rMix":rMix, "gMix":gMix, \
        "bMix":bMix, "H":hsvMean[0], "S":hsvMean[1], "V":hsvMean[2], \
        "hMix":hMix, "SMix":sMix, "vMix":vMix, "hStd":hsvStd[0], \
        "sStd":hsvStd[1], "vStd":hsvStd[2] }

    return str2print, resultDict

def resultsMaskedString(meanRGB, stdDevRGB, subImage, counter, hBool, \
                        hsv_from_mean_rgb = True):
    # test if we can use subImage as intended
    try:
        ma.min(subImage[:,:,0])
    except:
        # seems as if we can not. Abort
        print("resultsMaskedString: subImage not working")
        return '', {}

    str2print = ''; result2save = {}

    if hBool:
        hStr="#rNum\tred\tgreen\tblue\tintens\trStd\tgStd\tbStd\trMix\tgMix\tbMix\tH\tS\tV\thMix\tSMix\tvMix\thStd\tsStd\tvStd\n"

    fmtStr="%d\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%0.2f\t%s\t%s\t%s\t%0.2f\t%0.2f\t%0.2f\t%s\t%s\t%s\t%0.2f\t%0.2f\t%0.2f"
    intens = np.sqrt(meanRGB[0]**2+meanRGB[1]**2+meanRGB[2]**2)
    minRGB = (ma.min(subImage[:,:,0]),ma.min(subImage[:,:,1]),ma.min(subImage[:,:,2]))
    maxRGB = (ma.max(subImage[:,:,0]),ma.max(subImage[:,:,1]),ma.max(subImage[:,:,2]))
    rMix=str(minRGB[0])+'-'+str(maxRGB[0])
    gMix=str(minRGB[1])+'-'+str(maxRGB[1])
    bMix=str(minRGB[2])+'-'+str(maxRGB[2])

    hsvImage = rgb_to_hsv_array(subImage)
    hsvImage = np.ma.array(hsvImage,mask=subImage.mask)
    minHSV = (ma.min(hsvImage[:,:,0]),ma.min(hsvImage[:,:,1]),ma.min(hsvImage[:,:,2]))
    maxHSV = (ma.max(hsvImage[:,:,0]),ma.max(hsvImage[:,:,1]),ma.max(hsvImage[:,:,2]))

    if hsv_from_mean_rgb:
        hsvMean, hsvStd = rgb_to_hsv_mean_values(meanRGB,stdDevRGB)
        hsvMean = (hsvMean[0], hsvMean[1], hsvMean[2])
        hsvStd = (hsvStd[0], hsvStd[1], hsvStd[2])
    else:
        hsvMean = (ma.mean(hsvImage[:,:,0]), ma.mean(hsvImage[:,:,1]), ma.mean(hsvImage[:,:,2]))
        hsvStd = (ma.std(hsvImage[:,:,0]), ma.std(hsvImage[:,:,1]), ma.std(hsvImage[:,:,2]))

    hMix="%.0f-%.0f"%(minHSV[0],maxHSV[0])
    sMix="%.0f-%.0f"%(minHSV[1],maxHSV[1])
    vMix="%.0f-%.0f"%(minHSV[2],maxHSV[2])

    dataTup = (counter,meanRGB[0],meanRGB[1],meanRGB[2],intens,stdDevRGB[0],stdDevRGB[1],stdDevRGB[2],rMix,gMix,bMix,\
        hsvMean[0],hsvMean[1],hsvMean[2],hMix,sMix,vMix,hsvStd[0],hsvStd[1],hsvStd[2])

    if hBool:
        str2print = hStr + fmtStr %dataTup
    else:
        str2print = fmtStr %dataTup

    resultDict = { "rNum":counter, "red":meanRGB[0], "green":meanRGB[1], \
        "blue":meanRGB[2], "intens":intens, "rStd":stdDevRGB[0], \
        "gStd":stdDevRGB[1], "bStd":stdDevRGB[2], "rMix":rMix, "gMix":gMix, \
        "bMix":bMix, "H":hsvMean[0], "S":hsvMean[1], "V":hsvMean[2], \
        "hMix":hMix, "SMix":sMix, "vMix":vMix, "hStd":hsvStd[0], \
        "sStd":hsvStd[1], "vStd":hsvStd[2] }
    
    return str2print, resultDict

def rM_getNpFromFile(fName, myOptDict):
    if fName.endswith('json') or fName.endswith('jso'):
        with open(fName) as f:
            data = f.read()
            #jsonObj=json.load(f) #fails cause jsons are not valid
            #json.loads will also fail when called over the data variable!!
        jsonObj = ast.literal_eval(data)
        np_img0 = rM_getImgFromJson(jsonObj)
    else:
        np_img0=np.array(Image.open(fName))

    return np_img0

def rM_getMergedNpRGB(rgbL, myOptDict):
    np_img=None
    for i, cF in enumerate(myOptDict['--rgb']):
        if np_img is None:
            np_img=rM_getNpFromFile(cF, myOptDict)
        np_img[:,:,i]=rM_getNpFromFile(cF, myOptDict)[:,:,i]# np.array(img)[:,:,i]
    return np_img

# the same as rM_getMergedNpRGB but for the server
def rM_getMergedNpRGBServer(files_as_np_array_list):
    np_img=None
    for i, cF in enumerate(files_as_np_array_list):
        if np_img is None:
            np_img = cF
        np_img[:,:,i] = cF[:,:,i]# np.array(img)[:,:,i]
    return np_img

def findMinMax(points):
    xQRmin = 1e10
    xQRmax = -1
    yQRmin = 1e10
    yQRmax = -1
    for point in points[0]:
        xQRmin = np.minimum(point[0],xQRmin)
        xQRmax = np.maximum(point[0],xQRmax)
        yQRmin = np.minimum(point[1],yQRmin)
        yQRmax = np.maximum(point[1],yQRmax)
    return xQRmin, xQRmax, yQRmin, yQRmax

def getQRCenter(points):
    """Uses the diagonal edges for getting the central coordinates"""
    c1x,c1y=points[3]
    c4x,c4y=points[1]

    centerx=(c1x+c4x)//2
    centery=(c1y+c4y)//2
    center=(int(centerx), int(centery))
    return center

def getDataForFastHSV(np_img, center, radius):
    h, w = np_img.shape[:2]
    Idx = np.indices((h, w))
    Y = Idx[0,:,:]
    X = Idx[1,:,:]
    Idx, Idy = np.where(np.sqrt((X-center[0])**2+(Y-center[1])**2) <= radius)
    data = np_img[Idx,Idy,:]
    return data

def getCircularMask(np_arr, center, radius):
    h, w = np_arr.shape[:2]
    Idx = np.indices((h, w))
    Y = Idx[0,:,:]
    X = Idx[1,:,:]
    # Y, X = np.ogrid[:h, :w]
    mask = np.sqrt((X-center[0])**2+(Y-center[1])**2) <= radius
    #Returning a mask where everything is covered except the circle
    #True means covered & False not covered
    return mask

def getCircularMaskNegated(np_arr, center, radius):
    h, w = np_arr.shape[:2]
    Idx = np.indices((h, w))
    Y = Idx[0,:,:]
    X = Idx[1,:,:]

    # Y, X = np.ogrid[:h, :w]
    mask = np.sqrt((X-center[0])**2+(Y-center[1])**2) >= radius
    #Returning a mask where everything is covered except the circle
    #True means covered & False not covered
    return mask

def getQRCodeData(img, maxTries = 10, server = False):
    det = cv2.QRCodeDetector()
    retval, points, straight_qrcode = det.detectAndDecode(img)
    sucess = True

    angle=0
    m = img.copy()
    while points is None:
        angle=random.uniform(-1,1)
        m = ndimage.rotate(m, angle)
        retval, points, straight_qrcode = det.detectAndDecode(m)
        maxTries-=1
        if maxTries == 0:
            print("redMisc::getQRCodeData::Rotation strategy failed")
            sucess = False
            if not server:
                sys.exit()
            else:
                # get an empty list here
                points = [[ ]]
                # let's get out of this infinity loop
                break

    points = points[0] #Remembering that is was a lists of lists for
                       #multiple regions, we are simply concentrating
                       #in the first one which is the only one unless
                       #the decode multi is used... which is not ;-)

    if not server:
        return [m, angle, retval, points, straight_qrcode]
    else:
        return sucess, [m, angle, retval, points, straight_qrcode]

def getQRUnit(points):
    """Given the main sides of the QR square it returns the average"""
    l1 = sqrt((points[0][0]-points[1][0])**2+(points[0][1]-points[1][1])**2)
    l2 = sqrt((points[0][0]-points[3][0])**2+(points[0][1]-points[3][1])**2)

    return (l1+l2)/2

def doRectAnalysisStuff(np_img, myOptDict, server = False):
    # results to return
    listOfResultDicts = []
    
    mySize=np_img.shape[:2]

    if '-r' not in myOptDict:
        #Just including it for keeping a consistent structural form
        fracHalf = 0.15
        if '--shift' in myOptDict:
            # in case there are ROI centres given, we use these
            #   (Depending on the coordinates these are given in this does not
            #   make much sense. One ould scrap this part of the if condition
            #   and do only the else.)
            rectCoords = []
            halfWidth = 15
            # define boxes
            for cordPair in myOptDict['--shift']:
                xMin, xMax = cordPair[0] - halfWidth, cordPair[0] + halfWidth
                yMin, yMax = cordPair[1] - halfWidth, cordPair[1] + halfWidth
                rectIdx = [xMin, xMax, yMin, yMax]
                for cord in rectIdx: rectCoords.append(cord)
            myOptDict['-r'] = rectCoords
        else:
            rectIdx=getRectIdx(mySize,fracHalf)
            myOptDict['-r']=rectIdx

    counter = 0
    for i in range(len(myOptDict['-r'])//4):
        rectIdx = myOptDict['-r'][i*4:4*i+4]
        xMin, xMax, yMin, yMax = rectIdx
        # this is used for slicing images, so lets go int
        xMin, xMax = int(xMin), int(xMax)
        yMin, yMax = int(yMin), int(yMax)
        #Now doing extra parsing not initially possible
        maxY, maxX = mySize
        if  not 0 < xMin < maxX or not 0 < xMax < maxX:
            print(xMin)
            print("error: x values are out of bounds")
            if not server:
                sys.exit()
            else:
                resultDict = {}
                listOfResultDicts.append(resultDict)
                continue
        if  not 0 < yMin < maxY or not 0 < yMax < maxY:
            print("error: y values are out of bounds")
            if not server:
                sys.exit()
            else:
                resultDict = {}
                listOfResultDicts.append(resultDict)
                continue

        if '-q' in myOptDict: #This was undocumented why?!
            #It is not shown on the help!!
            #Why does the general behaviour change?! Who did this?!
            #Now I need to keep this because other scripts depend on this!!

            #Stain printing is not currently supported in this case.
            cList=getCList(np_img,rectIdx,colorFmt='rgb',bitFmt=8)
            hBool = True
            if counter > 0:
                hBool = False
            printCList(getCList(np_img,rectIdx,colorFmt='rgb',bitFmt=8),\
                    hBool, i)
            counter += 1
        else:
            #Just why!? Hue values can vary wildly because of this..
            #It makes the HSV analysis worse.
            wiener = False
            hBool = True
            if i > 0:
                hBool = False
            if '-w' in myOptDict:
                wiener = True

            if fastHSV:
                colorMeasVals = measureColor(np_img, \
                                                     coord=[xMin,yMin,xMax,yMax], \
                                                     counter=i,hBool=hBool)
            else:
                colorMeasVals = measureColorNpArr(np_img, \
                                                  coord = [xMin,yMin,xMax,yMax], \
                                                  wiener=wiener, counter=i, \
                                                  hBool=hBool)
            if colorMeasVals is not None:
                # it may be none when the analysis fails
                str2print, resultDict = colorMeasVals[0], colorMeasVals[1]
            else:
                str2print, resultDict = "", {}

            if '-C' not in myOptDict:
                print(str2print)
            else:
                if hBool:
                    print("#rNum\tconc\tcStd")

                out = getConcentration(resultDict, myOptDict, server = server)
                if out != None:
                    c, cStd = out[0], out[1]
                    print("%d\t%2.2f\t%2.2f" % (i, c, cStd))
                    resultDict["conc"] = c
                    resultDict["cStd"] = cStd

        if '--no-rect' not in myOptDict and '--no-ROI' not in myOptDict:
            rectIdx[0] = int(rectIdx[0])
            rectIdx[1] = int(rectIdx[1] + 0.5)
            rectIdx[2] = int(rectIdx[2])
            rectIdx[3] = int(rectIdx[3] + 0.5)
            np_img = drawNPRect(np_img, rectIdx)

        listOfResultDicts.append(resultDict)

    return np_img, listOfResultDicts

def doQRAnalysisStuff(np_img, myOptDict, qrCodeData, server = False):
    np_img, angle, retval, points, straight_qrcode = qrCodeData

    centerROIs = [getQRCenter(points)]
    listOfResultDicts = []
    if '--shift' in myOptDict:
        centerROIs=myOptDict['--shift']

    qrUnit, qrOrigin, qrXVector,\
        qrYVector, qrXUVector, qrYUVector = getQRRefSystem(points)

    R=0.05
    if '-R' in myOptDict:
        R=myOptDict['-R']
    myRadius = int(R*qrUnit)

    hBool = True
    for iCount, cROI in enumerate(centerROIs):
        centerROI = tuple(qrXUVector*cROI[0]+qrYUVector*cROI[1]+qrOrigin)
        centerROI = (int(centerROI[0]), int(centerROI[1]))

        if fastHSV:
            if useRectangle:
                x1=centerROI[0]-myRadius; x2 = centerROI[0]+myRadius
                y1=centerROI[1]-myRadius; y2 = centerROI[1]+myRadius
                str2print, resultDict = measureColor(np_img, coord=[x1,y1,x2,y2],counter=iCount,hBool=hBool)
            else:

                np_img_ = getDataForFastHSV(np_img, centerROI, myRadius)
                str2print, resultDict = measureColor(np_img_, coord=None,counter=iCount,hBool=hBool)
        else:
            if useRectangle:
                x1=centerROI[0]-myRadius; x2 = centerROI[0]+myRadius
                y1=centerROI[1]-myRadius; y2 = centerROI[1]+myRadius
                str2print, resultDict = measureColorNpArr(np_img, mask=None, coord=[x1,y1,x2,y2], wiener=False,\
                                                  winSize=5,file=False,\
                                                  showPic=True,counter=iCount,hBool=hBool)
            else:
                x1=centerROI[0]-myRadius; x2 = centerROI[0]+myRadius
                y1=centerROI[1]-myRadius; y2 = centerROI[1]+myRadius
                mask1 = getCircularMaskNegated(np_img, centerROI, myRadius)
                str2print, resultDict = measureColorNpArr(np_img, mask=mask1, coord=[x1,y1,x2,y2], wiener=False,\
                                                  winSize=5,file=False,\
                                                  showPic=True,counter=iCount,hBool=hBool)
        if '-C' not in myOptDict:
            print(str2print)
        else:
            if hBool:
                print("#rNum\tconc\tcStd")
            i = iCount

            out = getConcentration(resultDict, myOptDict, server)
            if out != None:
                c, cStd = out[0], out[1]
                print("%d\t%2.2f\t%2.2f" % (i, c, cStd))
                resultDict["conc"] = c
                resultDict["cStd"] = cStd

        listOfResultDicts.append(resultDict)

        hBool = False

        colorTuple=(0, 0, 255)#Blue
        if '-m' in myOptDict:
            if iCount == myOptDict['-m']:
                colorTuple=(255,0,0)#Red
        if '--no-rect' not in myOptDict and '--no-ROI' not in myOptDict:
            # np_img = cv2.circle(np_img, centerROI, radius=myRadius,\
            #             color=colorTuple, thickness=2)
            if useRectangle:
                np_img = drawNPRect(np_img, [x1, x2, y1, y2], color=colorTuple)
            else:
                np_img = cv2.circle(np_img, centerROI, radius=myRadius,\
                                color=colorTuple, thickness=2)

    np_img = np_img.astype('uint8')
    points = points.astype('int')
    if '--draw-axes' in myOptDict:
        cv2.arrowedLine(np_img, tuple(points[0]),\
                        tuple(points[1]), (255,0,0), 5)
        cv2.arrowedLine(np_img, tuple(points[0]),\
                        tuple(points[3]), (0,255,0), 5)

    return np_img, listOfResultDicts

def doQRColorDeconvStuff(np_img, myOptDict, qrCodeData, rgb_from_cus, \
                         server = False):
    np_img, angle, retval, points, straight_qrcode = qrCodeData
    centerROIs = [getQRCenter(points)]
    cus_from_rgb = np.linalg.inv(rgb_from_cus)
    listOfResultDicts = []
    if '--shift' in myOptDict:
        centerROIs=myOptDict['--shift']

    qrUnit, qrOrigin, qrXVector,\
        qrYVector, qrXUVector, qrYUVector = getQRRefSystem(points)
    R=0.05
    if '-R' in myOptDict:
        R=myOptDict['-R']
    myRadius = int(R*qrUnit)

    stainList = get3StainsArrCus(np_img, cus_from_rgb)
    stain_1, stain_2, stain_3 = stainList
    img_1, img_2, img_3 = getNPImgFromStainsCus(np_img,\
                                                stain_1,\
                                                stain_2,\
                                                stain_3,\
                                                rgb_from_cus)

    #now rotating everything to the required angle (cause of QR)
    list2Rot=[stain_1, stain_2, stain_3,\
              np_img, img_1, img_2, img_3]

    rotList = getRotArraysList(list2Rot, angle)

    stain_1R, stain_2R, stain_3R,\
        np_imgR, img_1R, img_2R, img_3R = rotList
    stainListR = [stain_1R, stain_2R, stain_3R]
    #finished the rotating part.

    hBool = True
    for iCount, cROI in enumerate(centerROIs):
        centerROI = tuple(qrXUVector*cROI[0]+qrYUVector*cROI[1]+qrOrigin)
        centerROI = (int(centerROI[0]), int(centerROI[1]))

        #All (rotated) stain arrays have the same dimensions
        mask = getCircularMask(stain_1R, centerROI, myRadius)
        #function is fast, might be worthwile doing all the work b4
        #  printing
        stainListStats = [getStainStats(sV, mask) for sV in stainListR]
        resultDict = createStainStatDictionary(stainListStats, iCount)

        if "-C" in myOptDict:
            if hBool:
                print("#rNum\tconc\tcStd")
            S1, S1Std = stainListStats[0][0], stainListStats[0][1]

            out = getConcentration([S1, S1Std], myOptDict, server = server)
            if out != None:
                c, cStd = out[0], out[1]
                print("%d\t%2.2f\t%2.2f" % (i, c, cStd))
                resultDict["conc"] = c
                resultDict["cStd"] = cStd

            print("%d\t%2.2f\t%2.2f" % (iCount, c, cStd))
        else:
            printStainListStats(stainListStats, iCount, hBool)

        listOfResultDicts.append(resultDict)

        hBool = False
        colorTuple=(0, 0, 255)#Blue
        if '-m' in myOptDict:
            if iCount == myOptDict['-m']:
                colorTuple=(255,0,0)#Red
        if '--no-rect' not in myOptDict and '--no-ROI' not in myOptDict:
            np_imgR = cv2.circle(np_imgR, centerROI, radius=myRadius,\
                                color=colorTuple, thickness=2)

            img_1R = cv2.circle(img_1R, centerROI, radius=myRadius,\
                                color=colorTuple, thickness=2)

            img_2R = cv2.circle(img_2R, centerROI, radius=myRadius,\
                                color=colorTuple, thickness=2)

            img_3R = cv2.circle(img_3R, centerROI, radius=myRadius,\
                                color=colorTuple, thickness=2)

    np_imgR = np_imgR.astype('uint8')
    points = points.astype('int')
    if '--draw-axes' in myOptDict:
        cv2.arrowedLine(np_imgR, tuple(points[0]),\
                        tuple(points[1]), (255,0,0), 5)
        cv2.arrowedLine(np_imgR, tuple(points[0]),\
                        tuple(points[3]), (0,255,0), 5)
        cv2.arrowedLine(img_1R, tuple(points[0]),\
                        tuple(points[1]), (255,0,0), 5)
        cv2.arrowedLine(img_1R, tuple(points[0]),\
                        tuple(points[3]), (0,255,0), 5)
        cv2.arrowedLine(img_2R, tuple(points[0]),\
                        tuple(points[1]), (255,0,0), 5)
        cv2.arrowedLine(img_2R, tuple(points[0]),\
                        tuple(points[3]), (0,255,0), 5)
        cv2.arrowedLine(img_3R, tuple(points[0]),\
                        tuple(points[1]), (255,0,0), 5)
        cv2.arrowedLine(img_3R, tuple(points[0]),\
                        tuple(points[3]), (0,255,0), 5)

    return np_imgR, img_1R, img_2R, img_3R, listOfResultDicts

def getQRRefSystem(points):
    qrUnit = getQRUnit(points)

    qrOrigin = np.array(points[0])
    qrXVector = np.array(points[1])-qrOrigin
    qrYVector = np.array(points[3])-qrOrigin

    qrXUVector = qrXVector/np.linalg.norm(qrXVector)*qrUnit
    qrYUVector = qrYVector/np.linalg.norm(qrYVector)*qrUnit

    return qrUnit, qrOrigin, qrXVector, qrYVector, qrXUVector, qrYUVector

def rgb_to_hsv_vector(npArray):
    npArray = npArray.astype(np.double)
    r = npArray[0,:]/255.0
    g = npArray[1,:]/255.0
    b = npArray[2,:]/255.0
    mx = np.maximum(r, np.maximum(g, b))
    mn = np.minimum(r, np.minimum(g, b))
    df = mx-mn
    df = np.where(df != 0, df, 1e-9)
    h1 = np.where(mx == r, np.remainder((60 * ((g-b)/df) + 360), 360), 0)
    h2 = np.where(mx == g, np.remainder(60 * ((b-r)/df) + 120, 360), 0)
    h3 = np.where(mx == b, np.remainder(60 * ((r-g)/df) + 240,360), 0)
    h = h1 + h2 + h3
    s = np.where(mx != 0,(df/mx)*100,0)
    v = mx*100
    h = np.where(h >= 360, 360-h, h)
    v = np.where(v >= 100, 100, v)
    s = np.where(s >= 100, 100, s)
    h = np.where(h < 0, h+360, h)
    v = np.where(v < 0, 0, v)
    s = np.where(s < 0, 0, s)

    hsvArray = np.copy(npArray)*0
    hsvArray[0,:] = h
    hsvArray[1,:] = s
    hsvArray[2,:] = v
    return hsvArray

def rgb_to_hsv_array(npArray, normalized = False):
    """ calculates HSV from an RGB image.

    Input:
    npArray: a numpy array of dimension (x pixel count, y pixel count, 3) 
      containing RGB values in each layer

    Returns:
    hsvArray: a numpy array of the same dimension as npArray, containing the HSV
      values in each layer
    """

    npArray = npArray.astype(np.double)
    # normalise the arrays from 0 - 1 (instead of 0 2555)
    r = npArray[:,:,0]/255.0
    g = npArray[:,:,1]/255.0
    b = npArray[:,:,2]/255.0
    # for all pixels, find the maximal value across RGB
    mx = np.maximum(r, np.maximum(g, b))
    # for all pixels, find the minimal value across RGB
    mn = np.minimum(r, np.minimum(g, b))
    # construct the chroma
    df = mx-mn
    # get a mask where all df == 0 values are False
    mask_df = np.where(df == 0, False, True)

    if normalized:
        h1 = np.where(mx == r, np.remainder(((g-b)/df) + 6, 6), 0)
        h2 = np.where(mx == g, np.remainder(((b-r)/df) + 2, 6), 0)
        h3 = np.where(mx == b, np.remainder(((r-g)/df) + 4, 6), 0)
        h = h1 + h2 + h3
        s = np.where(mx != 0,(df/mx),0)
        v = mx
        h = np.where(h >= 1, h-1, h)
        v = np.where(v >= 1, 1, v)
        s = np.where(s >= 1, 1, s)
        h = np.where(h < 0, h+1, h)
        v = np.where(v < 0, 0, v)
        s = np.where(s < 0, 0, s) #FIXME

    else:
        # HUE
        # these masks are needed to combine the chroma (df !=0) condition with 
        #   selecting to which colour the maximum corresponds to
        mask_h1 = np.where(mx == r, True, False)
        mask_h2 = np.where(mx == g, True, False)
        mask_h3 = np.where(mx == b, True, False)
        h1 = np.where(mask_h1 & mask_df, np.remainder(60 * ((g-b)/df), \
                                                      360), 0) 
                                                      # + 360 was removed
        h2 = np.where(mask_h2 & mask_df, np.remainder(60 * ((b-r)/df) + 120, \
                                                      360), 0)
        h3 = np.where(mask_h3 & mask_df, np.remainder(60 * ((r-g)/df) + 240, \
                                                      360), 0)
        h = h1 + h2 + h3
        
        # Saturation
        s = np.where(mx != 0, (df/mx)*100, 0)
        
        # Value
        v = mx*100
        
        # these ones seem weird
        #h = np.where(h >= 360, 360-h, h)
        #v = np.where(v >= 100, 100, v)
        #s = np.where(s >= 100, 100, s)
        #h = np.where(h < 0, h+360, h)
        #v = np.where(v < 0, 0, v)
        #s = np.where(s < 0, 0, s)

        # warnings
        if np.max(h) > 360 or np.min(h) < 0:
            print(f"rgb_to_hsv_array::Error: h of {h} outside bounds. This may" + \
                  " blow up later")
        if np.max(s) > 100 or np.min(s) < 0:
            print(f"rgb_to_hsv_array::Error: s of {s} outside bounds. This may" + \
                  " blow up later")
        if np.max(v) > 100 or np.min(v) < 0:
            print(f"rgb_to_hsv_array::Error: v of {v} outside bounds. This may" + \
                  " blow up later")

    hsvArray = np.copy(npArray)*0
    hsvArray[:,:,0] = h
    hsvArray[:,:,1] = s
    hsvArray[:,:,2] = v
    return hsvArray

def rgb_to_hsv_mean_values(meanRGB, stdDevRGB, normalized = False):
    """ calculates HSV from an RGB image.

    Input:
    meanRGB: a numpy array with three (R,G,B) values 

    Returns:
    hsvArray: a numpy array of the same dimension as npArray, containing the HSV
      values in each layer
    """

    #meanRGB = meanRGB.astype(np.double)
    # normalise the arrays from 0 - 1 (instead of 0 2555)
    r = meanRGB[0]/255.0
    g = meanRGB[1]/255.0
    b = meanRGB[2]/255.0
    # for all pixels, find the maximal value across RGB
    mx = np.maximum(r, np.maximum(g, b))
    # for all pixels, find the minimal value across RGB
    mn = np.minimum(r, np.minimum(g, b))
    # construct the chroma
    df = mx-mn
    # get a mask where all df == 0 values are False
    mask_df = np.where(df == 0, False, True)

    # HUE
    # these masks are needed to combine the chroma (df !=0) condition with 
    #   selecting to which colour the maximum corresponds to
    mask_h1 = np.where(mx == r, True, False)
    mask_h2 = np.where(mx == g, True, False)
    mask_h3 = np.where(mx == b, True, False)

    mask_min_h1 = np.where(mn == r, True, False)
    mask_min_h2 = np.where(mn == g, True, False)
    mask_min_h3 = np.where(mn == b, True, False)
    
    h1 = np.where(mask_h1 & mask_df, np.remainder(60 * ((g-b)/df), \
                                                  360), 0) 
    # + 360 was removed
    h2 = np.where(mask_h2 & mask_df, np.remainder(60 * ((b-r)/df) + 120, \
                                                  360), 0)
    h3 = np.where(mask_h3 & mask_df, np.remainder(60 * ((r-g)/df) + 240, \
                                                  360), 0)
    h = h1 + h2 + h3
        
    # Saturation
    s = np.where(mx != 0, (df/mx)*100, 0)
        
    # Value
    v = mx*100

    # warnings
    if np.max(h) > 360 or np.min(h) < 0:
        print(f"rgb_to_hsv_array::Error: h of {h} outside bounds. This may" + \
              " blow up later")
    if np.max(s) > 100 or np.min(s) < 0:
        print(f"rgb_to_hsv_array::Error: s of {s} outside bounds. This may" + \
              " blow up later")
    if np.max(v) > 100 or np.min(v) < 0:
        print(f"rgb_to_hsv_array::Error: v of {v} outside bounds. This may" + \
              " blow up later")
              
              
    # Error propagation for HSV
    
    #Begin with errors for mx, mn and df

    if mask_h1 :
        mx_e = stdDevRGB[0]/255
    if mask_h2 :
        mx_e = stdDevRGB[1]/255
    if mask_h3 :
        mx_e = stdDevRGB[2]/255

    if mask_min_h1 :
        mn_e = stdDevRGB[0]/255
    if mask_min_h2 :
        mn_e = stdDevRGB[1]/255
    if mask_min_h3 :
        mn_e = stdDevRGB[2]/255

        
    df_e = np.sqrt(mx_e**2 + mn_e**2)
        
    # Hue errors
    
    #h1
    h1_e = np.where(mask_h1 & mask_df, np.sqrt(((1/(6*df))**2)*(stdDevRGB[1]**2) \
                         + ((-1/(6*df))**2)*(stdDevRGB[2]**2) + \
                         (((meanRGB[2] - meanRGB[1])/(6*(df**2)))**2)*(df_e**2)),0)
    #h2
    h2_e = np.where(mask_h2 & mask_df, np.sqrt(((1/(6*df))**2)*(stdDevRGB[2]**2) \
                         + ((-1/(6*df))**2)*(stdDevRGB[0]**2) + \
                         (((meanRGB[0] - meanRGB[2])/(6*(df**2)))**2)*(df_e**2)),0)
    #h3
    h3_e = np.where(mask_h3 & mask_df, np.sqrt(((1/(6*df))**2)*(stdDevRGB[0]**2) \
                         + ((-1/(6*df))**2)*(stdDevRGB[1]**2) + \
                         (((meanRGB[1] - meanRGB[0])/(6*(df**2)))**2)*(df_e**2)),0)

    print(h1_e, h2_e, h3_e)
    
    #Final Hue
    h_e = np.sqrt((h1_e**2) + (h2_e**2) + (h3_e**2))
    
    # Saturation errors
    s_e = np.where(mx != 0, s*np.sqrt(((df_e/df)**2)+ ((mx_e/mx)**2)), 0)
    
    # Value errors
    v_e = v*(mx_e/mx)
    
    # change the format    
    hsvMean = np.array([h,s,v])
    hsvStd = np.array([h_e,s_e,v_e])

    return hsvMean, hsvStd

def deconvMultiplotCus(img_rgb, img_1, img_2, img_3):
    fig, axes = plt.subplots(2, 2, figsize=(7, 6), sharex=True, sharey=True)

    ax = axes.ravel()

    ax[0].imshow(img_rgb.astype('uint8'))
    ax[0].set_title("Original image")

    ax[1].imshow(img_1)
    ax[1].set_title("Stain 1")

    ax[2].imshow(img_2)
    ax[2].set_title("Stain 2")

    ax[3].imshow(img_3)
    ax[3].set_title("Stain 3")

    for a in ax.ravel():
        a.axis('off')

    fig.tight_layout()

    return fig, ax

# def statsMaskedNpArray(npArray, Mask=None):
#     if np.any(Mask) == None:
#         Mask = np.ones(npArray.shape)

#     npArrayMasked = npArray*Mask
#     N = Mask.sum()
#     mean = npArrayMasked.sum()/N
#     auxMat = (npArrayMasked - mean*Mask)**2
#     stdDev = np.sqrt(auxMat.sum()/N)
#     return mean, stdDev, npArrayMasked, Mask

# def minmaxMaskedNpArray(npArray, Mask=None):
#     npMaskedArray = npArray*Mask
#     max = npMaskedArray.max()
#     auxMatrix = 1e12*(1-Mask)
#     npMaskedArray = np.where(auxMatrix > npMaskedArray, auxMatrix, npMaskedArray)
#     min = npMaskedArray.min()
#     min = min.astype(np.int64)
#     #max = max.astype(np.double)
#     return min, max

# def statsNpArrayRGB(npArray,Mask=None):
#     npArrayMaskedRGB = np.copy(npArray)*0
#     if len(Mask.shape) == 2:
#         MaskRGB = np.copy(npArray)*0
#         MaskRGB[:,:,0] = Mask
#         MaskRGB[:,:,1] = Mask
#         MaskRGB[:,:,2] = Mask
#     elif len(Mask.shape) == 3:
#         MaskRGB = np.copy(Mask)
#     meanL = []
#     stdDevL = []
#     npArrayMasked = []
#     for i in range(0,3,1):
#         mean, stdDev, npArrayMasked, _ = statsMaskedNpArray(npArray[:,:,i], Mask=MaskRGB[:,:,i])
#         meanL.append(mean)
#         stdDevL.append(stdDev)
#         npArrayMaskedRGB[:,:,i] = npArrayMasked

#     return meanL, stdDevL, npArrayMaskedRGB, MaskRGB

# def minmaxNpArrayRGB(npArray,Mask=None):
#     npArrayMaskedRGB = np.copy(npArray)*0
#     if len(Mask.shape) == 2:
#         MaskRGB = np.copy(npArray)*0
#         MaskRGB[:,:,0] = Mask
#         MaskRGB[:,:,1] = Mask
#         MaskRGB[:,:,2] = Mask
#     elif len(Mask.shape) == 3:
#         MaskRGB = np.copy(Mask)

#     minL = []
#     maxL = []

#     for i in range(0,3,1):
#         min, max = minmaxMaskedNpArray(npArray[:,:,i], Mask=MaskRGB[:,:,i])
#         minL.append(min)
#         maxL.append(max)

#     return minL, maxL

def doRectColorDeconvStuff(np_img, qrCodeData, myOptDict, rgb_from_cus, \
                           server = False):
    # Caveat: using the result dictionaries created here has not yet been tested
    #
    cus_from_rgb = np.linalg.inv(rgb_from_cus)
    angle=0
    listOfResultDicts = []

    if qrCodeData is not None:
        angle = qrCodeData[1]

    mySize=np_img.shape[:2]

    if '-r' not in myOptDict:
        #Just including it for keeping a consistent structural form
        fracHalf=0.15
        rectIdx=getRectIdx(mySize,fracHalf)
        myOptDict['-r']=rectIdx

    #Obtaining the corresponding 3 stains
    stainList = get3StainsArrCus(np_img, cus_from_rgb)
    stain_1, stain_2, stain_3 = stainList
    #Now the corresponding images
    img_1, img_2, img_3 = getNPImgFromStainsCus(np_img,\
                                                stain_1,\
                                                stain_2,\
                                                stain_3,\
                                                rgb_from_cus)

    counter = 0
    for i in range(len(myOptDict['-r'])//4):
        rectIdx = myOptDict['-r'][i*4:4*i+4]
        xMin, xMax, yMin, yMax = rectIdx
        #Now doing extra parsing not initially possible
        maxY, maxX = mySize
        if  not 0 < xMin < maxX or not 0 < xMax < maxX:
            print(xMin)
            print("error: x values are out of bounds")
            sys.exit()

        if  not 0 < yMin < maxY or not 0 < yMax < maxY:
            print("error: y values are out of bounds")
            sys.exit()

        deconvList = getDeconvList(stainList, rectIdx)
        stainListStats = deconvListToStainStats(deconvList)
        resultDict = createStainStatDictionary(stainListStats, counter)

        hBool = True
        if counter > 0:
            hBool = False

        if "-C" in myOptDict:
            if hBool:
                print("#rNum\tconc\tcStd")

            out = getConcentration(deconvList, myOptDict)
            if out != None:
                c, cStd = out[0], out[1]
                print("%d\t%2.2f\t%2.2f" % (i, c, cStd))
                resultDict["conc"] = c
                resultDict["cStd"] = cStd

            print("%d\t%2.2f\t%2.2f" % (i, c, cStd))
        else:
            printDeconvList(deconvList, i,  hBool)

        counter += 1

        if '--no-rect' not in myOptDict and '--no-ROI' not in myOptDict:
            np_img = drawNPRect(np_img, rectIdx)
            img_1 = drawNPRect(img_1, rectIdx)
            img_3 = drawNPRect(img_3, rectIdx)
            img_2 = drawNPRect(img_2, rectIdx)

        listOfResultDicts.append(resultDict)

    return np_img, img_1, img_2, img_3, listOfResultDicts

def printDeconvList(deconvList, idx,\
                    hBool=False, scale=100):
    if hBool:
        hStr = "#rNum\t"
        hStr += "s_1Av\ts_2Av\ts_3Av\t"
        hStr += "s_1Std\ts_2Std\ts_3Std\t"
        hStr += "s_1Min\ts_2Min\ts_3Min\t"
        hStr += "s_1Max\ts_2Max\ts_3Max"
        print(hStr)

    #Scaling these values
    deconvList = [sV*scale for sV in deconvList]
    s_1Av, s_2Av, s_3Av,\
        s_1Std, s_2Std, s_3Std,\
        s_1Min, s_2Min, s_3Min,\
        s_1Max, s_2Max, s_3Max = deconvList

    #Note: format is to be decided need 2 see with final matrix
    strFmt = "%d\t"+"%4.2f\t"*11+"%4.2f"
    print(strFmt % (idx, s_1Av, s_2Av, s_3Av,\
                    s_1Std, s_2Std, s_3Std,\
                    s_1Min, s_2Min, s_3Min,\
                    s_1Max, s_2Max, s_3Max))

def printStainListStats(stainListStats, idx,\
                        hBool=False, scale=100):
    if hBool:
        hStr = "#rNum\t"
        hStr += "s_1Av\ts_2Av\ts_3Av\t"
        hStr += "s_1Std\ts_2Std\ts_3Std\t"
        hStr += "s_1Min\ts_2Min\ts_3Min\t"
        hStr += "s_1Max\ts_2Max\ts_3Max"
        print(hStr)
    #Note: format is to be decided need 2 see with final matrix
    strFmt = "%d\t"+"%4.2f\t"*11+"%4.2f"

    s_1Stats, s_2Stats, s_3Stats = stainListStats

    s_1Stats = [sV*scale for sV in s_1Stats]
    s_2Stats = [sV*scale for sV in s_2Stats]
    s_3Stats = [sV*scale for sV in s_3Stats]

    s_1Av, s_2Av, s_3Av = s_1Stats[0], s_2Stats[0], s_3Stats[0]
    s_1Std, s_2Std, s_3Std = s_1Stats[1], s_2Stats[1], s_3Stats[1]
    s_1Min, s_2Min, s_3Min = s_1Stats[2], s_2Stats[2], s_3Stats[2]
    s_1Max, s_2Max, s_3Max = s_1Stats[3], s_2Stats[3], s_3Stats[3]

    print(strFmt % (idx, s_1Av, s_2Av, s_3Av,\
                    s_1Std, s_2Std, s_3Std,\
                    s_1Min, s_2Min, s_3Min,\
                    s_1Max, s_2Max, s_3Max))

# creates a dictionary of the stain values in an ROI from the stain list
def createStainStatDictionary(stainListStats, idx, scale = 100):
    """ creates a dictionary of the stain values found for an ROI according to the
    field names which are used by printStainListStats. It returns the dictionary

    * stainListStats: List with stain values for an ROI
    * idx: ID for the current ROI
    """
    keyList = ["rNum", \
        "s_1Av", "s_2Av", "s_3Av", \
        "s_1Std", "s_2Std", "s_3Std", \
        "s_1Min", "s_2Min", "s_3Min", \
        "s_1Max", "s_2Max", "s_3Max"]

    s_1Stats, s_2Stats, s_3Stats = stainListStats

    s_1Stats = [sV * scale for sV in s_1Stats]
    s_2Stats = [sV * scale for sV in s_2Stats]
    s_3Stats = [sV * scale for sV in s_3Stats]

    s_1Av, s_2Av, s_3Av = s_1Stats[0], s_2Stats[0], s_3Stats[0]
    s_1Std, s_2Std, s_3Std = s_1Stats[1], s_2Stats[1], s_3Stats[1]
    s_1Min, s_2Min, s_3Min = s_1Stats[2], s_2Stats[2], s_3Stats[2]
    s_1Max, s_2Max, s_3Max = s_1Stats[3], s_2Stats[3], s_3Stats[3]

    dataList = [ idx, s_1Av, s_2Av, s_3Av, \
                 s_1Std, s_2Std, s_3Std, \
                 s_1Min, s_2Min, s_3Min, \
                 s_1Max, s_2Max, s_3Max ]

    stainDictionary = {}

    for key, value in zip(keyList, dataList):
        stainDictionary[key] = value

    return stainDictionary

def getRotArraysList(list2Rot, angle):
    rotList = []
    for e in list2Rot:
        if angle == 0:
            rotList.append(e)
        else:
            rotList.append(ndimage.rotate(e, angle))

    return rotList

def getDeconvList(stainList,rectIdx):
    #unpacking the stains
    stain_1, stain_2, stain_3 = stainList
    #unpacking the ranges
    xMin,xMax,yMin,yMax=rectIdx
    #slicing to the corresponding region
    s_1 = stain_1[yMin:yMax,xMin:xMax]
    s_2 = stain_2[yMin:yMax,xMin:xMax]
    s_3 = stain_3[yMin:yMax,xMin:xMax]

    #flattening them for doing averages
    s_1Flat=s_1.flatten()
    s_2Flat=s_2.flatten()
    s_3Flat=s_3.flatten()

    s_1Av=np.average(s_1Flat)
    s_2Av=np.average(s_2Flat)
    s_3Av=np.average(s_3Flat)

    s_1Std=np.std(s_1Flat)
    s_2Std=np.std(s_2Flat)
    s_3Std=np.std(s_3Flat)

    s_1Min=np.amin(s_1Flat)
    s_2Min=np.amin(s_2Flat)
    s_3Min=np.amin(s_3Flat)

    s_1Max=np.amax(s_1Flat)
    s_2Max=np.amax(s_2Flat)
    s_3Max=np.amax(s_3Flat)

    deconvList=[s_1Av, s_2Av, s_3Av,\
                s_1Std, s_2Std, s_3Std,\
                s_1Min, s_2Min, s_3Min,\
                s_1Max, s_2Max, s_3Max]

    return deconvList

def deconvListToStainStats(deconv_list):
    """ The format of the deconv_list as returned by getDeconvList() does not fit
    with that is done in doQRColorDeconvStuff(..) using getStainStats(...) so we
    need to transform this list in order to process it with existing functions

    Returns: the deconv_list as a list of list (one list for every stain). Every
      sub list has the values: meanV, stdV, minV, maxV

    Input parameters:
    deconv_list: the deconvolution list
    """

    stainStatList = [[], [], []]

    counter = 0
    for element in deconv_list:
        if counter % 3 == 0:
            counter = 0

        stainStatList[counter].append(element)
        counter += 1

    return stainStatList

def getStainStats(stainV, mask):
    maskedStainArr = np.ma.array(stainV, mask=~mask)
    meanV = maskedStainArr.mean()
    stdV = maskedStainArr.std()
    minV = maskedStainArr.min()
    maxV = maskedStainArr.max()
    return [meanV, stdV, minV, maxV]

def hsv_to_rgb_vector(npArray):
    npArray = npArray.astype(np.double)
    h = npArray[0,:]
    s = npArray[1,:]/100.0
    v = npArray[2,:]/100.0
    c = v*s
    x = c*(1-np.abs(np.mod(h/60,2)-1))
    m = v - c

    rgbarray = np.copy(npArray)*0

    rgbarray[0,:] = np.where((h >= 0) ,(h < 60),rgbarray[0,:],c)
    rgbarray[0,:] = np.where((h >= 60) ,(h < 120),rgbarray[0,:],x)
    rgbarray[0,:] = np.where((h >= 120) ,(h < 180),rgbarray[0,:],0)
    rgbarray[0,:] = np.where((h >= 180) ,(h < 240),rgbarray[0,:],0)
    rgbarray[0,:] = np.where((h >= 240) ,(h < 300),rgbarray[0,:],x)
    rgbarray[0,:] = np.where((h >= 300) ,(h < 360),rgbarray[0,:],c)

    rgbarray[1,:] = np.where((h >= 0) ,(h < 60),rgbarray[1,:],x)
    rgbarray[1,:] = np.where((h >= 60) ,(h < 120),rgbarray[1,:],c)
    rgbarray[1,:] = np.where((h >= 120) ,(h < 180),rgbarray[1,:],c)
    rgbarray[1,:] = np.where((h >= 180) ,(h < 240),rgbarray[1,:],x)
    rgbarray[1,:] = np.where((h >= 240) ,(h < 300),rgbarray[1,:],0)
    rgbarray[1,:] = np.where((h >= 300) ,(h < 360),rgbarray[1,:],0)

    rgbarray[2,:] = np.where((h >= 0) ,(h < 60),rgbarray[2,:],0)
    rgbarray[2,:] = np.where((h >= 60) ,(h < 120),rgbarray[2,:],0)
    rgbarray[2,:] = np.where((h >= 120) ,(h < 180),rgbarray[2,:],x)
    rgbarray[2,:] = np.where((h >= 180) ,(h < 240),rgbarray[2,:],c)
    rgbarray[2,:] = np.where((h >= 240) ,(h < 300),rgbarray[2,:],c)
    rgbarray[2,:] = np.where((h >= 300) ,(h < 360),rgbarray[2,:],x)
    rgbarray[0,:] += m
    rgbarray[1,:] += m
    rgbarray[2,:] += m

    rgbarray *= 255
    return rgbarray

def hsv_to_rgb_array(npArray):
    npArray = npArray.astype(np.double)
    h = npArray[:,:,0]
    s = npArray[:,:,1]/100.0
    v = npArray[:,:,2]/100.0
    c = v*s
    x = c*(1-np.abs(np.mod(h/60,2)-1))
    m = v - c

    rgbarray = np.copy(npArray)*0

    rgbarray[:,:,0] = np.where(np.logical_and((h >= 0),   (h < 60)),   c,rgbarray[:,:,0])
    rgbarray[:,:,0] = np.where(np.logical_and((h >= 60),  (h < 120)), x,rgbarray[:,:,0])
    rgbarray[:,:,0] = np.where(np.logical_and((h >= 120), (h < 180)),0,rgbarray[:,:,0])
    rgbarray[:,:,0] = np.where(np.logical_and((h >= 180), (h < 240)),0,rgbarray[:,:,0])
    rgbarray[:,:,0] = np.where(np.logical_and((h >= 240), (h < 300)),x,rgbarray[:,:,0])
    rgbarray[:,:,0] = np.where(np.logical_and((h >= 300), (h < 360)),c,rgbarray[:,:,0])

    rgbarray[:,:,1] = np.where(np.logical_and((h >= 0) ,(h < 60)),   x,rgbarray[:,:,1])
    rgbarray[:,:,1] = np.where(np.logical_and((h >= 60) ,(h < 120)), c,rgbarray[:,:,1])
    rgbarray[:,:,1] = np.where(np.logical_and((h >= 120) ,(h < 180)),c,rgbarray[:,:,1])
    rgbarray[:,:,1] = np.where(np.logical_and((h >= 180) ,(h < 240)),x,rgbarray[:,:,1])
    rgbarray[:,:,1] = np.where(np.logical_and((h >= 240) ,(h < 300)),0,rgbarray[:,:,1])
    rgbarray[:,:,1] = np.where(np.logical_and((h >= 300) ,(h < 360)),0,rgbarray[:,:,1])
    rgbarray[:,:,2] = np.where(np.logical_and((h >= 0) ,(h < 60)),   0,rgbarray[:,:,2])
    rgbarray[:,:,2] = np.where(np.logical_and((h >= 60) ,(h < 120)), 0,rgbarray[:,:,2])
    rgbarray[:,:,2] = np.where(np.logical_and((h >= 120) ,(h < 180)),x,rgbarray[:,:,2])
    rgbarray[:,:,2] = np.where(np.logical_and((h >= 180) ,(h < 240)),c,rgbarray[:,:,2])
    rgbarray[:,:,2] = np.where(np.logical_and((h >= 240) ,(h < 300)),c,rgbarray[:,:,2])
    rgbarray[:,:,2] = np.where(np.logical_and((h >= 300) ,(h < 360)),x,rgbarray[:,:,2])
    rgbarray[:,:,0] += m
    rgbarray[:,:,1] += m
    rgbarray[:,:,2] += m

    rgbarray *= 255
    return rgbarray
