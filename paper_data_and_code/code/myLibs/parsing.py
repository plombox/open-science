import sys
import os
from myLibs.redMisc import *
from pathlib import Path
import numpy as np

#some options are either unimplemented or need to be removed.
#options were grouped cause they are equivalent.
preAccOpts=[['-h', '--help'],\
            ['-c', '--command'],\
            ['-f', '--fetch'],\
            ['-o', '--open'],\
            ['--sampleMatrix'],\
            ['-r', '--rect'],\
            ['--QR'],\
            ['--average'],\
            ['-p','--port'],\
            ['-b', '--baudrate'],\
            ['-t', '--timeout'],\
            ['-s', '--save'],\
            ['-a', '--analysis'],\
            ['-d', '--deconvolution'],\
            ['-q','--quick','--fast'],\
            ['-w','--wiener'],\
            ['-i','--intensity'],\
            ['--no-show'],\
            ['--no-rect'],\
            ['--no-ROI'],\
            ['--save-jpg'],\
            ['--flatfield'],\
            ['--rgb'],\
            ['--print-json'],\
            ['--print-json-p'],\
            ['--print-raw'],\
            ['--shift'],\
            ['-Q','--quickMatrix'],\
            ['-m'],\
            ['-R'],\
            ['--draw-axes'],\
            ['--refImg'],\
            ['--hed'],\
            ['--mau'],\
            ['--mau2'],\
            ['--maca0'],\
            ['--10Hnov9'],\
            ['--ONnov9'],\
            ['--cellNov23'],\
            ['--espNov23'],\
            ['--espNov25'],\
            ['--sampleDec24'],\
            ['--dMatrix'],\
            ['--mFile'],\
            ['-C', '--concentration'],\
            ['--C0'],\
            ['--C1'],\
            ['--C2'],\
            ['--C3'],\
            ['--CDummy'],\
            #the second form options
            ['--S1'],\
            ['--S2'],\
            ['--S3'],\
            ['-D'],\
            ['-O'],\
            #These are currently unused
            ['--noStats'],\
            ['--rectS'],\
            ['--oldStats'],\
            ['--offsetX'],\
            ['--offsetY']]

accOpts=[ee for e in preAccOpts for ee in e]
def par_getPrefDict(preAccOpts):
    """Returns a dictionary with the preferred option forms, those are the
first elements of preAccOpts

    """
    prefDict = {}
    for pL in preAccOpts:
        for p in pL:
            prefDict[p] = pL[0]

    return prefDict

def par_getPrefOptDict(myOptDict, preAccOpts):
    prefD = par_getPrefDict(preAccOpts)#A dictionary for using the preferred forms
    newOptDict = {}
    for e in myOptDict:
        if e in prefD:
            newOptDict[prefD[e]] = myOptDict[e]
    return newOptDict

def par_getMyOptDict(myArgs):
    myOptDict={}
    tmpOpt=''
    for i in range(len(myArgs)):
        e=myArgs[i]
        if e == '':#makes some scripting easier (empty strings)
            continue
        if e[0] == '-' and not isFloat(e): #only changes if new option
                                           #is found negative numbers
                                           #are not options
            if e not in myOptDict:
                myOptDict[e]=[]
            tmpOpt=e
            continue #Just skipping the option
        if tmpOpt == '':
            #Ignoring for now everything that is not an option
            continue

        myOptDict[tmpOpt].append(myArgs[i])

    return myOptDict

def par_checkIfValidOpts(myOptDict, accOpts):
    """Just checks if the options are valid."""
    for e in myOptDict:
        #Will leave as is for now
        if e not in accOpts:
            print("error: %s is not a valid option" %(e))
            return False

    return True

def par_pOD(mOD):
    """Receives the option dictionary assigns the proper values for each
option, it processes them"""
    if '--sampleMatrix' in mOD:
        print("Creating a sample matrix file called sampleMatrix.tsv with the following:")
        mStr="""0.737540710923858	0.470772547662308	0.484155871695749
0.164307477432839	0.164307477432839	0.972628452040816
0.784733839021013	0.100217817775556	0.611677358495186
"""
        print(mStr)

        with open("sampleMatrix.tsv", 'w') as f:
            f.write(mStr)

        print("Please look at the file located under the miscellaneousDir called mausBase.ods")
        print("In order to create your own matrix for later pasting it into your tsv file.")
        sys.exit()


    if '-c' in mOD:
        if len(mOD['-c']) != 1:
            print("error: -c option needs a single argument (put it in single or double quotes)")
            sys.exit()

    if '-f' in mOD:
        if len(mOD['-f']) != 1:
            print("error: -f needs a single argument")
            sys.exit()

    if '-o' in mOD:
        if len(mOD['-o']) != 1:
            print("error: -o option needs a single argument")
            sys.exit()
        myVal=mOD['-o'][0]
        if not os.path.isfile(myVal):
            print("error: file %s does not exist" %(myVal))
            sys.exit()
        accFilenamesO = ['.jpg', '.jpeg','.JPG', '.png', '.PNG', '.json', '.jso']
        fileTerm = list(filter(myVal.endswith, accFilenamesO))
        if fileTerm == []:
            print("error: invalid format, this option supports the following:")
            for e in accFilenamesO:
                print(e)
            sys.exit()

    if '-r' in mOD:
        if (len(mOD['-r'])) < 4:
            print("error: -r needs at least 4 arguments")
            sys.exit()

        if (len(mOD['-r']) % 4) != 0:
            print("error: -r needs a multiple of 4 in arguments")
            sys.exit()
        rList = mOD['-r']
        newList = []
        for v in rList:
            if not v.isdigit():
                print("error: -r needs positive integers")
                sys.exit()
            newList.append(int(v)) # new list with integers
            if int(v) < 0:
                print("error: -r needs positive integers")
                sys.exit()

        for i in range(len(mOD['-r'])//4):
            if newList[4*i] > newList[4*i+1]:
                print("error: -r xMin < xMax has to be true")
                sys.exit()

            if newList[4*i+2] > newList[4*i+3]:
                print("error: -r yMin < yMax has to be true")
                sys.exit()

        mOD['-r'] = newList


    if '--average' in mOD:
        if len(mOD['--average']) < 1:
            print("error: --average option needs at least one argument")
            sys.exit()
        accFilenames = ['.jpg', '.jpeg','.JPG', '.json', '.jso']
        myVal = mOD['--average'][0]
        firstEndL = list(filter(myVal.endswith, accFilenames))
        if firstEndL == []:
            print("error: filenames have to end with one of the following")
            for accF in accFilenames:
                print(accF)
            sys.exit()
        firstEnd = firstEndL[0]
        for f in mOD['--average']:
            if not os.path.isfile(f):
                print("error: file %s does not exist" %(f))
                sys.exit()

            if not os.path.isfile(f):
                print("error: file %s does not exist" %(f))
                sys.exit()

            if not f.endswith(firstEnd):
                print("error: all files need the same termination")
                sys.exit()


    if '-p' in mOD:
        if len(mOD['-p']) != 1:
            print("error: -p needs a single argument")
            sys.exit()
        mOD['-p'] = mOD['-p'][0]

    if '-b' in mOD:
        if len(mOD['-b']) != 1:
            print("error: -b needs a single argument")
            sys.exit()
        myVal=mOD['-b'][0]
        if not myVal.isdigit():
            print("error: -b needs a positive integer")
            sys.exit()
        myVal = int(myVal)
        mOD['-b'] = myVal

    if '-t' in mOD:
        if len(mOD['-t']) != 1:
            print("error: -t needs a single argument")
            sys.exit()
        myVal=mOD['-t'][0]
        if not myVal.isdigit():
            print("error: -t needs a positive integer")
            sys.exit()
        myVal = int(myVal)
        mOD['-t'] = myVal

    if '-s' in mOD:
        if len(mOD['-s']) != 1:
            print("error: -s needs a single argument")
            sys.exit()
        mOD['-s'] = mOD['-s'][0]

    if '-a' in mOD:
        if len(mOD['-a']) != 0:
            print("error: -a needs no arguments")
            sys.exit()

        if '-d' in mOD:
            print("error: -a is incompatible with -d")
            sys.exit()

    if '-d' in mOD:
        if len(mOD['-d']) != 0:
            print("error: -d needs no arguments")
            sys.exit()

        if '-a' in mOD:
            print("error: -d is incompatible with -a")
            sys.exit()

    if '-C' in mOD:
        if len(mOD['-C']) != 0:
            print("error: -C needs no arguments")
            sys.exit()

        if '-a' not in mOD and '-d' not in mOD:
            print("error: -C requires either -a or -d")
            sys.exit()

    if '--C0' in mOD:
        if '-C' not in mOD:
            print("error: --C0 requires the -C option")
            sys.exit()

    if '--C1' in mOD:
        if '-C' not in mOD:
            print("error: --C1 requires the -C option")
            sys.exit()

    if '--C2' in mOD:
        if '-C' not in mOD:
            print("error: --C2 requires the -C option")
            sys.exit()

    if '--C3' in mOD:
        if '-C' not in mOD:
            print("error: --C3 requires the -C option")
            sys.exit()

    if '--CDummy' in mOD:
        if '-C' not in mOD:
            print("error: --CDummy requires the -C option")
            sys.exit()

    if '--hed' in mOD:
        if len(mOD['--hed']) != 0:
            print("error: --hed needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --hed needs the -d option")
            sys.exit()

    if '--mau' in mOD:
        if len(mOD['--mau']) != 0:
            print("error: --mau needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --mau needs the -d option")
            sys.exit()

    if '--mau2' in mOD:
        if len(mOD['--mau2']) != 0:
            print("error: --mau2 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --mau2 needs the -d option")
            sys.exit()

    if '--maca0' in mOD:
        if len(mOD['--maca0']) != 0:
            print("error: --maca0 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --maca0 needs the -d option")
            sys.exit()

    if '--10Hnov' in mOD:
        if len(mOD['--10Hnov']) != 0:
            print("error: --10Hnov needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --10Hnov needs the -d option")
            sys.exit()

    if '--ONnov9' in mOD:
        if len(mOD['--ONnov9']) != 0:
            print("error: --ONnov9 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --ONnov9 needs the -d option")
            sys.exit()

    if '--cellNov23' in mOD:
        if len(mOD['--cellNov23']) != 0:
            print("error: --cellNov23 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --cellNov23 needs the -d option")
            sys.exit()

    if '--espNov23' in mOD:
        if len(mOD['--espNov23']) != 0:
            print("error: --espNov23 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --espNov23 needs the -d option")
            sys.exit()

    if '--espNov25' in mOD:
        if len(mOD['--espNov25']) != 0:
            print("error: --espNov25 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --espNov25 needs the -d option")
            sys.exit()

    if '--sampleDec24' in mOD:
        if len(mOD['--sampleDec24']) != 0:
            print("error: --sampleDec24 needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --sampleDec24 needs the -d option")
            sys.exit()

    if '--dMatrix' in mOD:
        if len(mOD['--dMatrix']) != 0:
            print("error: --dMatrix needs no arguments")
            sys.exit()

        if '-d' not in mOD:
            print("error: --dMatrix needs the -d option")
            sys.exit()

    if '--mFile' in mOD:
        if len(mOD['--mFile']) != 1:
            print("error: --mFile needs exactly 1 argument")
            sys.exit()

        myVal=mOD['--mFile'][0]
        if not os.path.isfile(myVal):
            print("error: file %s does not exist" %(myVal))
            sys.exit()

        try:
            rgb_from_cus = np.loadtxt(myVal, dtype=float)
        except:
            print("error: --mFile, couldn't load the matrix")
            sys.exit()

        if rgb_from_cus.shape != (3,3):
            print("error: %s does not have a 3x3 matrix!" %(myVal))
            sys.exit()

        if '-d' not in mOD:
            print("error: --mFile needs the -d option")
            sys.exit()

    if '--no-show' in mOD:
        if len(mOD['--no-show']) != 0:
            print("error: --no-show needs no arguments")
            sys.exit()

    if '--no-rect' in mOD:
        if len(mOD['--no-rect']) != 0:
            print("error: --no-rect needs no arguments")
            sys.exit()

    if '--no-ROI' in mOD:
        if len(mOD['--no-ROI']) != 0:
            print("error: --no-ROI needs no arguments")
            sys.exit()

    if '--save-jpg' in mOD:
        if len(mOD['--save-jpg']) != 1:
            print("error: --save-jpg needs a single argument")
            sys.exit()

        myVal=mOD['--save-jpg'][0]

        if not myVal.endswith(".jpg"):
            print("error: --save-jpg's argument has to end with .jpg")
            sys.exit()

        mOD['--save-jpg'] = myVal

    if '--flatfield' in mOD:
        if len(mOD['--flatfield']) != 1:
            print("error: --flatfield needs a single argument")
            sys.exit()

        myVal=mOD['--flatfield'][0]

        accFilenamesO = ['.jpg', '.jpeg','.JPG', '.png', '.PNG', '.json', '.jso']
        fileTerm = list(filter(myVal.endswith, accFilenamesO))
        if fileTerm == []:
            print("error: --flatfield's argument has to end with any of the following")
            for e in accFilenamesO:
                print(e)
            sys.exit()

        if not os.path.isfile(myVal):
            print("error: file %s does not exist" %(myVal))
            sys.exit()

        if '-o' not in mOD and '--open' not in mOD\
           and '--rgb' not in mOD:
            print("error: the --flatfield needs either the -o or --rgb options")
            sys.exit()

        mOD['--flatfield'] = myVal

    if '--QR' in mOD:
        if len(mOD['--QR']) != 0:
            print("error: --QR needs no arguments")
            sys.exit()

        if '-r' in mOD:
            print("error: --QR is incompatible with the -r option")
            sys.exit()

        if '-o' not in mOD:
            print("error: --QR can only be used with the -o option at the moment.")
            sys.exit()

    if '-Q' in mOD:
        if len(mOD['-Q']) != 1:
            print("error: -Q needs a single argument")
            sys.exit()

        myVal = mOD['-Q'][0]

        if not myVal.endswith('.tsv'):
            print("error: -Q's filename has to be a tsv")
            sys.exit()

        if '-a' not in mOD:
            print("error: -Q can only be used with the -a option at the moment.")
            sys.exit()


    if '--shift' in mOD:
        if '--QR' not in mOD:
            print("error: --shift can only be used if --QR is present")
            sys.exit()

        if len(mOD['--shift']) < 2:
            print("error: --shift needs at leas 2 arguments")
            sys.exit()

        if (len(mOD['--shift']) % 2) != 0:
            print("error: --shift needs a multiple of 2 arguments")
            sys.exit()

        sList = mOD['--shift']
        newList = []
        for v in sList:
            if not isFloat(v):
                print("error: --shift needs float values")
                sys.exit()
            newList.append(float(v)) # new list with floats

        #Note: since the ranges from the image changes with rotations
        #(when the default QR approach fails), and the QR itself
        #hasn't been identified, I can't check if the ROIs are within
        #the boundaries of the image

        #Making it a numpy array for easy manipulation.

        newNpArray = np.array(newList, dtype=float).reshape(len(newList)//2, 2)

        mOD['--shift'] = newNpArray


    if '-m' in mOD:
        if '--QR' not in mOD:
            print("error: -m can only be used if --QR is present")
            sys.exit()

        if len(mOD['-m']) != 1:
            print("error: -m needs exactly 1 argument")
            sys.exit()

        if not mOD['-m'][0].isdigit():
            print("error: -m needs an integer")
            sys.exit()

        myVal=int(mOD['-m'][0])

        if myVal < 0:
            print("error: -m needs a non-negative integer")
            sys.exit()

        maxMainIdx=0
        if "--shift" in mOD:
            maxMainIdx=len(mOD["--shift"])-1

        if myVal > maxMainIdx:
            print("error: the -m maximum value of %d was exceeded" %(maxMainIdx))
            sys.exit()

        mOD['-m']=myVal

    if '-R' in mOD:
        if '--QR' not in mOD:
            print("error: -R option requires --QR")
            sys.exit()

        if len(mOD['-R']) != 1:
            print("error: -R requires a single argument")
            sys.exit()

        R=mOD['-R'][0]
        if not isFloat(R):
            print("error: -R requires a float")
            sys.exit()

        R = float(R)
        if R <= 0:
            print("error: -R requires a positive value")
            sys.exit()

        mOD['-R']=R

    if '--draw-axes' in mOD:
        if '--QR' not in mOD:
            print("error: --draw-axes option requires --QR")
            sys.exit()

        if len(mOD['--draw-axes']) != 0:
            print("error: -R requires no arguments")
            sys.exit()

    if '--refImg' in mOD:
        if len(mOD['--refImg']) != 1:
            print("error: --refImg option needs a single argument")
            sys.exit()
        myVal=mOD['--refImg'][0]
        if not os.path.isfile(myVal):
            print("error: file %s does not exist" %(myVal))
            sys.exit()
        accFilenamesO = ['.jpg', '.jpeg','.JPG', '.png', '.PNG', '.json', '.jso']
        fileTerm = list(filter(myVal.endswith, accFilenamesO))
        if fileTerm == []:
            print("error: invalid format, this option supports the following:")
            for e in accFilenamesO:
                print(e)
            sys.exit()

    if '--S1' in mOD or '--S2' in mOD or '--S3' in mOD:
        if not ('--S1' in mOD and '--S2' in mOD and '--S3' in mOD):
            print("error: --S(i) needs the 3 options used at the same time.")
            sys.exit()

        if len(mOD["--S1"]) != 3:
            print("error: --S1 needs exactly 3 values")
            sys.exit()
        if len(mOD["--S2"]) != 3:
            print("error: --S2 needs exactly 3 values")
            sys.exit()
        if len(mOD["--S3"]) != 3:
            print("error: --S3 needs exactly 3 values")
            sys.exit()

        s1List=[]
        for e in mOD["--S1"]:
            if not isFloat(e):
                print("error: --S1 needs exactly 3 float values")
                sys.exit()
            if not 0 <= float(e) <= 255:
                print("error: values need to be positive and at most 255")
                sys.exit()
            s1List.append(float(e))
        mOD["--S1"]=s1List

        s2List=[]
        for e in mOD["--S2"]:
            if not isFloat(e):
                print("error: --S2 needs exactly 3 float values")
                sys.exit()
            if not 0 <= float(e) <= 255:
                print("error: values need to be positive and at most 255")
                sys.exit()
            s2List.append(float(e))
        mOD["--S2"]=s2List

        s3List=[]
        for e in mOD["--S3"]:
            if not isFloat(e):
                print("error: --S3 needs exactly 3 float values")
                sys.exit()
            if not 0 <= float(e) <= 255:
                print("error: values need to be positive and at most 255")
                sys.exit()
            s3List.append(float(e))
        mOD["--S3"]=s3List


    if '-D' in mOD:
        if '--S1' not in mOD:
            print("error: -D needs the --S(i) options")
            sys.exit()

        if len(mOD["-D"]) != 3:
            print("error: -D needs exactly 3 values")
            sys.exit()

        dList=[]
        for e in mOD["-D"]:
            if not isFloat(e):
                print("error: -D needs exactly 3 float values")
                sys.exit()
            if not 0 <= float(e) <= 255:
                print("error: values need to be positive and at most 255")
                sys.exit()
            dList.append(float(e))
        mOD["-D"]=dList

    if "-O" in mOD:
        if '--S1' not in mOD:
            print("error: -O needs the --S(i) options")
            sys.exit()

        if len(mOD["-O"]) != 1:
            print("error: -O option needs exactly 1 argument")
            sys.exit()

        myFName = mOD["-O"][0]
        if not myFName.endswith(".tsv"):
            print("error: -O file has to end with .tsv")
            sys.exit()

    if '--rgb' in mOD:
        if len(mOD['--rgb']) != 3:
            print("error: --rgb needs exactly 3 arguments")
            sys.exit()

        for v in mOD['--rgb']:
            accFilenamesO = ['.jpg', '.jpeg','.JPG', '.png', '.PNG', '.json', '.jso']
            fileTerm = list(filter(v.endswith, accFilenamesO))
            if fileTerm == []:
                print("error: --rgb's arguments have to end with any of the following")
                for e in accFilenamesO:
                    print(e)
                sys.exit()

                if not os.path.isfile(v):
                    print("error: file %s does not exist" %(v))
                    sys.exit()

        if '-o' in mOD or '--open' in mOD:
            print("error: the --rgb conflicts with the -o option")
            sys.exit()

        # mOD['--rgb'] = myVal

    if '--print-json' in mOD:
        if len(mOD['--print-json']) != 0:
            print("error: --print-json needs no arguments")
            sys.exit()

    if '--print-json-p' in mOD:
        if len(mOD['--print-json-p']) != 0:
            print("error: --print-json-p needs no arguments")
            sys.exit()

    if '--print-raw' in mOD:
        if len(mOD['--print-raw']) != 0:
            print("error: --print-raw needs no arguments")
            sys.exit()

    if '--offsetX' in mOD:
        if len(mOD['--offsetX']) != 1:
            print("error: --offsetX option needs a single argument")
            sys.exit()
        myVal=mOD['--offsetX'][0]
        if not myVal.lstrip("-").isdigit():
            print("error: --offsetX needs an integer")
            sys.exit()
        myVal = int(myVal)
        mOD['--offsetX'] = myVal

    if '--offsetY' in mOD:
        if len(mOD['--offsetY']) != 1:
            print("error: --offsetY option needs a single argument")
            sys.exit()
        myVal=mOD['--offsetY'][0]
        if not myVal.lstrip("-").isdigit():
            print("error: --offsetY needs an integer")
            sys.exit()
        myVal = int(myVal)
        mOD['--offsetY'] = myVal

    return mOD
