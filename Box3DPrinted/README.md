# Print Settings

## Piece: cuerpo V.I 3-base

![](images/image9.jpg)

> Material: PLA
>
> Amount of material: 48g
>
> Time: 3 hours 52 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: cuerpo V.I 3-cuerpo

![](images/image5.jpg)

> Material: PLA
>
> Amount of material: 124g
>
> Time: 10 hours 53 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: cuerpo V.I 3-tapa

![](images/image1.jpg)

> Material: PLA
>
> Amount of material: 41g
>
> Time: 3 hours 45 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: PuertaMuestra_8Cavidades

![](images/image6.jpg)

> Material: PLA
>
> Amount of material: 39g
>
> Time: 2 hours 56 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: PuertaMuestra_8Cavidades_tapa

![](images/image8.jpg)

> Material: PLA
>
> Amount of material: 5g
>
> Time: 32 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: PuertaMuestra_16Cavidades
![](images/image7.jpg)

> Material: PLA
>
> Amount of material: 46g
>
> Time: 3 hours 41 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: PuertaMuestra_16Cavidades_tapa

![](images/image2.jpg)

> Material: PLA
>
> Amount of material: 35g
>
> Time: 15 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: Lamina_difusor_0.4mm / 0.6mm

![](images/image4.jpg)

> Material: PLA
>
> Amount of material: 1g
>
> Time: 4 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: Difusor_conalojamiento

![](images/image10.jpg)

> Material: PLA
>
> Amount of material: 6g
>
> Time: 33 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.

## Piece: SoportePCB_crosca

![](images/image3.jpg)

> Material: PLA
>
> Amount of material: 22g
>
> Time: 2 hours 4 minutes
>
> Layer Height: 0.2mm
>
> Line Width: 0.4mm
>
> Top/Bottom Layers:4
>
> Infill Density: 30%
>
> Initial Printing Temp: 205°C
>
> Final Printing Temp: 195°C
>
> Build Plate Temp: 55°C
>
> Support: No.
