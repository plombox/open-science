#!/usr/bin/env python3

import sys
import plomboxtalk as Pb
from PIL import Image
import numpy as np

comm_port=1
device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

input("Ready for calibration. Please insert a white paper and press enter to continue")

if comm_port:
    # initialize serial device
    Pb.init(device)
else:
    Pb.btinit()

# set correct UTC time
Pb.settime(verbose=True,comm_port=comm_port)

# Erase all previous files if needed
for i in Pb.ls(verbose=True,comm_port=comm_port):
    Pb.erasefile(i,verbose=True,comm_port=comm_port)

# set camera to default config with lens correction and exposure 100
Pb.camconfig(conf={"lenc":"1","aec_value":"100"},verbose=True,comm_port=comm_port)

col=["r","g","b"]
# take r,g,b colors
cal=[100,100,100]
for i in range(3):
    param={"r":"0","g":"0","b":"0"}
    param[col[i]]="255"
    Pb.takepict(param=param,verbose=True,comm_port=comm_port)
    for f in Pb.ls(comm_port=comm_port):
        Pb.getjsonfile(f,verbose=True,basename=col[i],comm_port=comm_port)
    # now analyse the image obtained
    img=Image.open(col[i]+".jpg").convert('RGB')
    # calibration value aims at <img>=200, compared to aec_value=100
    cal[i]=int(100*200/np.mean(np.array(img)[:,:,i]))

input("Calibration done. Please insert object to image and press enter to continue")
# now that we have the calibrated values, take "2" images, one normal, one calibrated
Pb.camconfig(conf={"lenc":"1","aec_value":str(int(np.mean(cal)))},verbose=True,comm_port=comm_port)
Pb.takepict(verbose=True,comm_port=comm_port)
for i in Pb.ls(comm_port=comm_port):
    Pb.getjsonfile(i,verbose=True,basename="original",comm_port=comm_port)

# take the 3 RGB images and combine them
final=np.array(img)
for i in range(3):
    Pb.camconfig(conf={"lenc":"1","aec_value":str(cal[i])},verbose=True,comm_port=comm_port)
    param={"r":"0","g":"0","b":"0"}
    param[col[i]]="255"
    Pb.takepict(param=param,verbose=True,comm_port=comm_port)
    for f in Pb.ls(comm_port=comm_port):
        Pb.getjsonfile(f,verbose=True,basename=col[i],comm_port=comm_port)
    img=Image.open(col[i]+".jpg").convert('RGB')
    final[:,:,i]=np.array(img)[:,:,i]

img = Image.fromarray(final, 'RGB')
img.save("final.jpg")
