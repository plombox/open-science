import plomboxtalk as Pb
import bluetooth
import time

def scan():
    print("Scanning for bluetooth devices:")
    devices = bluetooth.discover_devices(lookup_names = True)
    number_of_devices = len(devices)
    if number_of_devices:
        print(number_of_devices,"devices found")
        for device in devices:
            if str(device[1]).startswith("pb-"):
                print("Found one Plombox device!")
                print("Connecting...")
                return device[0]
    else:
        print("No Plombox devices found")
        return None

def btinit():
    pbaddr=scan()
    if pbaddr is None:
        quit()
    else:
        try:
            Pb.btsoc.connect((pbaddr,1))
            print("Connected!")
        except Exception as e:
            print("Error in serial connection: ",e)
            quit()

def btpwrite(msg,verbose=False):
    if verbose:
        print(" >> Sending command: "+str(msg))
    try :
        Pb.btsoc.sendall(str(msg).encode())
    except socket.error:
        print( 'Send failed')
        quit()

def btpread(verbose=False,timeout=2):
    Pb.btsoc.setblocking(0)
    total_data='';
    data='';
    begin=time.time()
    while 1:
        if total_data and time.time()-begin > timeout:
            break
        elif time.time()-begin > timeout*2:
            break
        try:
            data = Pb.btsoc.recv(8192)
            if data:
                total_data+=data.decode()
                begin=time.time()
            else:
                time.sleep(0.1)
        except:
            pass

    if verbose:
        for line in total_data.splitlines():
            print("  <<",line)

    return total_data

