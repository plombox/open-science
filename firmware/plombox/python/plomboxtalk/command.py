import base64
import time
import json
import plomboxtalk as Pb

def filedump(jout,jname,verbose=False):
    with open(jname, 'w') as fo:
        fo.write(str(jout))

def imagesave(jout,verbose=False,basename=None):
    if basename is None:
        dname=jout["esp32"]["ID"]
        date=jout["telemetry"]["date"]
        date=date.replace(" ","_")
        date=date.replace("/","_")
        date=date.replace(":","_")
        date=date.replace("-","_")
        basename=dname+"_"+date
    fname=basename+".jpg"
    jname=basename+".json"
    if (jout["measurement"]["image"]):
        with open(fname, 'wb') as fo:
            img=base64.b64decode(jout["measurement"]["image"])
            fo.write(img)
            if verbose:
                print("File saved to "+fname)
    else:
        if verbose:
            print("Empty image")
    filedump(jout,jname)

def writefile(fname,content,comm_port=1,verbose=False):
    if comm_port:
        Pb.pwrite('{"cmd":"sd","arg":"wrf","param":{path:"'+fname+'","message":"'+str(content)+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        Pb.pread(verbose=verbose,expectjson=False) # NOTE: should implement write answer
    else:
        Pb.btpwrite('{"cmd":"sd","arg":"wrf","param":{path:"'+fname+'","message":"'+str(content)+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        Pb.btpread(verbose=verbose,expectjson=False) # NOTE: should implement write answer

def erasefile(fname,verbose=False,comm_port=1):
    if comm_port:
        Pb.pwrite('{"cmd":"sd","arg":"rmf","param":{path:"'+fname+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        Pb.pread(verbose=verbose)
    else:
        Pb.btpwrite('{"cmd":"sd","arg":"rmf","param":{path:"'+fname+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        Pb.btpread(verbose=verbose)


def erasedir(fname,verbose=False,comm_port=1):
    if comm_port:
        Pb.pwrite('{"cmd":"sd","arg":"rmdir","param":{path:"'+fname+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        Pb.pread(verbose=verbose,expectjson=False) # NOTE: should implement erasedir answer
    else:
        Pb.btpwrite('{"cmd":"sd","arg":"rmdir","param":{path:"'+fname+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        Pb.btpread(verbose=verbose,expectjson=False) # NOTE: should implement erasedir answer


def getjsonfile(fname,erase=True,verbose=False,basename=None,comm_port=1):
    if comm_port:
        Pb.pwrite('{"cmd":"sd","arg":"rdf","param":{path:"'+fname+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        out=Pb.pread(verbose=verbose)
    else:
        Pb.btpwrite('{"cmd":"sd","arg":"rdf","param":{path:"'+fname+'",debug:1,"comm_port":"'+str(comm_port)+'"}}',verbose=verbose)
        out=Pb.btpread(verbose=verbose)

    jout=Pb.parsejson(out)
    if jout:
        if "measurement" in jout:
            imagesave(jout,basename=basename)
            if verbose:
                jout["measurement"]["image"]="..."
                print(jout)
        else:
            outname=fname
            if fname[0]=='/':
                outname=fname[1:]
            filedump(jout,outname)
        if erase:
            erasefile(fname,verbose=verbose,comm_port=comm_port)
    else:
        if verbose:
            print(f'File {fname} not found')

def ls(fdir="/",canbeNone=False,verbose=False, comm_port=1):
    if comm_port:
        Pb.pwrite('{"cmd":"sd","arg":"fmd","param":{path:"'+fdir+'",debug:1,"comm_port":'+str(comm_port)+'}}',verbose=verbose)
        out=Pb.pread(verbose=verbose)
    else:
        Pb.btpwrite('{"cmd":"sd","arg":"fmd","param":{path:"'+fdir+'",debug:1,"comm_port":'+str(comm_port)+'}}',verbose=verbose)
        out=Pb.btpread(verbose=verbose)
    
    jout=Pb.parsejson(out)

    if jout is not None:
        return jout
    else:
        if canbeNone:
            return None
        else:
            return ls(fdir,canbeNone,verbose,comm_port)

def settime(epoch=None,verbose=False, comm_port=1):
    if not epoch:
        epoch=int(time.time())
    if comm_port:
        Pb.pwrite('{"cmd":"sys","arg":"sst","param":{"epoch_time":"'+str(epoch)+'","comm_port":'+str(comm_port)+'}}',verbose=verbose)
        out=Pb.pread(verbose=verbose,expectjson=False) # NOTE: should implement sst answer
    else:
        Pb.btpwrite('{"cmd":"sys","arg":"sst","param":{"epoch_time":"'+str(epoch)+'","comm_port":'+str(comm_port)+'}}',verbose=verbose)
        out=Pb.btpread(verbose=verbose) 
    return out

def camconfig(conf={},verbose=False,save=None, comm_port=1):
    defaultconf={
    #"framesize": "FRAMESIZE_VGA",
    #"pixformat": "PIXFORMAT_JPEG",
    #"quality": 10,
    "brightness": 0,
    "contrast": 0,
    "saturation": 0,
    "special_effect": 0,
    "whitebal": 0,
    "awb_gain": 0,
    "wb_mode": 0,
    "exposure_ctrl": 0,
    "aec2": 0,
    "ae_level": 0,
    "aec_value": 200,
    "gain_ctrl": 0,
    "agc_gain": 0,
    "gainceiling": 0,
    "bpc": 0,
    "wpc": 0,
    "raw_gma": 1,
    "lenc": 0,
    "hmirror": 0,
    "vflip": 0,
    "dcw": 1,
    "colorbar": 0
    }
    for i in defaultconf:
        if i not in conf:
            conf[i]=defaultconf[i]
    savepath="";
    if save:
        savepath=',"path":"/'+save+'"'
    cmd='{"cmd":"cam","arg":"cfgSet"'+savepath+',"param":'+json.dumps(conf)+',"debug":1,"comm_port":'+str(comm_port)+'}'
    if comm_port:
        Pb.pwrite(cmd,verbose=verbose)
        out=Pb.pread(verbose=verbose)
    else:
        Pb.btpwrite(cmd,verbose=verbose)
        out=Pb.btpread(verbose=verbose)

    return out

def takepict(param={},verbose=False, comm_port=1):
    defaultparam={
    "path": "/",
    "picN": 1,
    "picT": 2000,
    "nleds": 6,
    "brightness": 255,
    "r": 255,
    "g": 255,
    "b": 255,
    "comm_port": 0,
    "debug": 0
    }
    for i in defaultparam:
        if i not in param:
            param[i]=defaultparam[i]
    if comm_port:
        Pb.pwrite('{"cmd":"exp","arg":"picTimeLapse","param":'+json.dumps(param)+',"debug":1,"comm_port":'+str(comm_port)+'}',verbose=verbose)
        out=Pb.pread(verbose=verbose)
    else:
        Pb.btpwrite('{"cmd":"exp","arg":"picTimeLapse","param":'+json.dumps(param)+',"debug":1,"comm_port":'+str(comm_port)+'}',verbose=verbose)
        out=Pb.btpread(verbose=verbose)
    return out
