import plomboxtalk as Pb
import serial
import time

def init(device):
    try:
        Pb.ser=serial.Serial(device, 115200, timeout=1)
    except Exception as e:
        print("Error in serial connection: ",e)
        quit()

def pwrite(msg,verbose=False):
    if verbose:
        print(" >> Sending command: "+str(msg))
    #Pb.ser.flush()
    Pb.ser.write(str(msg).encode())

def pread(verbose=False,delay=1,expectjson=True):
    if not expectjson:
        time.sleep(delay)
    a = Pb.ser.readline().decode()
    out=a
    if not expectjson: # if I don't know how to stop, I read until it's empty
        while a != '':
            a = Pb.ser.readline().decode()
            out=out+a
    if verbose:
        for line in out.splitlines():
            print("  <<",line)
    if expectjson:
        end=0
        for line in out.splitlines():
            if line[0]=='{':
                end=1
        if not end:
            #time.sleep(0.01)
            out=out+pread(verbose,delay,expectjson)
    return out
