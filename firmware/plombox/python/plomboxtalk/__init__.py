from .command import imagesave,erasefile,getjsonfile,ls,settime,camconfig,takepict,erasedir,writefile
from .serial import pwrite,pread,init
from .parse import parsejson

import serial, sys
try:
    import bluetooth
    from .bt import btinit,scan,btpwrite,btpread
    btsoc=bluetooth.BluetoothSocket(bluetooth.RFCOMM)
except:
    # no BT yet :(
    btsoc=None
ser=serial.Serial(None, 115200, timeout=1)
