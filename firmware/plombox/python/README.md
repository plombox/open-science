# Python scripts

## plomboxtalk library

The plomboxtalk python library implements JSON comunication between the
PlomBOX and a computer to which the box is plugged by USB. It does not
use bluetooth for communication and the firmware has to be properly
compiled to use the serial link communication for it to work.

### function calls

All code needs to start by configuring the communication with the proper
serial port (something like /dev/ttyUSB0 on Linux, COM3 on Windows, and
dev/tty.usbserial-A50285BI on Mac):

    import plomboxtalk as Pb
    Pb.init("/dev/ttyUSB0")

Then it is a good practice to set the internal clock of the PlomBOX:

    Pb.settime(verbose=True)

Most function calls accept the verbose keyword. By default functions are
not verbose. Adding verbose will display what is sent to the box
(preceded by the **>>** mark) and what is received (preceded by **<<**).

The camera should be configured at start and whenever its configuration
should be changed. There are quite a few parameters that can be changed.
If no parameter is passed to the configuration function or if only some
of the parameters are passed, the others will be set to the default
value:

    camconf={
    "brightness": "0",
    "contrast": "0",
    "saturation": "0",
    "special_effect": "0",
    "whitebal": "0",
    "awb_gain": "0",
    "wb_mode": "0",
    "exposure_ctrl": "0",
    "aec2": "0",
    "ae_level": "0",
    "aec_value": "200",
    "gain_ctrl": "0",
    "agc_gain": "0",
    "gainceiling": "0",
    "bpc": "0",
    "wpc": "0",
    "raw_gma": "1",
    "lenc": "0",
    "hmirror": "0",
    "vflip": "0",
    "dcw": "1",
    "colorbar": "0"
    }

    Pb.camconfig(conf=camconf,verbose=True) # will load the camconf configuration into the PlomBOX. Note it has the default values
    Pb.camconfig(verbose=True) # will also load the default configuration

    camconf["aec_value"]="600"
    Pb.camconfig(conf=camconf,verbose=True) # will set the exposition time to 3 times its default value

    Pb.camconfig(conf={"aec_value":"600"},verbose=True) # does the same thing

Note that all parameters need to be passed as trings.  
Then, a picture or series of pictures can be taken, with some parameters
for the shot, mostly to control the LEDs:

    pictparam={
    "path": "/",
    "picN": "1",
    "picT": "2000",
    "nleds": "3",
    "brightness": "255",
    "r": "255",
    "g": "255",
    "b": "255",
    "send": "0",
    "comm_port": "0",
    "debug": "1"
    }

    Pb.takepict(param=pictparam,verbose=True) # takes a picture with default parameters

    pictparam["brightness"]="100"
    Pb.takepict(param=pictparam,verbose=True) # takes a picture with less LED brightness
    Pb.takepict(param={"brightness":"100"},verbose=True) # same here

    Pb.takepict(param={picN": "3", "picT": "10"}) # takes 3 pictures in a row

Again also all parameters are strings.  
Finally, the content of the device disk can be queried and files can be
downloaded:

    for file in Pb.ls():
        Pb.getjsonfile(file,verbose=True)

## examples

Some example codes are provided.

- PlomBOXusb.py is a somewhat complete DAQ code that can be tuned
- colors.py takes 6 pictures that should be (if a white paper is put in
  the plombox) red, green, blue, cyan, yellow and magenta by changing the
  3 LED values
- stripes.py takes pictures at short exposition time and low LED
  brightness to showcase the issue with LED PWM
- clean.py removes files and directories from PlomBOX
