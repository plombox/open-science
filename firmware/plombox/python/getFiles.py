#!/usr/bin/env python3

import sys
import plomboxtalk as Pb

comm_port=1
device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

if comm_port:
    # initialize serial device
    Pb.init(device)
else:
    Pb.btinit()

# set correct UTC time
Pb.settime(verbose=True)

# get the content of "/" after taking images
ls=Pb.ls()
# download all files
for i in ls:
    Pb.getjsonfile(i,verbose=True)

# clean disk:
for i in ls:
    Pb.erasefile(i)


