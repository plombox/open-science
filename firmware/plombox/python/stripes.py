#!/usr/bin/env python3

import time
import sys
import plomboxtalk as Pb

comm_port=1
device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

# initialize serial device
Pb.init(device)

for exp in range(20,50,10):
    Pb.camconfig(conf={"aec_value": str(exp)},verbose=True)
    Pb.takepict(param={"brightness":"200"},verbose=True)

# get the content of "/" after taking images
ls=Pb.ls()
# download all files
for i in ls:
    Pb.getjsonfile(i,verbose=True)
