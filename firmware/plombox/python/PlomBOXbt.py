#!/usr/bin/env python3

import time
import sys
import plomboxtalk as Pb

# initialize serial device
Pb.btinit()

# set correct UTC time
Pb.settime(verbose=True,comm_port=0)

# get the content of "/" before taking images so one can download new files
ls1=Pb.ls(verbose=True,comm_port=0)

# configure properly the camera, can pass a conf={} with specific configuration parameters, see plomboxtalk/command.py
camconfig={
    "brightness": "0",
    "contrast": "0",
    "saturation": "0",
    "special_effect": "0",
    "whitebal": "0",
    "awb_gain": "0",
    "wb_mode": "0",
    "exposure_ctrl": "0",
    "aec2": "0",
    "ae_level": "0",
    "aec_value": "200",
    "gain_ctrl": "0",
    "agc_gain": "0",
    "gainceiling": "0",
    "bpc": "0",
    "wpc": "0",
    "raw_gma": "1",
    "lenc": "0",
    "hmirror": "0",
    "vflip": "0",
    "dcw": "1",
    "colorbar": "0"
    }

# note, if you want to take images with different cam configs, this has to be called whenever you change the camconfig
Pb.camconfig(conf=camconfig,verbose=True,comm_port=0)#,save="cam.jso")

# configuration for image taking: 3 images per "takepict"
imgparam={
    "path": "/",
    "picN": "3",
    "picT": "10",
    "nleds": "6",
    "brightness": "255",
    "r": "255",
    "g": "255",
    "b": "255",
    "send": "0",
    "comm_port": "0",
    "debug": "1"
    }

for brightness in range(100,255,30):
    imgparam["brightness"]=str(brightness)
    Pb.takepict(param=imgparam,verbose=True,comm_port=0)

# get the content of "/" after taking images
ls2=Pb.ls(comm_port=0)
# download all new files
for i in ls2:
    if i not in ls1:
        Pb.getjsonfile(i,verbose=True,comm_port=0)

# clean disk:
for i in ls1:
    Pb.erasefile(i,comm_port=0)
