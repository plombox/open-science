#/***********************************************************************;
# * Project           : Plombox, WP2, ESP32-CAM sub-task
# *
# * Program name      : imgPlotPlombox.py
# *
# * Author            : @lharnaldi
# *
# * Date created      : 20200911
# *
# * Purpose           : Image plotting and analizer. It takes a JSON file and
# *                     parse it for data processing.
# *
# * Revision History  :
# *
# * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
# * 20200911    lharnaldi   1.0    Initial version
# *
# * Copyright/License information.
# ***********************************************************************/
import io
import numpy as np
from base64 import b64encode, b64decode
import matplotlib.pyplot as plt
from PIL import Image
from matplotlib.gridspec import GridSpec
import json

def plot_image_data(fname):
    #open file and save data to data variable
    with open(fname) as jf:
        jf.readline()
        jdata = json.load(jf)

    b64img = b64decode(jdata["measurement"]["image"])
    img = Image.open(io.BytesIO(b64img))
    width, height = img.size
    r, g, b = img.split()

    fig = plt.figure(constrained_layout=True,figsize=(10,10))
    gs = GridSpec(3, 2, figure=fig)
    # left column
    ax0=fig.add_subplot(gs[0,0])
    #ax1=fig.add_subplot(gs[1,0])
    ax2=fig.add_subplot(gs.new_subplotspec((1, 0), rowspan=2))
    # right column
    ax3=fig.add_subplot(gs[0,1])
    ax4=fig.add_subplot(gs[1,1],sharex=ax3)
    ax5=fig.add_subplot(gs[2,1],sharex=ax3)

    ax0.imshow(img)
    ax2.tick_params(labelbottom=False, labelleft=False)
    #Data file format is:
    #"format":0,"image_n":0,"detectorID":"042FA1962BC8","date":"21/05/01 22:19:36","user":{"name":"Bond","ID":7},"measurement":{"led":{"brightness":255,"R":255,"G":255,"B":255},"ID":0,"location":"here","C":23,"F":73.4,"exposure":300,"image":"
    ax2.text(0.10, 0.95, 'Data format         : {}'.format(jdata['format']))
    ax2.text(0.10, 0.90, 'Image Number   : {}'.format(jdata['image_n']))
    ax2.text(0.10, 0.85, 'Detector ID    : {}'.format(jdata['detectorID']))
    ax2.text(0.10, 0.80, 'Date               : {}'.format(jdata['date']))
    ax2.text(0.10, 0.75, 'User           : ')
    ax2.text(0.15, 0.70, 'Name           : {}'.format(jdata['user']['name']))
    ax2.text(0.15, 0.65, 'ID             : {}'.format(jdata['user']['ID']))
    ax2.text(0.10, 0.60, 'Measurement : ')
    ax2.text(0.15, 0.55, 'LED configuration : ')
    ax2.text(0.20, 0.50, 'Brightness     : {}'.format(jdata['measurement']['led']['brightness']))
    ax2.text(0.20, 0.45, 'Red       : {}'.format(jdata['measurement']['led']['R']))
    ax2.text(0.20, 0.40, 'Green       :   {}'.format(jdata['measurement']['led']['G']))
    ax2.text(0.20, 0.35, 'Blue       : {}'.format(jdata['measurement']['led']['B']))
    ax2.text(0.15, 0.30, 'ID      : {}'.format(jdata['measurement']['ID']))
    ax2.text(0.15, 0.25, 'Location: {}'.format(jdata['measurement']['location']))
    ax2.text(0.10, 0.20, 'Temperature : ')
    ax2.text(0.15, 0.15, 'Celsius : {}'.format(jdata['measurement']['C']))
    ax2.text(0.15, 0.10, 'Fahrenheit : {}'.format(jdata['measurement']['F']))
    ax2.text(0.10, 0.05, 'Exposure : {}'.format(jdata['measurement']['exposure']))#, va="center", ha="center")

    ax3.bar(np.arange(256),r.histogram(),color='r',label='R')
    ax3.set_xlim(0,256)
    ax3.set_ylabel('Pixel count')
    ax3.legend()
    
    ax4.bar(np.arange(256),g.histogram(),color='g',label='G')
    ax4.set_xlim(0,256)
    ax4.set_ylabel('Pixel count')
    ax4.legend()

    ax5.bar(np.arange(256),b.histogram(),color='b',label='B')
    ax5.set_xlim(0,256)
    ax5.set_ylabel('Pixel count')
    ax5.set_xlabel('Color value')
    ax5.legend()

    #plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    #fname=input('Give me the file name: ')
    fname="pb_data.json"
    plot_image_data(fname)

