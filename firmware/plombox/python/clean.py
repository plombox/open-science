#!/usr/bin/env python3

import sys
import plomboxtalk as Pb

comm_port=1
device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

# recursive reasing function
def erase(filelist,recdir=True):
    for i in filelist:
        if recdir and filelist[i]=="0": # is a directory
            print("!! Found directory "+i+", exploring...")
            if (Pb.ls(i,verbose=True)):
                erase(Pb.ls(i,verbose=True)) # erase directory content
                print("!! Removing directory "+i)
                Pb.erasedir(i,verbose=True)
        else:
            print("!! Found file "+i+", removing...")
            Pb.erasefile(i,verbose=True) # just erase file

Pb.init(device)
Pb.settime()
# Erase all files
erase(Pb.ls(verbose=True),recdir=False)
erase(Pb.ls(verbose=True))
