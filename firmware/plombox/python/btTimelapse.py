#!/usr/bin/env python3

import sys
import plomboxtalk as Pb
from PIL import Image
import numpy as np
import time

input("Ready for calibration. Please insert a white paper and press enter to continue")
# initialize serial device
Pb.btinit()

# set correct UTC time
Pb.settime(verbose=True,comm_port=0)

# set camera to default config with lens correction and exposure 100
Pb.camconfig(conf={"aec_value":"100"},verbose=True,comm_port=0)

col=["r","g","b"]
# take r,g,b colors
cal=[100,100,100]
jcal={}
for i in range(3):
    param={"r":"0","g":"0","b":"0"}
    param[col[i]]="255"
    Pb.takepict(param=param,verbose=True,comm_port=0)
    for f in Pb.ls(comm_port=0):
        Pb.getjsonfile(f,verbose=True,basename=col[i],comm_port=0)
    # now analyse the image obtained
    img=Image.open(col[i]+".jpg").convert('RGB')
    # calibration value aims at <img>=200, compared to aec_value=100
    cal[i]=int(100*200/np.mean(np.array(img)[:,:,i]))
    jcal[col[i]]=cal[i]
with open("calib.json","w") as file:
    file.write(str(jcal))

# take 3 RGB images and combine them for a flat field
def TakeRGBImage(cal):
    out=None
    for i in range(3):
        Pb.camconfig(conf={"aec_value":str(cal[i])},verbose=True,comm_port=0)
        param={"r":"0","g":"0","b":"0"}
        param[col[i]]="255"
        Pb.takepict(param=param,verbose=True,comm_port=0)
        for f in Pb.ls(comm_port=0):
            Pb.getjsonfile(f,verbose=True,basename=col[i],comm_port=0)
        img=Image.open(col[i]+".jpg").convert('RGB')
        if out is None:
            out=np.array(img)
        out[:,:,i]=np.array(img)[:,:,i]
    return out

flat=TakeRGBImage(cal)
img = Image.fromarray(flat, 'RGB')
img.save("flatfield.jpg")

input("Calibration done. Please insert object to image and press enter to continue.\nPictures will be taken for 10h, do not touch the window for 10h...")
# now that we have the calibrated values, take "2" images, one normal, one calibrated
for seq in range(20):
    Pb.camconfig(conf={"aec_value":str(int(np.mean(cal)))},verbose=True,comm_port=0)
    Pb.takepict(verbose=True,comm_port=0)
    for i in Pb.ls(comm_port=0):
        Pb.getjsonfile(i,verbose=True,basename=f'original_{seq:02d}',comm_port=0)

    # take the 3 RGB images and combine them
    final=TakeRGBImage(cal)
    img = Image.fromarray(final, 'RGB')
    img.save(f'final_{seq:02d}.jpg')

    # proper image creation with saturation cleaning
    img16bit=np.array(200*np.divide(final,flat)).astype(int)
    img16bit[img16bit>255]=255
    img = Image.fromarray(img16bit.astype('uint8'), 'RGB')
    img.save(f'final-flatfielded_{seq:02d}.jpg')
    print(f'Image {seq}/20 taken')
    for t in range(30):
        print(f'Waiting one minute {t}/30',end='\r')
        time.sleep(1)
