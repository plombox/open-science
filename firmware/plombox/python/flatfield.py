#!/usr/bin/env python3

import sys
import plomboxtalk as Pb
from PIL import Image
import numpy as np

comm_port=1
device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

input("Ready for calibration. Please insert a white paper and press enter to continue")
# initialize serial device
Pb.init(device)

# set camera to default config with lens correction and exposure 100
Pb.camconfig(conf={"aec_value":"100"},verbose=True)

col=["r","g","b"]
# take r,g,b colors
cal=[100,100,100]
jcal={}
for i in range(3):
    param={"r":0,"g":0,"b":0}
    param[col[i]]=255
    Pb.takepict(param=param,verbose=True)
    for f in Pb.ls():
        Pb.getjsonfile(f,verbose=True,basename=col[i])
    # now analyse the image obtained
    img=Image.open(col[i]+".jpg").convert('RGB')
    # calibration value aims at <img>=200, compared to aec_value=100
    cal[i]=int(100*200/np.mean(np.array(img)[:,:,i]))
    jcal[col[i]]=cal[i]
with open("calib.json","w") as file:
    file.write(str(jcal))

# take 3 RGB images and combine them for a flat field
def TakeRGBImage(cal):
    out=None
    for i in range(3):
        Pb.camconfig(conf={"aec_value":cal[i]},verbose=True)
        param={"r":0,"g":0,"b":0}
        param[col[i]]=255
        Pb.takepict(param=param,verbose=True)
        for f in Pb.ls():
            Pb.getjsonfile(f,verbose=True,basename=col[i])
        img=Image.open(col[i]+".jpg").convert('RGB')
        if out is None:
            out=np.array(img)
        out[:,:,i]=np.array(img)[:,:,i]
    return out

flat=TakeRGBImage(cal)
img = Image.fromarray(flat, 'RGB')
img.save("flatfield.jpg")

input("Calibration done. Please insert object to image and press enter to continue")
# now that we have the calibrated values, take "2" images, one normal, one calibrated
Pb.camconfig(conf={"aec_value":int(np.mean(cal))},verbose=True)
Pb.takepict(verbose=True)
for i in Pb.ls():
    Pb.getjsonfile(i,verbose=True,basename="original")

# take the 3 RGB images and combine them
final=TakeRGBImage(cal)
img = Image.fromarray(final, 'RGB')
img.save("final.jpg")

# proper image creation with saturation cleaning
img16bit=np.array(200*np.divide(final,flat)).astype(int)
img16bit[img16bit>255]=255
img = Image.fromarray(img16bit.astype('uint8'), 'RGB')
img.save("final-flatfielded.jpg")
