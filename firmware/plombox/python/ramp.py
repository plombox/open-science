#!/usr/bin/env python3

import time
import sys
import plomboxtalk as Pb

device="/dev/cu.usbserial-A10KGSP9"
#device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

# initialize serial device
Pb.init(device)

# set correct UTC time
Pb.settime(verbose=True)

# clean disk:
for i in Pb.ls(verbose=True):
    Pb.erasefile(i,verbose=True)

# configure properly the camera, can pass a conf={} with specific configuration parameters, see plomboxtalk/command.py
camconfig={
    "brightness": "0",
    "contrast": "0",
    "saturation": "0",
    "special_effect": "0",
    "whitebal": "0",
    "awb_gain": "0",
    "wb_mode": "0",
    "exposure_ctrl": "0",
    "aec2": "0",
    "ae_level": "0",
    "aec_value": "200",
    "gain_ctrl": "0",
    "agc_gain": "0",
    "gainceiling": "0",
    "bpc": "0",
    "wpc": "0",
    "raw_gma": "1",
    "lenc": "0",
    "hmirror": "0",
    "vflip": "0",
    "dcw": "1",
    "colorbar": "0"
    }

# note, if you want to take images with different cam configs, this has to be called whenever you change the camconfig
Pb.camconfig(conf=camconfig,verbose=True)#,save="cam.jso")

# configuration for image taking: 3 images per "takepict"
imgparam={
    "path": "/",
    "picN": "1",
    "picT": "10",
    "nleds": "3",
    "brightness": "255",
    "r": "255",
    "g": "255",
    "b": "255",
    "send": "0",
    "comm_port": "0",
    "debug": "1"
    }

for exposure in range(100,1201,50):
    camconfig["brightness"]=str(-2)
    camconfig["aec_value"]=str(exposure)
    Pb.camconfig(conf=camconfig,verbose=True)
    Pb.takepict(param=imgparam,verbose=True)
    for f in Pb.ls():
        Pb.getjsonfile(f,verbose=True,
        basename="capture" + "_exp{}".format(exposure))

# clean disk:
for i in Pb.ls(verbose=True):
    Pb.erasefile(i,verbose=True)