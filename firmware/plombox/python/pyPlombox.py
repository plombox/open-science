#!/usr/bin/env python

#/***********************************************************************;
# * Project           : Plombox, WP2, ESP32-CAM sub-task
# *
# * Program name      : pyPlombox.py
# *
# * Author            : @lharnaldi
# *
# * Date created      : 20200211
# *
# * Purpose           : Bluetooth socket client example in python
# *
# * Revision History  :
# *
# * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
# * 20200211    lharnaldi   1.0    Initial version
# *
# * Copyright/License information.
# ***********************************************************************/

import io
import sys	#for exit
import struct
import time
from datetime import datetime
import re
import numpy as np
from base64 import b64encode, b64decode
import matplotlib.pyplot as plt
#import matplotlib.image as image
from PIL import Image
from matplotlib.gridspec import GridSpec
import bluetooth
import json

# setup server
port = 1
target_addr = 'C8:2B:96:A1:2F:06' #My E32
#target_addr = 'C8:2B:96:A1:55:0A'
#target_addr = '7C:9E:BD:07:AA:AE' #Bs As
#target_addr = '7C:9E:BD:07:AA:AE'
#target_addr = 'AC:67:B2:2E:1A:8E'


def createBTSocket():
    try:
        s=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print( 'Failed to create socket')
        sys.exit()
    print( 'Socket Created')
    return s

def connectBTSocket(the_socket):
    #Connect to remote server
    the_socket.connect((target_addr,port))

#Send some data to remote server
def sendBTMsg(the_socket,message):
    try :
        #Set the whole string
        the_socket.sendall(message.encode())
    except socket.error:
        #Send failed
        print( 'Send failed')
        sys.exit()
    print( 'Message send successfully')

#def plot_image_data(imagefile,datafile):
#    #im = Image.open(imagefile)
#    #im.show()
#    im = image.imread(imagefile)
#    d=np.loadtxt(datafile)
#    print(d.shape)
#
#    fig,ax=plt.subplots(3,2,figsize=(10,10))
#    ax[0][0].imshow(im,label=imagefile)
#    ax[0][0].legend()
#    #ax[0][1].plot(d[0],'r',label='R')
#    ax[0][1].bar(d[0],'r',label='R')
#    ax[0][1].set_xlim(0,256)
#    ax[0][1].legend()
#    #ax[1][1].plot(d[1],'g',label='G')
#    ax[1][1].bar(d[1],'g',label='G')
#    ax[1][1].set_xlim(0,256)
#    ax[1][1].legend()
#    #ax[2][1].plot(d[2],'b',label='B')
#    ax[2][1].bar(d[2],'b',label='B')
#    ax[2][1].set_xlim(0,256)
#    ax[2][1].legend()
#    plt.legend()
#    plt.tight_layout()
#    plt.show()

def plot_image_data2(imagefile,datafile):
    im = image.imread(imagefile)
    d=np.loadtxt(datafile)
    print(d.shape)

    fig = plt.figure(constrained_layout=True,figsize=(10,10))
    gs = GridSpec(3, 2, figure=fig)
    # left column
    ax0=fig.add_subplot(gs[0:3,0])
    # right column
    ax1=fig.add_subplot(gs[0,1])
    ax2=fig.add_subplot(gs[1,1],sharex=ax1)
    ax3=fig.add_subplot(gs[2,1],sharex=ax1)

    ax0.imshow(im,label=imagefile)
    ax0.legend()

    ax1.bar(np.arange(len(d[0])),d[0],color='r',label='R')
    ax1.set_xlim(0,256)
    ax1.set_ylabel('Pixel count')
    ax1.legend()
    #ax[1][1].plot(d[1],'g',label='G')
    ax2.bar(np.arange(len(d[2])),d[1],color='g',label='G')
    ax2.set_xlim(0,256)
    ax2.set_ylabel('Pixel count')
    ax2.legend()
    #ax[2][1].plot(d[2],'b',label='B')
    ax3.bar(np.arange(len(d[2])),d[2],color='b',label='B')
    ax3.set_xlim(0,256)
    ax3.set_ylabel('Pixel count')
    ax3.set_xlabel('Color value')
    ax3.legend()
    #plt.tight_layout()
    plt.show()

def plot_datafile(datafile):
    d=np.loadtxt(datafile)
    print(d.shape)

    fig,ax=plt.subplots(3,1,sharex=True,figsize=(10,10))
    #ax[0].plot(d[0],'r',label='R')
    ax[0].bar(np.arange(len(d[0])),d[0],color='r',label='R')
    ax[0].set_xlim(0,256)
    ax[0].set_ylabel('Pixel count')
    ax[0].legend()
    #ax[1].plot(d[1],'g',label='G')
    ax[1].bar(np.arange(len(d[1])),d[1],color='g',label='G')
    ax[1].set_xlim(0,256)
    ax[1].set_ylabel('Pixel count')
    ax[1].legend()
    #ax[2].plot(d[2],'b',label='B')
    ax[2].bar(np.arange(len(d[2])),d[2],color='b',label='B')
    ax[2].set_xlim(0,256)
    ax[2].set_xlabel('Color value')
    ax[2].set_ylabel('Pixel count')
    ax[2].legend()
    plt.tight_layout()
    plt.show()

#def plot_datafile(datafile):
#    d=np.loadtxt(datafile)
#    print(d.shape)
#
#    fig,ax=plt.subplots(3,1,figsize=(10,10))
#    ax[0].plot(d[0],'r',label='R')
#    ax[0].set_xlim(0,256)
#    ax[1].plot(d[1],'g',label='G')
#    ax[1].set_xlim(0,256)
#    ax[2].plot(d[2],'b',label='B')
#    ax[2].set_xlim(0,256)
#    plt.legend()
#    plt.tight_layout()
#    plt.show()

def plot_data(data):
    #extract the numbers from the string
    l=[int(number) for number in data.split() if number.isdigit()]
    d=np.reshape(l,(3,256))

    fig,ax=plt.subplots(3,1,figsize=(10,10))
    ax[0].plot(np.arange(256),d[0],'r',label='R')
    ax[0].set_xlim(0,256)
    ax[1].plot(d[1],'g',label='G')
    ax[1].set_xlim(0,256)
    ax[2].plot(d[2],'b',label='B')
    ax[2].set_xlim(0,256)
    plt.legend()
    plt.tight_layout()
    plt.show()

def plot_image_data(fname):
    #open file and save data to data variable
    with open(fname) as jf:
        jdata = json.load(jf)

    b64img = b64decode(jdata["measurement"]["image"])
    img = Image.open(io.BytesIO(b64img))
    width, height = img.size
    r, g, b = img.split()

    fig = plt.figure(constrained_layout=True,figsize=(10,10))
    gs = GridSpec(3, 2, figure=fig)
    # left column
    ax0=fig.add_subplot(gs[0,0])
    #ax1=fig.add_subplot(gs[1,0])
    ax2=fig.add_subplot(gs.new_subplotspec((1, 0), rowspan=2))
    # right column
    ax3=fig.add_subplot(gs[0,1])
    ax4=fig.add_subplot(gs[1,1],sharex=ax3)
    ax5=fig.add_subplot(gs[2,1],sharex=ax3)

    ax0.imshow(img)
    ax2.tick_params(labelbottom=False, labelleft=False)
    #Data file format is:
    #{"format":0,"detectorID":42,"user":{"name":"Bond","ID":007},"measurement":{"ID":666,"location":"here","image"
    ax2.text(0.2, 0.9, 'Format     : {}'.format(jdata['format']))
    ax2.text(0.2, 0.8, 'Detector ID: {}'.format(jdata['detectorID']))
    ax2.text(0.2, 0.7, 'User       : ')
    ax2.text(0.3, 0.6, 'Name : {}'.format(jdata['user']['name']))
    ax2.text(0.3, 0.5, 'ID   : {}'.format(jdata['user']['ID']))
    ax2.text(0.2, 0.4, 'Measurement: ')
    ax2.text(0.3, 0.3, 'ID      : {}'.format(jdata['measurement']['ID']))
    ax2.text(0.3, 0.2, 'Location: {}'.format(jdata['measurement']['location']))#, va="center", ha="center")

    ax3.bar(np.arange(256),r.histogram(),color='r',label='R')
    ax3.set_xlim(0,256)
    ax3.set_ylabel('Pixel count')
    ax3.legend()
    
    ax4.bar(np.arange(256),g.histogram(),color='g',label='G')
    ax4.set_xlim(0,256)
    ax4.set_ylabel('Pixel count')
    ax4.legend()

    ax5.bar(np.arange(256),b.histogram(),color='b',label='B')
    ax5.set_xlim(0,256)
    ax5.set_ylabel('Pixel count')
    ax5.set_xlabel('Color value')
    ax5.legend()

    #plt.tight_layout()
    plt.show()

def recv_timeout(the_socket,pict=0,timeout=2):
    #make socket non blocking
    the_socket.setblocking(0)

    #total data partwise in an array
    total_data=[];
    data='';

    #beginning time
    begin=time.time()
    while 1:
        if total_data and time.time()-begin > timeout:
            #if you got some data, then break after timeout
            break

        elif time.time()-begin > timeout*2:
            #if you got no data at all, wait a little longer, twice the timeout
            break

    #recv something
        try:
            data = the_socket.recv(8192)
            if data:
                if pict:
                    total_data.append(data)
                else:
                    total_data.append(data.decode())
                    #change the beginning time for measurement
                begin=time.time()
            else:
                #sleep for sometime to indicate a gap
                time.sleep(0.1)
        except:
            pass

    #join all parts to make final string
    if pict:
        return b''.join(total_data)
    else:
        return ''.join(total_data)

#Tests start here
s=createBTSocket()
connectBTSocket(s)


cmd=input('Enter the command: ')
#sendBTMsg(cmd)
print('Command sent: {}'.format(cmd))

if cmd == '0':
    #Testing: plot datafile
    sendBTMsg(s,'0')
    print(recv_timeout(s))

elif cmd == '1':
    #Testing: plot data directly
    #sendBTMsg(s,'H')
    #get reply and print
    #print(recv_timeout(s))
    sendBTMsg(s,'1')
    #plot_data(recv_timeout(s))
    print(recv_timeout(s))

elif cmd == '2':
    #Testing: plot data directly
    #sendBTMsg(s,'H')
    #get reply and print
    #print(recv_timeout(s))
    #sendBTMsg(s,'2')
    #plot_data(recv_timeout(s))
    #print(recv_timeout(s))

    #Testing: plot datafile
    sendBTMsg(s,'2')
    with open('histoRGB.txt','w') as f:
        f.write(recv_timeout(s))
    plot_datafile('histoRGB.txt')
    #print(recv_timeout(s))
    #plot_image_data2('picture40.jpg','histoRGB.txt')

elif cmd == '3':
    #Testing: get JSON data
    sendBTMsg(s,'3')
    #print(recv_timeout(s))
    with open('pb_data.json','w') as f:
        f.write(recv_timeout(s))

elif cmd == '4':
    #Testing: get mean from photo 
    sendBTMsg(s,'4')
    print(recv_timeout(s))

elif cmd == '5':
    #Testing: info from photo
    sendBTMsg(s,'5')
    print(recv_timeout(s))

elif cmd == '6':
    #Testing: get photo and open it
    #filename='picture40.jpg'
    filename=input('Ingrese el nombre de archivo: ')
    sendBTMsg(s,'6')
    sendBTMsg(s,filename)
    sendBTMsg(s,'\n')
    with open(filename,'wb') as f:
        f.write(recv_timeout(s,1))

    time.sleep(1)
    #im = Image.open(filename)
    #im.show()
    im = image.imread(filename)
    fig,ax=plt.subplots(figsize=(10,10))
    ax.imshow(im)
    plt.show()

elif cmd == '7':
    #Testing: get photo and open it
    filename=input('Ingrese el nombre de archivo: ')
    sendBTMsg(s,'7')
    #sendBTMsg(s,'test3.json')
    sendBTMsg(s,filename)
    sendBTMsg(s,'\n')
    time.sleep(0.8);
    sendBTMsg(s,'0')
    print(recv_timeout(s))

elif cmd == '8':
    #Testing: info from photo
    filename=input('Ingrese el nombre de archivo: ')
    sendBTMsg(s,'8')
    #sendBTMsg(s,'test3.json')
    sendBTMsg(s,filename)
    #sendBTMsg(s,'picture40.jpg')
    sendBTMsg(s,'\n')
    #print(recv_timeout(s))
    with open(filename,'w') as f:
        f.write(recv_timeout(s))
    plot_image_data(filename)

elif cmd == '9':
    #Testing: get JSON data
    sendBTMsg(s,'9')
    #print(recv_timeout(s))
    with open('pb_data.json','w') as f:
        f.write(recv_timeout(s))

elif cmd == 'a':
    #Testing: plot data directly
    #sendBTMsg(s,'H')
    #get reply and print
    #print(recv_timeout(s))
    #sendBTMsg(s,'2')
    #plot_data(recv_timeout(s))
    #print(recv_timeout(s))

    #Testing: plot datafile
    sendBTMsg(s,'a')
    with open('histoRGB.txt','w') as f:
        f.write(recv_timeout(s))
    plot_datafile('histoRGB.txt')
    #print(recv_timeout(s))

elif cmd == 'b':
    #Testing: set system time
    sendBTMsg(s,'b')
    now = datetime.now()
    epoch_time = int(time.time())
    print(time.time(), epoch_time)
    sendBTMsg(s,str(epoch_time))
    sendBTMsg(s,'\n')
    #print(recv_timeout(s))
    print(now)
    print(epoch_time)
    #print(recv_timeout(s))

elif cmd == 'c':
    #Testing: get JSON data
    sendBTMsg(s,'c')
    print(recv_timeout(s))

elif cmd == 'd':
    #Testing: neopixel LED strip
    sendBTMsg(s,'d')
    #print(recv_timeout(s))

elif cmd == 'e':
    #Testing: get voltage from ADC
    sendBTMsg(s,'e')
    print(recv_timeout(s))

else:
    print("No input command :( ")

#plot_image_data2('picture40.jpg','histoRGB.txt')
#Close the socket
s.close()
