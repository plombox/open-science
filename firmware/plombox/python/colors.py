#!/usr/bin/env python3

import sys
import plomboxtalk as Pb

comm_port=1
device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

if comm_port:
    # initialize serial device
    Pb.init(device)
else:
    Pb.btinit()

Pb.settime()

# set camera to default config (just in case)
Pb.camconfig(verbose=True,comm_port=comm_port)

# take r,g,b colors
for col in ["r","g","b"]:
    param={"r":"0","g":"0","b":"0"}
    param[col]="255"
    Pb.takepict(param=param,verbose=True,comm_port=comm_port)
    param={"r":"255","g":"255","b":"255"}
    param[col]="0"
    Pb.takepict(param=param,verbose=True,comm_port=comm_port)

# download (and erase) all files
for i in Pb.ls(comm_port=comm_port):
    Pb.getjsonfile(i,verbose=True,comm_port=comm_port)
