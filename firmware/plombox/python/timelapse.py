#!/usr/bin/env python3

import sys
import plomboxtalk as Pb
from PIL import Image
import numpy as np
import time

device="/dev/ttyUSB0"
if len(sys.argv)>1:
    device=sys.argv[1]

input("Ready for calibration. Please insert a white paper and press enter to continue")
# initialize serial device
Pb.init(device)

# set correct UTC time
Pb.settime(verbose=True)

# get initial files
ls=Pb.ls(verbose=True)

# function to take RGB image
col=["r","g","b"]
def TakeRGBImage(cal=[100,100,100],basename=""):
    out=None
    for i in range(3):
        Pb.camconfig(conf={"aec_value":str(cal[i])},verbose=True)
        param={"r":"0","g":"0","b":"0","nleds":"6"}
        param[col[i]]="255"
        Pb.takepict(param=param,verbose=True)
        for f in Pb.ls():
            if f not in ls:
                Pb.getjsonfile(f,verbose=True,basename=basename+col[i])
        img=Image.open(basename+col[i]+".jpg").convert('RGB')
        if out is None:
            out=np.array(img)
        out[:,:,i]=np.array(img)[:,:,i]
    return out

# calibrate r,g,b LED brightness
default=100
cal=[default,default,default]
jcal={}
img=TakeRGBImage(cal)

def centerColor(image):
    w,h = image.shape[:2]
    return np.mean(np.mean(image[w*4//9:w*5//9,h*4//9:h*5//9,...], axis=0), axis=0)

for i in range(3):
    # calibration value aims at <img>=200, compared to aec_value=100
    cal[i]=int(100*200/centerColor(np.array(img))[i])
    jcal[col[i]]=cal[i]
with open("calib.json","w") as file:
    file.write(str(jcal))

# take 3 RGB images and combine them for a flat field
flat=TakeRGBImage(cal)
img = Image.fromarray(flat, 'RGB')
img.save("flatfield.jpg")

input("Calibration done. Please insert object to image and press enter to continue.\nPictures will be taken for hours, do not touch the window during that time...")
# now that we have the calibrated values, take "2" images, one normal, one calibrated
nimg=30
delay=10 # minutes
for seq in range(nimg):
    Pb.camconfig(conf={"aec_value":str(int(np.mean(cal)))},verbose=True)
    Pb.takepict(verbose=True)
    for f in Pb.ls():
        if f not in ls:
            Pb.getjsonfile(f,verbose=True,basename=f'original_{seq:02d}')

    # take the 3 RGB images and combine them
    final=TakeRGBImage(cal,basename=f'rgb_{seq:02d}_')
    img = Image.fromarray(final, 'RGB')
    img.save(f'final_{seq:02d}.jpg')

    # proper image creation with saturation cleaning
    img16bit=np.array(200*np.divide(final,flat)).astype(int)
    img16bit[img16bit>255]=255
    img = Image.fromarray(img16bit.astype('uint8'), 'RGB')
    img.save(f'final-flatfielded_{seq:02d}.jpg')
    print(f'Image {seq+1}/{nimg} taken')
    for t in range(delay):
        print(f'Waiting one minute {t}/{delay}',end='\r')
        time.sleep(60)
