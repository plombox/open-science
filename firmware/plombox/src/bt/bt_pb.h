/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : bt_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : Header file for the bluetooth interface.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _BT_PB_H
#define _BT_PB_H_

#include "Arduino.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "BluetoothSerial.h"

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

extern String bt_inputString;     // a String to hold incoming data
extern bool bt_stringComplete;    // whether the string is complete
extern BluetoothSerial SerialBT;
extern char * misc_getChipID();
extern char *cid;

int bt_init(uint8_t debug);
String bt_getDeviceAddress();
int bt_readCommand();
int bt_writeMsg(char* bt_msg);

#endif
