/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : bt_pb.cpp
 * *
 * Author            : @lharnaldi
 *
 * Date created      : 20202110
 *
 * Purpose           : Functions and definitions related to the bluetooth
 *                     interface. 
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20201021    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#include "bt_pb.h"

/*
 * Initialize bluetooth interface
 * It get the name from the unique device ID
 * */
int bt_init(uint8_t debug) {
  //cid = misc_getChipID();
  String bt_mac = "";
  char bt_name[32];

  if (debug)
    Serial.println(F("Initializing bluetooth...      OK"));

  //String bt_name = "pb-"+ String(cid);
  strcpy(bt_name,"pb-");
  strcat(bt_name,cid);

  delay(100);
  if(!SerialBT.begin(bt_name)){
    Serial.println(F("An error occurred initializing Bluetooth"));
    return 1;
  }

  bt_mac = bt_getDeviceAddress();
  delay(100);
  if (debug)
  {
    Serial.println();
    Serial.println();
    Serial.print(F("The device started with name: "));
    Serial.println(bt_name);
    Serial.print(F("The bluetooth MAC address is: "));
    Serial.println(bt_mac);
    Serial.println(F("Now you can pair to the PlomApp!"));
    Serial.println();
    Serial.println();
  }
  delay(100);

  //free(cid);
  return 0;
}

/*
 * Get the bluetooth MAC address
 * */
String bt_getDeviceAddress() {
  String finalstr;
  const uint8_t* point = esp_bt_dev_get_address();

  for (int i = 0; i < 6; i++) {

    char str[3];

    // append ch to str
    sprintf(str, "%02X", (int)point[i]);
    finalstr += str;
    //Serial.print(str);

    //if (i < 5){
    //  Serial.print(":");
    //}

  }
  //Serial.print("MAC address: ");
  //Serial.println(finalstr);
  return finalstr;
}

/*
   bt_readCommand occurs whenever a new data comes in the Bluetooth serial 
   interface. This routine is run between each time loop() runs, so using 
   delay inside loop can delay response. Multiple bytes of data may be 
   available.
   */
int bt_readCommand() {
  while(SerialBT.available()) {
    // get the new byte:
    char inChar = SerialBT.read();
    if (inChar != '\n'){
      // add it to the bt_inputString:
      bt_inputString += String(inChar);
    }
    // if the incoming character is a newline, 
    // set a flag so the main loop can do 
    // something about it:
    if (inChar == '\n') {
      bt_stringComplete = true;
    }
    //Serial.write(inChar);
  }
  return 0;
}

int bt_writeMsg(char* bt_msg) {
  SerialBT.print(bt_msg);
  return 0;
}

