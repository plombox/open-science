/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : sd_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : Header file for the SD (Secure Digital) related
*                     functions.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format)
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _SD_PB_H_
#define _SD_PB_H_

#include "Arduino.h"
#include <base64.h>
#include <ArduinoJson.h>
#include "BluetoothSerial.h"
#include "FS.h"
#include "SD_MMC.h"
#include "math.h"
#include "esp_camera.h"
#include "fd_forward.h"

//To use external PSRAM with ArduinoJson
struct SpiRamAllocator {
	void* allocate(size_t size) {
		return heap_caps_malloc(size, MALLOC_CAP_SPIRAM);
	}
	void deallocate(void* pointer) {
		heap_caps_free(pointer);
	}
};

using SpiRamJsonDocument = BasicJsonDocument<SpiRamAllocator>;

extern BluetoothSerial SerialBT;
extern camera_fb_t * cam_capture();

int sd_init(uint8_t debug);
int sd_listDir(fs::FS &fs, const JsonDocument& _docCmd);
int sd_createDir(fs::FS &fs, const JsonDocument& _docCmd);
int sd_removeDir(fs::FS &fs, const JsonDocument& _docCmd);
int sd_readFile(fs::FS &fs, const JsonDocument& _docCmd);
int sd_writeFile(fs::FS &fs, const JsonDocument& _docCmd);
int sd_appendFile(fs::FS &fs, const JsonDocument& _docCmd);
int sd_renameFile(fs::FS &fs, const JsonDocument& _docCmd);
int sd_removeFile(fs::FS &fs, const JsonDocument& _docCmd);
int sd_deleteFile(fs::FS &fs, const JsonDocument& _docCmd);
int sd_dirNumOfFiles(fs::FS &fs, const JsonDocument& _docCmd);
int sd_fetchDMetaSerial(fs::FS &fs, const JsonDocument& _docCmd);
DynamicJsonDocument* sd_readJsonFile(fs::FS &fs, const char* filename, uint8_t debug);
int sd_chgCamCfgFromFile(fs::FS &fs, const char* filename, const JsonDocument& doc);

#endif
