/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : sd_pb.cpp
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20202110
 *
 * Purpose           : SD (Secure Digital) related functions.
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format)
 * 20201021    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#include "sd_pb.h"

/*
 * Initialize SD card
 * */
int sd_init(uint8_t debug){

  //while(!SD_MMC.begin("/sdcard",true)){
  //  Serial.println(F("Card Mount Failed"));
  //  delay(200);
  //  return 1;
  //}
  if (!SD_MMC.begin("/sdcard",true)) {
    Serial.println(F("An Error has occurred while mounting SD card. Restarting..."));
    ESP.restart();
  }
  else {
    delay(500);
    if (debug)
      Serial.println(F("Initializing SD card...        OK"));
  }

  return 0;
}

/*
 * List directories in SD card
 * debug flag is for printing the content into the Serial (debug=1) or SerialBT
 * (debug=0) port
 * Example: {cmd:"sd",arg:"ls",param:{path:"/",debug:"1"}}
 * */
int sd_listDir(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  uint8_t debug;
  uint8_t comm_port;
  //uint8_t levels;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  //if (_docCmd["param"]["levels"] == nullptr) levels = 0;
  //else levels = _docCmd["param"]["levels"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }
  if (debug) {
    if(comm_port){
      Serial.print(F("Listing directory: "));
      Serial.println(path);
    } else {
      SerialBT.print(F("Listing directory: "));
      SerialBT.println(path);
    }
  }

  File root = fs.open(path.c_str());
  if(!root) {
    if (debug) {
      if(comm_port){
        Serial.println(F("Failed to open directory"));
      } else {
        SerialBT.println(F("Failed to open directory"));
      }
    }
    return 1;
  }
  if(!root.isDirectory()) {
    if (debug) {
      if(comm_port){
        Serial.println(F("Not a directory"));
      } else {
        SerialBT.println(F("Not a directory"));
      }
    }
    return 1;
  }

  File file = root.openNextFile();
  while(file) {
    if(file.isDirectory()) {
      if (debug) {
        if (comm_port){
          Serial.print(F("  DIR : "));
          Serial.println(file.name());
        } else {
          SerialBT.print(F("  DIR : "));
          SerialBT.println(file.name());
        }
      }
      //if(levels)
      //{
      //_docCmd["param"]["path"] = (String)file.name();
      //_docCmd["param"]["levels"] = (const uint8_t)levels-1;
      //  sd_listDir(fs, _docCmd);
      //  //sd_listDir(fs, file.name(), levels -1, debug);
      //}
    } else {
      if (debug) {
        if (comm_port){
          Serial.print(F("  FILE: "));
          Serial.print(file.name());
          Serial.print(F("  SIZE: "));
          Serial.println(file.size());
        } else {
          SerialBT.print(F("  FILE: "));
          SerialBT.print(file.name());
          SerialBT.print(F("  SIZE: "));
          SerialBT.println(file.size());
        }
      }
    }
    file = root.openNextFile();
  }
  return 0;
}

/*
 * Create a dir in SD card
 * */
int sd_createDir(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  if(debug){
    if (comm_port){
      Serial.printf("Creating Dir: %s\n", path.c_str());
    }else{
      SerialBT.printf("Creating Dir: %s\n", path.c_str());
    }
  }
  if(fs.mkdir(path.c_str())){
    if(debug){
      if (comm_port){
        Serial.println(F("Dir created"));
      }else{
        SerialBT.println(F("Dir created"));
      }
    }
    return 0;
  } else {
    if(debug){
      if (comm_port){
        Serial.println(F("mkdir failed"));
      }else{
        SerialBT.println(F("mkdir failed"));
      }
    }
    return 1;
  }
}

/*
 * delete a dir in SD card
 * */
int sd_removeDir(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  if(debug){
    if(comm_port){
      Serial.printf("Removing Dir: %s\n", path.c_str());
    }else{
      SerialBT.printf("Removing Dir: %s\n", path.c_str());
    }
  }
  if(fs.rmdir(path.c_str())){
    if(debug){
      if (comm_port){
        Serial.println(F("Dir removed"));
      } else {
        SerialBT.println(F("Dir removed"));
      }
    }
    return 0;
  }else{
    if(debug){
      if (comm_port){
        Serial.println(F("rmdir failed"));
      } else {
        SerialBT.println(F("rmdir failed"));
      }
    }
    return 1;
  }
}

/*
 * Read a file from SD card and print the content into 
 * Serial (debug=1) or SerialBT (debug=0) port.
 * Example: {cmd:"sd",arg:"rdf",param:{path:"/test2.json",debug:"1"}}
 * */
int sd_readFile(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  uint8_t debug;
  uint8_t comm_port;


  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  File file = fs.open(path.c_str());
  if(!file){
    if(debug){
      if(comm_port){
        Serial.println(F("Failed to open file for reading"));
      }else{
        SerialBT.println(F("Failed to open file for reading"));
      }
    }
    return 1;
  }

  String jsonString = "";
  //Serial.print(F("Read from file: "));
  while(file.available()){
    if (comm_port){
      Serial.write(file.read());
    }else{
      //jsonString += (char)
      SerialBT.print((char)file.read());
    }  
  }

  if(!comm_port){
    jsonString += "\n";
    jsonString += '#';
    SerialBT.println(jsonString);
    SerialBT.flush();
  }

  file.close();  
  return 0;
}

/*
 * Write a file in SD card
 * Example: {cmd:"sd",arg:"wrf",param:{path:"/test2.json",message:"Hola manola.",debug:"1"}}
 * */
int sd_writeFile(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  const char * message="";
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["message"] == nullptr) message = "";
  else message = _docCmd["param"]["message"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 0;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  if(debug){
    if (comm_port){
      Serial.printf("Writing file: %s\n", path.c_str());
    }else{
      SerialBT.printf("Writing file: %s\n", path.c_str());
    }
  }

  File file = fs.open(path, FILE_WRITE);
  if(!file){
    if(debug){
      if (comm_port){
        Serial.println("Failed to open file for writing");
      }else{
        SerialBT.println("Failed to open file for writing");
      }
    }
    return 1;
  }

  //fwrite(fb->buf, 1, fb->len, file);
  if(file.print(message)){
    if (debug){
      if (comm_port){
        Serial.println(F("File written"));
      }else{
        SerialBT.println(F("File written"));
      }
    }
    return 0;
  } else {
    if (debug){
      if (comm_port){
        Serial.println("Write failed");
      }else{
        SerialBT.println("Write failed");
      }
    }
    return 1;
  }

  file.close();
  return 0;
}

/*
 * Append to the end of file in SD card
 * */
int sd_appendFile(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  const char * message = "";
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["message"] == nullptr) message = "";
  else message = _docCmd["param"]["message"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 0;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  if (debug){
    if (comm_port){
      Serial.printf("Appending to file: %s\n", path.c_str());
    }else {
      SerialBT.printf("Appending to file: %s\n", path.c_str());
    }
  }

  File file = fs.open(path.c_str(), FILE_APPEND);
  if(!file){
    if (debug){
      if (comm_port){
        Serial.println(F("Failed to open file for appending"));
      }else {
        SerialBT.println(F("Failed to open file for appending"));
      }
    }
    return 1;
  }
  if(file.print(message)){
    if (debug){
      if (comm_port){
        Serial.println(F("Message appended"));
      }else{
        SerialBT.println(F("Message appended"));
      }
    }
    return 0;
  } else {
    if (debug){
      if (comm_port){
        Serial.println(F("Append failed"));
      }else{
        SerialBT.println(F("Append failed"));
      }
    }
    return 1;
  }
  file.close();
  return 0;
}

/*
 * Rename a file in SD card
 */
int sd_renameFile(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath1;
  const char * ppath2;
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path1"] == nullptr) ppath1 = "/";
  else ppath1 = _docCmd["param"]["path1"];
  if (_docCmd["param"]["path1"] == nullptr) ppath2 = "/";
  else ppath2 = _docCmd["param"]["path2"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 0;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path1 = ppath1;
  if (!path1.startsWith("/")){
    path1 ="/"+(String)ppath1;
  }
  String path2 = ppath2;
  if (!path2.startsWith("/")){
    path2 ="/"+(String)ppath2;
  }

  if (debug){
    if (comm_port){
      Serial.printf("Renaming file %s to %s\n", path1.c_str(), path2.c_str());
    }else {
      SerialBT.printf("Renaming file %s to %s\n", path1.c_str(), path2.c_str());
    }
  }

  if (fs.rename(path1.c_str(), path2.c_str())) {
    if (debug){
      if (comm_port){
        Serial.println(F("File renamed"));
      }else {
        SerialBT.println(F("File renamed"));
      }
    }
    return 0;
  } else {
    if (debug){
      if (comm_port){
        Serial.println(F("Rename failed"));
      }else {
        SerialBT.println(F("Rename failed"));
      }
    }
    return 1;
  }
}

/*
 * Delete a file in SD card
 * */
int sd_deleteFile(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  if(debug){
    if(comm_port){
      Serial.printf("Deleting file: %s\n", path.c_str());
    }else{
      SerialBT.printf("Deleting file: %s\n", path.c_str());
    }
  }
  if (comm_port) {
    if(fs.remove(path.c_str())){
      Serial.println(F("File deleted"));
      Serial.println(F("{'del':'1'}"));
      return 0;
    } else {
      Serial.println(F("Delete failed"));
      Serial.println(F("{'del':'0'}"));
      return 1;
    }
  } else {
    if(fs.remove(path.c_str())){
      SerialBT.println(F("File deleted"));
      SerialBT.println(F("{'del':'1'}"));
      return 0;
    } else {
      SerialBT.println(F("Delete failed"));
      SerialBT.println(F("{'del':'0'}"));
      return 1;
    }
  }
  return 0;
}

int sd_dirNumOfFiles(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  File root = fs.open(path.c_str());
  if(!root){
    Serial.println(F("Failed to open directory"));
    return 0;
  }
  if(!root.isDirectory()){
    Serial.println(F("Not a directory"));
    return 0;
  }

  File file = root.openNextFile();
  int k = 0;
  while(file){
    k += 1;
    file = root.openNextFile();
  }

  return k;
}

int sd_fetchDMetaSerial(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  uint8_t debug;
  uint8_t comm_port;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String path = ppath;
  if (!path.startsWith("/")){
    path ="/"+(String)ppath;
  }

  int k = sd_dirNumOfFiles(fs, _docCmd);

  File root = fs.open(path.c_str());
  if(!root){
    //Serial.println(F("Failed to open directory"));
    return 1;
  }
  if(!root.isDirectory()){
    //Serial.println(F("Not a directory"));
    return 1;
  }

  if (debug){
    if (comm_port){
      File file = root.openNextFile();
      Serial.print("{");
      while(file){
        // String temp = \"(String)file.name()+": "+(String)file.size();
        if ( k == 1){
          Serial.print("\""+(String)file.name()+"\""+":"+"\""+(String)file.size()+"\"");
          break;
        }
        Serial.print("\""+(String)file.name()+"\""+":"+"\""+(String)file.size()+"\",");
        k--;
        file = root.openNextFile();
      }
      Serial.println("}");
    } else {
      File file = root.openNextFile();
      SerialBT.print("{");
      while(file){
        // String temp = \"(String)file.name()+": "+(String)file.size();
        if ( k == 1){
          SerialBT.print("\""+(String)file.name()+"\""+":"+"\""+(String)file.size()+"\"");
          break;
        }
        SerialBT.print("\""+(String)file.name()+"\""+":"+"\""+(String)file.size()+"\",");
        k--;
        file = root.openNextFile();
      }
      SerialBT.println("}");
      SerialBT.println("#");
    }
  }
  return 0;
}

DynamicJsonDocument* sd_readJsonFile(fs::FS &fs, const char* filename, uint8_t debug) {
  File file = fs.open(filename, FILE_READ);
  if (!file) {
    Serial.println(F("Failed to open the file"));
  }
  size_t size = file.size();
  if (size > 1024) {
    Serial.println(F("File size is too large"));
  }
  std::unique_ptr<char[]> buf(new char[size]);
  file.readBytes(buf.get(), size);
  DynamicJsonDocument* jsonBuffer = new DynamicJsonDocument(1024);
  DeserializationError error = deserializeJson(*jsonBuffer, buf.get());
  if (error) {
    Serial.println("Failed to parse file");
  }
  if (debug){
    Serial.println();
    serializeJson(*jsonBuffer, Serial);
    Serial.println();
  }

  file.close();
  return jsonBuffer;
}

int sd_chgCamCfgFromFile(fs::FS &fs, const char* filename, const JsonDocument& doc){
  File file = fs.open(filename, FILE_READ);
  if (!file) {
    Serial.println(F("Failed to open the file"));
  }
  size_t size = file.size();
  if (size > 1024) {
    Serial.println(F("File size is too large"));
  }
  std::unique_ptr<char[]> buf(new char[size]);
  file.readBytes(buf.get(), size);
  DynamicJsonDocument* settings = new DynamicJsonDocument(1024);
  DeserializationError error = deserializeJson((*settings), buf.get());
  if (error) {
    Serial.println(F("Failed to parse file"));
  }
  Serial.println();
  serializeJson((*settings), Serial);
  Serial.println();
  file.close();

  // Copy values from the JsonDocument to the config settings
  //Set default value if not present
  if (doc["brightness"] == nullptr) (*settings)["brightness"] = "0";
  else (*settings)["brightness"] = doc["brightness"];
  if (doc["contrast"] == nullptr) (*settings)["contrast"] = "2";
  else (*settings)["contrast"] = doc["contrast"];
  if (doc["saturation"] == nullptr) (*settings)["saturation"] = "-2";
  else (*settings)["saturation"] = doc["saturation"];
  if (doc["special_effect"] == nullptr) (*settings)["special_effect"] = "0";
  else (*settings)["special_effect"] = doc["special_effect"];
  if (doc["whitebal"] == nullptr) (*settings)["whitebal"] = "0";
  else (*settings)["whitebal"] = doc["whitebal"];
  if (doc["awb_gain"] == nullptr) (*settings)["awb_gain"] = "0";
  else (*settings)["awb_gain"] = doc["awb_gain"];
  if (doc["wb_mode"] == nullptr) (*settings)["wb_mode"] = "0";
  else (*settings)["wb_mode"] = doc["wb_mode"];
  if (doc["exposure_ctrl"] == nullptr) (*settings)["exposure_ctrl"] = "1";
  else (*settings)["exposure_ctrl"] = doc["exposure_ctrl"];
  if (doc["aec2"] == nullptr) (*settings)["aec2"] = "0";
  else (*settings)["aec2"] = doc["aec2"];
  if (doc["ae_level"] == nullptr) (*settings)["ae_level"] = "0";
  else (*settings)["ae_level"] = doc["ae_level"];
  if (doc["aec_value"] == nullptr) (*settings)["aec_value"] = "400";
  else (*settings)["aec_value"] = doc["aec_value"];
  if (doc["gain_ctrl"] == nullptr) (*settings)["gain_ctrl"] = "1";
  else (*settings)["gain_ctrl"] = doc["gain_ctrl"];
  if (doc["agc_gain"] == nullptr) (*settings)["agc_gain"] = "0";
  else (*settings)["agc_gain"] = doc["agc_gain"];
  if (doc["gainceiling"] == nullptr) (*settings)["gainceiling"] = "0";
  else (*settings)["gainceiling"] = doc["gainceiling"];
  if (doc["bpc"] == nullptr) (*settings)["bpc"] = "0";
  else (*settings)["bpc"] = doc["bpc"];
  if (doc["wpc"] == nullptr) (*settings)["wpc"] = "0";
  else (*settings)["wpc"] = doc["wpc"];
  if (doc["raw_gma"] == nullptr) (*settings)["raw_gma"] = "1";
  else (*settings)["raw_gma"] = doc["raw_gma"];
  if (doc["lenc"] == nullptr) (*settings)["lenc"] = "0";
  else (*settings)["lenc"] = doc["lenc"];
  if (doc["hmirror"] == nullptr) (*settings)["hmirror"] = "0";
  else (*settings)["hmirror"] = doc["hmirror"];
  if (doc["vflip"] == nullptr) (*settings)["vflip"] = "0";
  else (*settings)["vflip"] = doc["vflip"];
  if (doc["dcw"] == nullptr) (*settings)["dcw"] = "1";
  else (*settings)["dcw"] = doc["dcw"];
  if (doc["colorbar"] == nullptr) (*settings)["colorbar"] = "0";
  else (*settings)["colorbar"] = doc["colorbar"];

  //Apply settings
  //sys_camConfig(*settings);

  File file2 = fs.open(filename, FILE_WRITE);
  if (!file2) {
    Serial.println(F("Failed to open the file in write mode"));
  }
  // serialize json to file
  if (serializeJson((*settings), file2) == 0) {
    Serial.println(F("Failed to write to file"));
  }

  // close the file
  file2.close();
  return 0;
}
