/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : cam_pb.cpp
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20202110
 *
 * Purpose           : Cam related functions. Hardware is OV2640
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20201021    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#include "cam_pb.h"

// XB64
typedef unsigned char BYTE;
char * xbase64_encode(BYTE const* buf, unsigned int bufLen);

static const std::string xbase64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";

char* xbase64_encode(BYTE const* buf, unsigned int bufLen) {
  char* ret=(char *)heap_caps_malloc(int(1.4*bufLen),MALLOC_CAP_SPIRAM);
  //ret.reserve(int(1.4*bufLen));
  int i = 0;
  int j = 0;
  int k = 0;
  BYTE char_array_3[3];
  BYTE char_array_4[4];

  while (bufLen--) {
    char_array_3[i++] = *(buf++);
    if (i == 3) {
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;

      for(i = 0; (i <4) ; i++)
        ret[k++] = xbase64_chars[char_array_4[i]];
      i = 0;
    }
  }

  if (i)
  {
    for(j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      ret[k++] = xbase64_chars[char_array_4[j]];

    while((i++ < 3))
      ret[k++] = '=';
  }
  ret[k++]='\0';

  return ret;
}

/*
 * Initialize the camera
 * It takes the default configuration
 * */
esp_err_t cam_init(uint8_t debug) {
  camera_config_t camera_pin_cfg;
  camera_pin_cfg.ledc_channel = LEDC_CHANNEL_0;
  camera_pin_cfg.ledc_timer = LEDC_TIMER_0;
  camera_pin_cfg.pin_d0 = Y2_GPIO_NUM;
  camera_pin_cfg.pin_d1 = Y3_GPIO_NUM;
  camera_pin_cfg.pin_d2 = Y4_GPIO_NUM;
  camera_pin_cfg.pin_d3 = Y5_GPIO_NUM;
  camera_pin_cfg.pin_d4 = Y6_GPIO_NUM;
  camera_pin_cfg.pin_d5 = Y7_GPIO_NUM;
  camera_pin_cfg.pin_d6 = Y8_GPIO_NUM;
  camera_pin_cfg.pin_d7 = Y9_GPIO_NUM;
  camera_pin_cfg.pin_xclk = XCLK_GPIO_NUM;
  camera_pin_cfg.pin_pclk = PCLK_GPIO_NUM;
  camera_pin_cfg.pin_vsync = VSYNC_GPIO_NUM;
  camera_pin_cfg.pin_href = HREF_GPIO_NUM;
  camera_pin_cfg.pin_sscb_sda = SIOD_GPIO_NUM;
  camera_pin_cfg.pin_sscb_scl = SIOC_GPIO_NUM;
  camera_pin_cfg.pin_pwdn = PWDN_GPIO_NUM;
  camera_pin_cfg.pin_reset = RESET_GPIO_NUM;
  camera_pin_cfg.xclk_freq_hz = 20000000;
  camera_pin_cfg.pixel_format = PIXFORMAT_JPEG;

  if (psramFound()) {
    camera_pin_cfg.frame_size = FRAMESIZE_SVGA; //FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA
    camera_pin_cfg.jpeg_quality = 12;
    camera_pin_cfg.fb_count = 1;
  } else {
    camera_pin_cfg.frame_size = FRAMESIZE_VGA;
    camera_pin_cfg.jpeg_quality = 12;
    camera_pin_cfg.fb_count = 1;
  }
  //camera_pin_cfg.fb_count = 1;
  //camera_pin_cfg.jpeg_quality = 10;
  //camera_pin_cfg.frame_size = FRAMESIZE_XGA;

  esp_err_t err = esp_camera_init(&camera_pin_cfg);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return ESP_FAIL;
  }

  //this is just an holder for the JSON (it is a null JSON)
  //It is for defaul camera registers configuration
  StaticJsonDocument<512> cfg_doc;
  cam_config(cfg_doc, camera_cfg_tmp, 1);

  if (debug){
    Serial.println(F("Initializing CAM module...     OK"));
  }

  return ESP_OK;
}

/*
 * Configure the camera parameters
 * */
esp_err_t cam_config(const JsonDocument& _docCmd, struct camConfig &actual_config, int init){

  sensor_t * s = esp_camera_sensor_get();

  if (_docCmd["param"]["framesize"] != nullptr){
    s->set_framesize(s, (framesize_t)_docCmd["param"]["framesize"]);
    actual_config.framesize  = (framesize_t)_docCmd["param"]["framesize"];
  }else{
    s->set_framesize(s, (framesize_t)camera_cfg_default.framesize);
    actual_config.framesize  = (framesize_t)camera_cfg_default.framesize;
  }
  if (_docCmd["param"]["pixformat"] != nullptr){
    s->set_pixformat(s, (pixformat_t)_docCmd["param"]["pixformat"]);
    actual_config.pixformat  = (pixformat_t)_docCmd["param"]["pixformat"];
  }else{
    s->set_pixformat(s, (pixformat_t)camera_cfg_default.pixformat);
    actual_config.pixformat  = (pixformat_t)camera_cfg_default.pixformat;
  }
  if (_docCmd["param"]["quality"] != nullptr){
    s->set_quality(s, _docCmd["param"]["quality"]);
    actual_config.quality    = _docCmd["param"]["quality"];
  }else{
    s->set_quality(s, camera_cfg_default.quality);
    actual_config.quality    = camera_cfg_default.quality;
  }
  if (_docCmd["param"]["brightness"] != nullptr){
    s->set_brightness(s, _docCmd["param"]["brightness"]);
    actual_config.brightness    = _docCmd["param"]["brightness"];
  }else{
    s->set_brightness(s, camera_cfg_default.brightness);
    actual_config.brightness    = camera_cfg_default.brightness;
  }
  if (_docCmd["param"]["contrast"] != nullptr) {
    s->set_contrast(s, _docCmd["param"]["contrast"]);
    actual_config.contrast = _docCmd["param"]["contrast"];
  }else{
    s->set_contrast(s, camera_cfg_default.contrast);
    actual_config.contrast = camera_cfg_default.contrast;
  }
  if (_docCmd["param"]["saturation"] != nullptr){
    s->set_saturation(s, _docCmd["param"]["saturation"]);
    actual_config.saturation = _docCmd["param"]["saturation"];
  }else{
    s->set_saturation(s, camera_cfg_default.saturation);
    actual_config.saturation = camera_cfg_default.saturation;
  }
  if (_docCmd["param"]["special_effect"] != nullptr){
    s->set_special_effect(s, _docCmd["param"]["special_effect"]);
    actual_config.special_effect= _docCmd["param"]["special_effect"];
  } else{
    s->set_special_effect(s, camera_cfg_default.special_effect);
    actual_config.special_effect = camera_cfg_default.special_effect;
  }
  if (_docCmd["param"]["whitebal"] != nullptr){
    s->set_whitebal(s, _docCmd["param"]["whitebal"]);
    actual_config.whitebal    = _docCmd["param"]["whitebal"];
  }else {
    s->set_whitebal(s, camera_cfg_default.whitebal);
    actual_config.whitebal = camera_cfg_default.whitebal;
  }
  if (_docCmd["param"]["awb_gain"] != nullptr){ 
    s->set_awb_gain(s, _docCmd["param"]["awb_gain"]);
    actual_config.awb_gain = _docCmd["param"]["awb_gain"];
  } else{ 
    s->set_awb_gain(s, camera_cfg_default.awb_gain);
    actual_config.awb_gain = camera_cfg_default.awb_gain;
  }
  if (_docCmd["param"]["wb_mode"] != nullptr) {
    s->set_wb_mode(s, _docCmd["param"]["wb_mode"]);
    actual_config.wb_mode = _docCmd["param"]["wb_mode"];
  } else {
    s->set_wb_mode(s, camera_cfg_default.wb_mode);
    actual_config.wb_mode = camera_cfg_default.wb_mode;
  }
  if (_docCmd["param"]["exposure_ctrl"] != nullptr){
    s->set_exposure_ctrl(s, _docCmd["param"]["exposure_ctrl"]);
    actual_config.exposure_ctrl = _docCmd["param"]["exposure_ctrl"];
  } else {
    s->set_exposure_ctrl(s, camera_cfg_default.exposure_ctrl);
    actual_config.exposure_ctrl = camera_cfg_default.exposure_ctrl;
  }
  if (_docCmd["param"]["aec2"] != nullptr) {
    s->set_aec2(s, _docCmd["param"]["aec2"]);
    actual_config.aec2 = _docCmd["param"]["aec2"];
  } else {
    s->set_aec2(s, camera_cfg_default.aec2);
    actual_config.aec2 = camera_cfg_default.aec2;
  }
  if (_docCmd["param"]["ae_level"] != nullptr) {
    s->set_ae_level(s, _docCmd["param"]["ae_level"]);
    actual_config.ae_level = _docCmd["param"]["ae_level"];
  }else {
    s->set_ae_level(s, camera_cfg_default.ae_level);
    actual_config.ae_level = camera_cfg_default.ae_level;
  }
  if (_docCmd["param"]["aec_value"] != nullptr) {
    s->set_aec_value(s, _docCmd["param"]["aec_value"]);
    actual_config.aec_value = _docCmd["param"]["aec_value"];
  } else {
    s->set_aec_value(s, camera_cfg_default.aec_value);
    actual_config.aec_value = camera_cfg_default.aec_value;
  }
  if (_docCmd["param"]["gain_ctrl"] != nullptr) {
    s->set_gain_ctrl(s, _docCmd["param"]["gain_ctrl"]);
    actual_config.gain_ctrl = _docCmd["param"]["gain_ctrl"];
  } else {
    s->set_gain_ctrl(s, camera_cfg_default.gain_ctrl);
    actual_config.gain_ctrl = camera_cfg_default.gain_ctrl;
  }
  if (_docCmd["param"]["agc_gain"] != nullptr) {
    s->set_agc_gain(s, _docCmd["param"]["agc_gain"]);
    actual_config.agc_gain = _docCmd["param"]["agc_gain"];
  } else {
    s->set_agc_gain(s, camera_cfg_default.agc_gain);
    actual_config.agc_gain = camera_cfg_default.agc_gain;
  }
  if (_docCmd["param"]["gainceiling"] != nullptr) {
    s->set_gainceiling(s, (gainceiling_t)_docCmd["param"]["gainceiling"]);
    actual_config.gainceiling = (gainceiling_t)_docCmd["param"]["gainceiling"];
  } else {
    s->set_gainceiling(s, (gainceiling_t)camera_cfg_default.gainceiling);
    actual_config.gainceiling = (gainceiling_t)camera_cfg_default.gainceiling;
  }
  if (_docCmd["param"]["bpc"] != nullptr) {
    s->set_bpc(s, _docCmd["param"]["bpc"]);
    actual_config.bpc = _docCmd["param"]["bpc"];
  } else {
    s->set_bpc(s, camera_cfg_default.bpc);
    actual_config.bpc = camera_cfg_default.bpc;
  }
  if (_docCmd["param"]["wpc"] != nullptr) {
    s->set_wpc(s, _docCmd["param"]["wpc"]);
    actual_config.wpc = _docCmd["param"]["wpc"];
  } else {
    s->set_wpc(s, camera_cfg_default.wpc);
    actual_config.wpc = camera_cfg_default.wpc;
  }
  if (_docCmd["param"]["raw_gma"] != nullptr) {
    s->set_raw_gma(s, _docCmd["param"]["raw_gma"]);
    actual_config.raw_gma = _docCmd["param"]["raw_gma"];
  } else {
    s->set_raw_gma(s, camera_cfg_default.raw_gma);
    actual_config.raw_gma = camera_cfg_default.raw_gma;
  }
  if (_docCmd["param"]["lenc"] != nullptr) {
    s->set_lenc(s, _docCmd["param"]["lenc"]);
    actual_config.lenc = _docCmd["param"]["lenc"];
  } else {
    s->set_lenc(s, camera_cfg_default.lenc);
    actual_config.lenc = camera_cfg_default.lenc;
  }
  if (_docCmd["param"]["hmirror"] != nullptr) {
    s->set_hmirror(s, _docCmd["param"]["hmirror"]);
    actual_config.hmirror = _docCmd["param"]["hmirror"];
  } else {
    s->set_hmirror(s, camera_cfg_default.hmirror);
    actual_config.hmirror = camera_cfg_default.hmirror;
  }
  if (_docCmd["param"]["vflip"] != nullptr) {
    s->set_vflip(s, _docCmd["param"]["vflip"]);
    actual_config.vflip = _docCmd["param"]["vflip"];
  } else {
    s->set_vflip(s, camera_cfg_default.vflip);
    actual_config.vflip = camera_cfg_default.vflip;
  }
  if (_docCmd["param"]["dcw"] != nullptr) {
    s->set_dcw(s, _docCmd["param"]["dcw"]);
    actual_config.dcw = _docCmd["param"]["dcw"];
  } else {
    s->set_dcw(s, camera_cfg_default.dcw);
    actual_config.dcw = camera_cfg_default.dcw;
  }
  if (_docCmd["param"]["colorbar"] != nullptr) {
    s->set_colorbar(s, _docCmd["param"]["colorbar"]);
    actual_config.colorbar = _docCmd["param"]["colorbar"];
  }  else {
    s->set_colorbar(s, camera_cfg_default.colorbar);
    actual_config.colorbar = camera_cfg_default.colorbar;
  }

  //check if we should save the new configuration to a file
  if (_docCmd["path"] != nullptr){
    fs::FS &fs = SD_MMC;
    const char * ppath = _docCmd["path"];
    String path = ppath;
    if (!path.startsWith("/")){
      path ="/"+(String)ppath;
    }

    File file = fs.open(path, FILE_WRITE);
    if (!file) {
      Serial.println(F("Failed to open the file in write mode"));
      return 1;
    }
    // serialize json to file
    if (serializeJson(_docCmd["param"], file) == 0) {
      Serial.println(F("Failed to write to file"));
      return 1;
    }

    file.close();
  }

  if(!init){
    Serial.println("{}");
  }
  return 0;
}

/* 
 * Check if photo capture was successful
 */
bool cam_checkPhoto( fs::FS &fs , const char* photoName) {
  File f_pic = fs.open( photoName );
  unsigned int pic_sz = f_pic.size();
  return ( pic_sz > 100 );
}

/*
 * Save a photo with date as part of the file name.
 * Data is saved in .json format. The file contains all the 
 * required Plombox fields.
 * */
esp_err_t cam_saveJsonPicDated(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  const char * ttype;
  uint8_t nleds, brightness, r, g, b, debug, comm_port, nloop=0;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["type"] == nullptr) ttype = "whitePaper";
  else ttype = _docCmd["param"]["type"];
  if (_docCmd["param"]["nleds"] == nullptr) nleds = 6;
  else nleds = _docCmd["param"]["nleds"];
  if (_docCmd["param"]["brightness"] == nullptr) brightness = 255;
  else brightness = _docCmd["param"]["brightness"];
  if (_docCmd["param"]["r"] == nullptr) r = 255;
  else r = _docCmd["param"]["r"];
  if (_docCmd["param"]["g"] == nullptr) g = 255;
  else g = _docCmd["param"]["g"];
  if (_docCmd["param"]["b"] == nullptr) b = 255;
  else b = _docCmd["param"]["b"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String firmware = "v"+(String)currentVersion+"r"+(String)currentMayorRevision+(String)currentMinorRevision;
  String filename;
  String path = ppath;
  if ((!path.startsWith("/")) | (path != "/")){
    path ="/"+(String)ppath+"/";
  }

  //Check if file exists
  do{
    time(&now);
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%y_%m_%d_%H_%M_%S", &timeinfo);

    filename = path+cid+"_"+String(strftime_buf)+".jso";

    if (fs.exists(filename)){
      Serial.print(filename);
      Serial.println(F(" ----> File EXISTS!!!"));
    }
    //delay(1000);
  }while(fs.exists(filename));

  strftime(strftime_buf, sizeof(strftime_buf), "%FT%TZ", gmtime(&now));

  //cam_SpiRamJsonDocument doc(131077); //128 kB
  //cam_SpiRamJsonDocument doc(262144); //256 kB
  cam_SpiRamJsonDocument doc(1048576); //1 MB

  doc["version"] = currentDataVersion;
  JsonObject esp32 = doc.createNestedObject("esp32");
  esp32["ID"] = cid;
  esp32["firmware"] = firmware;
  JsonObject telemetry = doc.createNestedObject("telemetry");
  telemetry["date"] = strftime_buf;
  telemetry["time"] = misc_getEpochTime(0);
  JsonObject temp = telemetry.createNestedObject("temp");
  temp["C"] = ds18b20_getTemperature(0); //0: Celsius, 1: Farhenheit
  temp["F"] = ds18b20_getTemperature(1); //0: Celsius, 1: Farhenheit
  JsonObject measurement = doc.createNestedObject("measurement");
  measurement["ID"]= (String)jsonDated_file_number;
  measurement["run"] = (String)jsonDated_file_number;
  measurement["fname"] = filename;

  File file = fs.open(filename, FILE_WRITE);
  if(!file){
    Serial.println(F("cam_picSaveJsonDated: Could not open file while creating the JSON file."));
    return 1;
  }  else  {
    jsonDated_file_number++;
    misc_neoInit(brightness);
    for (uint8_t i=0; i < nleds; i++)
      misc_neoSetColorRGB(i,r,g,b);

    delay(100);
    camera_fb_t *pic = esp_camera_fb_get();
    //camera_fb_t *pic = NULL;

    size_t pic_len;
    pic_len = pic->len;
    Serial.print(F("Pic len: "));
    Serial.println(pic_len);

    sensor_t * s = esp_camera_sensor_get();

    while (pic_len>59999 && nloop < 50) {
      Serial.println(F("Error: Camera image too large"));
      Serial.println(F("Trying again with lower quality image"));
      s->set_quality(s, camera_cfg_tmp.quality+nloop); // lower quality
      
      pic = esp_camera_fb_get();
      pic_len = pic->len;
      Serial.print(F("Pic len: "));
      Serial.println(pic_len);
      Serial.println(nloop);
      nloop++;
      if (nloop >= 49)
        return 666;
    }
    
    s->set_quality(s, camera_cfg_tmp.quality); // put back quality

    char* encoded = xbase64_encode((const uint8_t *)pic->buf, pic_len);

    measurement["image"]=encoded;
    measurement["nloop"]=nloop;

    esp_camera_fb_return(pic);
    misc_neoClear();

    JsonObject led = measurement.createNestedObject("led");
    led["brightness"] = brightness;
    led["R"] = r;
    led["G"] = g;
    led["B"] = b;
    JsonObject cam = measurement.createNestedObject("cam");
    cam["brightness"] = camera_cfg_tmp.brightness; 
    cam["contrast"] = camera_cfg_tmp.contrast; 
    cam["saturation"] = camera_cfg_tmp.saturation; 
    cam["special_effect"] = camera_cfg_tmp.special_effect; 
    cam["whitebal"] = camera_cfg_tmp.whitebal; 
    cam["awb_gain"] = camera_cfg_tmp.awb_gain; 
    cam["wb_mode"] = camera_cfg_tmp.wb_mode; 
    cam["exposure_ctrl"] = camera_cfg_tmp.exposure_ctrl; 
    cam["aec2"] = camera_cfg_tmp.aec2; 
    cam["ae_level"] = camera_cfg_tmp.ae_level; 
    cam["aec_value"] = camera_cfg_tmp.aec_value; 
    cam["gain_ctrl"] = camera_cfg_tmp.gain_ctrl; 
    cam["agc_gain"] = camera_cfg_tmp.agc_gain; 
    cam["gainceiling"] = camera_cfg_tmp.gainceiling; 
    cam["bpc"] = camera_cfg_tmp.bpc; 
    cam["wpc"] = camera_cfg_tmp.wpc; 
    cam["raw_gma"] = camera_cfg_tmp.raw_gma; 
    cam["lenc"] = camera_cfg_tmp.lenc; 
    cam["hmirror"] = camera_cfg_tmp.hmirror; 
    cam["vflip"] = camera_cfg_tmp.vflip; 
    cam["dcw"] = camera_cfg_tmp.dcw; 
    cam["colorbar"] = camera_cfg_tmp.colorbar; 
    measurement["type"]=ttype;
    JsonObject extra = doc.createNestedObject("extra");
    extra["cmd"] = _docCmd.as<String>();

    // Serialize JSON to file
    if (serializeJson(doc, file) == 0) {
      Serial.println(F("Failed to write to file"));
      return 1;
    }
    file.close();
    Serial.println(F("File saved."));

    if (debug){
      if (comm_port){
        //Serial.println(measureJson(doc));
        serializeJson(doc, Serial);
      }else{
        SerialBT.println(measureJson(doc));
        serializeJson(doc, SerialBT);
      }
    }
    heap_caps_free(encoded);
  }
  return 0;
}

/*
 * Save a photo with a number as part of the file name.
 * Data is saved in .json format. The file contains all the 
 * required Plombox fields.
 * */
esp_err_t cam_saveJsonPicNumbered(fs::FS &fs, const JsonDocument& _docCmd){
  const char * ppath;
  const char * ttype;
  uint8_t nleds, brightness, r, g, b, debug, comm_port, nloop=0;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) ppath = "/";
  else ppath = _docCmd["param"]["path"];
  if (_docCmd["param"]["type"] == nullptr) ttype = "whitePaper";
  else ttype = _docCmd["param"]["type"];
  if (_docCmd["param"]["nleds"] == nullptr) nleds = 6;
  else nleds = _docCmd["param"]["nleds"];
  if (_docCmd["param"]["brightness"] == nullptr) brightness = 255;
  else brightness = _docCmd["param"]["brightness"];
  if (_docCmd["param"]["r"] == nullptr) r = 255;
  else r = _docCmd["param"]["r"];
  if (_docCmd["param"]["g"] == nullptr) g = 255;
  else g = _docCmd["param"]["g"];
  if (_docCmd["param"]["b"] == nullptr) b = 255;
  else b = _docCmd["param"]["b"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  String firmware = "v"+(String)currentVersion+"r"+(String)currentMayorRevision+(String)currentMinorRevision;
  String filename;
  String path = ppath;
  if ((!path.startsWith("/")) | (path != "/")){
    path ="/"+(String)ppath+"/";
  }

  //Check if file exists
  do{
    json_file_number++;
    filename = path+cid+"_"+String(json_file_number)+".jso";

    if (fs.exists(filename)){
      Serial.print(filename);
      Serial.println(F(" ----> File EXISTS!!!"));
    }
    //delay(100);
  }while(fs.exists(filename));

  time(&now);
  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%FT%TZ", gmtime(&now));

  //cam_SpiRamJsonDocument doc(262144); //256 kB
  cam_SpiRamJsonDocument doc(1048576); //1 MB

  doc["version"] = currentDataVersion;
  JsonObject esp32 = doc.createNestedObject("esp32");
  esp32["ID"] = cid;
  esp32["firmware"] = firmware;
  JsonObject telemetry = doc.createNestedObject("telemetry");
  telemetry["date"] = strftime_buf;
  telemetry["time"] = misc_getEpochTime(0);
  JsonObject temp = telemetry.createNestedObject("temp");
  temp["C"] = ds18b20_getTemperature(0); //0: Celsius, 1: Farhenheit
  temp["F"] = ds18b20_getTemperature(1); //0: Celsius, 1: Farhenheit
  JsonObject measurement = doc.createNestedObject("measurement");
  measurement["ID"]= (String)jsonDated_file_number;
  measurement["run"] = (String)jsonDated_file_number;
  measurement["fname"] = filename;

  File file = fs.open(filename, FILE_WRITE);
  if(!file){
    Serial.println(F("Could not open file"));
    return 1;
  }  else  {
    Serial.print(F("Taking JSON picture "));
    Serial.print(json_file_number);
    Serial.print(F(": "));
    Serial.println(filename);

    misc_neoInit(brightness);
    for (uint8_t i=0; i < nleds; i++)
      misc_neoSetColorRGB(i,r,g,b);

    camera_fb_t *pic = esp_camera_fb_get();
    //camera_fb_t *pic = NULL;

    size_t pic_len;
    pic_len = pic->len;
    Serial.print(F("Pic len: "));
    Serial.println(pic_len);

    sensor_t * s = esp_camera_sensor_get();

    while (pic_len>59999 && nloop < 20) {
      Serial.println(F("Error: Camera image too large"));
      Serial.println(F("Trying again with lower quality image"));
      s->set_quality(s, camera_cfg_tmp.quality+nloop); // lower quality

      pic = esp_camera_fb_get();
      pic_len = pic->len;
      Serial.print(F("Pic len: "));
      Serial.println(pic_len);
      Serial.println(nloop);
      nloop++;
      if (nloop >= 19)
        return 666;
    }

    s->set_quality(s, camera_cfg_tmp.quality); // put back quality

    char* encoded = xbase64_encode((const uint8_t *)pic->buf, pic_len);

    measurement["image"]=encoded;
    measurement["nloop"]=nloop;

    esp_camera_fb_return(pic);
    misc_neoClear();

    JsonObject led = measurement.createNestedObject("led");
    led["brightness"] = brightness;
    led["R"] = r;
    led["G"] = g;
    led["B"] = b;
    JsonObject cam = measurement.createNestedObject("cam");
    cam["brightness"] = camera_cfg_tmp.brightness; 
    cam["contrast"] = camera_cfg_tmp.contrast; 
    cam["saturation"] = camera_cfg_tmp.saturation; 
    cam["special_effect"] = camera_cfg_tmp.special_effect; 
    cam["whitebal"] = camera_cfg_tmp.whitebal; 
    cam["awb_gain"] = camera_cfg_tmp.awb_gain; 
    cam["wb_mode"] = camera_cfg_tmp.wb_mode; 
    cam["exposure_ctrl"] = camera_cfg_tmp.exposure_ctrl; 
    cam["aec2"] = camera_cfg_tmp.aec2; 
    cam["ae_level"] = camera_cfg_tmp.ae_level; 
    cam["aec_value"] = camera_cfg_tmp.aec_value; 
    cam["gain_ctrl"] = camera_cfg_tmp.gain_ctrl; 
    cam["agc_gain"] = camera_cfg_tmp.agc_gain; 
    cam["gainceiling"] = camera_cfg_tmp.gainceiling; 
    cam["bpc"] = camera_cfg_tmp.bpc; 
    cam["wpc"] = camera_cfg_tmp.wpc; 
    cam["raw_gma"] = camera_cfg_tmp.raw_gma; 
    cam["lenc"] = camera_cfg_tmp.lenc; 
    cam["hmirror"] = camera_cfg_tmp.hmirror; 
    cam["vflip"] = camera_cfg_tmp.vflip; 
    cam["dcw"] = camera_cfg_tmp.dcw; 
    cam["colorbar"] = camera_cfg_tmp.colorbar; 
    measurement["type"] = ttype;
    JsonObject extra = doc.createNestedObject("extra");
    extra["cmd"] = _docCmd.as<String>();

    // Serialize JSON to file
    if (serializeJson(doc, file) == 0){
      Serial.println(F("Failed to write to file"));
      return 1;
    }
    file.close();
    Serial.println(F("File saved."));

    if (debug){
      if (comm_port){
        //Serial.println(measureJson(doc));
        serializeJson(doc, Serial);
      }else{
        SerialBT.println(measureJson(doc));
        serializeJson(doc, SerialBT);
      }
    }
    heap_caps_free(encoded);
  }
  return 0;
}

