/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : cam_pb.h
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20202110
 *
 * Purpose           : Header file for cam related functions. 
 *                     Hardware is OV2640
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20201021    lharnaldi   1.0    Initial version
 * 20201023    lharnaldi   1.1    Added the BT interface for rcv and snd data
 *
 * Copyright/License information.
 ***********************************************************************/

#ifndef _CAM_PB_H_
#define _CAM_PB_H_

#include "Arduino.h"
#include <ArduinoJson.h>
#include <base64.h>
#include "BluetoothSerial.h"
#include "FS.h"
#include "SD_MMC.h"
#include "time.h"
#include "esp_camera.h"
#include "fd_forward.h"

// Select camera model
//#define CAMERA_MODEL_WROVER_KIT
//#define CAMERA_MODEL_ESP_EYE
//#define CAMERA_MODEL_M5STACK_PSRAM
//#define CAMERA_MODEL_M5STACK_WIDE
#define CAMERA_MODEL_AI_THINKER

#include "camera_pins.h"
 
//To use external PSRAM with ArduinoJson
struct cam_SpiRamAllocator
{
  void* allocate(size_t size) {
    return heap_caps_malloc(size, MALLOC_CAP_SPIRAM);
  }
  void deallocate(void* pointer) {
    heap_caps_free(pointer);
  }
};

using cam_SpiRamJsonDocument = BasicJsonDocument<cam_SpiRamAllocator>;

struct camConfig {
	framesize_t framesize;
	pixformat_t pixformat;
	int quality;
  int brightness;
  int contrast;
  int saturation;
  int special_effect;
  int whitebal;
  int awb_gain;
  int wb_mode;
  int exposure_ctrl;
  int aec2;
  int ae_level;
  int aec_value;
  int gain_ctrl;
  int agc_gain;
  int gainceiling;
  int bpc;
  int wpc;
  int raw_gma;
  int lenc;
  int hmirror;
  int vflip;
  int dcw;
  int colorbar;
};

extern BluetoothSerial SerialBT;

extern camConfig camera_cfg_tmp;
extern const camConfig camera_cfg_default;

extern int currentVersion;
extern int currentMayorRevision;
extern int currentMinorRevision;
extern int currentDataVersion;

extern char strftime_buf[64];
extern int json_file_number;
extern int jsonDated_file_number;
extern int jpg_file_number;
extern struct tm timeinfo;
extern time_t now;
extern char *cid; 

extern int ds18b20_init(bool debug);
extern float ds18b20_getTemperature(int unit);
extern int misc_neoInit(int brightness);
extern int misc_neoSetColorRGB(int pixel, int r, int g, int b);
extern int misc_neoClear();
extern int misc_printLocalTime();
extern unsigned long misc_getEpochTime(bool debug);
extern int sd_createDir(fs::FS &fs, const JsonDocument& _docCmd);

esp_err_t cam_init(uint8_t debug);
esp_err_t cam_config(const JsonDocument& _docCmd, camConfig &actual_config, int init);
bool cam_checkPhoto(fs::FS &fs, const char* photoName);
esp_err_t cam_saveJsonPicDated(fs::FS &fs, const JsonDocument& _docCmd);
esp_err_t cam_saveJsonPicNumbered(fs::FS &fs, const JsonDocument& _docCmd);

#endif

