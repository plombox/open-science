/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : sys_pb.cpp
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20210525
 *
 * Purpose           : Cam related functions. Hardware is OV2640
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20210525    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#include "sys_pb.h"

/*
   sys_readSerCommand occurs whenever a new data comes in the hardware serial 
   interface. This routine is run between each time loop() runs, so using
   delay inside loop can delay response. Multiple bytes of data may be
   available.
   */
int sys_readSerCommand() {
  if(Serial.available()) {
    serInputString = Serial.readString();
    serStringComplete = true;
  }
  return 0;
}

/*
   sys_readBTCommand occurs whenever a new data comes in the Bluetooth serial
   interface. This routine is run between each time loop() runs, so using
   delay inside loop can delay response. Multiple bytes of data may be
   available.
   */
int sys_readBTCommand() {
  if(SerialBT.available()) {
    btInputString = SerialBT.readString();
    btStringComplete = true;
  }
  return 0;
}
void sys_restart() {

  Serial.println();
  Serial.println(F("restarting.."));
  Serial.println();

  //isResetting = true;

  //shutDownWiFi();

  EEPROM.end();
  Serial.end();

  delay(500);

  ESP.restart();
}

int sys_setTime(const JsonDocument& _docCmd){
  long epoch_time;
  uint8_t debug, comm_port;
  
  //default set system time to 1 May 2021
  if (_docCmd["param"]["epoch_time"] == nullptr) epoch_time = 1619903000; //epoch time
  else epoch_time = _docCmd["param"]["epoch_time"];
  if (_docCmd["param"]["debug"] == nullptr) debug = 1;
  else debug = _docCmd["param"]["debug"];
  if (_docCmd["param"]["comm_port"] == nullptr) comm_port = 1;
  else comm_port = _docCmd["param"]["comm_port"];

  if (debug) {
    if (comm_port) {  
      Serial.print(F("EPOCH time is "));
      Serial.println(epoch_time);
    } else {
      SerialBT.print(F("EPOCH time is "));
      SerialBT.println(epoch_time);
      }
    }
  
  misc_setTime(epoch_time);
  misc_getTime(debug,comm_port);

  return 0;
}

