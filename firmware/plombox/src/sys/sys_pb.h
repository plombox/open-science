/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : sys_pb.h
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20210525
 *
 * Purpose           : Header file for system wide functions. 
 *                    
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20200525    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#ifndef _SYS_PB_H_
#define _SYS_PB_H_

#include "Arduino.h"
#include "BluetoothSerial.h"
#include <ArduinoJson.h>
#include <EEPROM.h>

extern String serInputString;   
extern bool serStringComplete;
extern BluetoothSerial SerialBT;
extern String btInputString;  
extern bool btStringComplete;

extern int misc_getTime(uint8_t debug, uint8_t comm_port);
extern int misc_setTime(time_t epoch_time);

void sys_restart();
int sys_readSerCommand();
int sys_readBTCommand();
int sys_setTime(const JsonDocument& _docCmd);

#endif

