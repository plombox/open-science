/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : exp_pb.cpp
 * *
 * Author            : @lharnaldi
 *
 * Date created      : 20210105
 *
 * Purpose           : Functions and definitions related to the bluetooth
 *                     interface. 
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20210105    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#include "exp_pb.h"

/*
 * takeRGB experiment
 * The function expect:
 * {"cmd":"exp","arg":"takeRGB","param":{"r_exposure":100,"g_exposure":100,"b_exposure":100,"debug":1}}
 * send = 1: indicates if the file should be send through serialBT (comm_port=0)
 * or serial port (comm_port=1). send=0 indicates the file should be saved to
 * SD and not send through serial ports (serial nor serialBT).
 * */
esp_err_t exp_takeRGB(fs::FS &fs, const JsonDocument& _docCmd){
  const char * path;
  const char * type;
  unsigned int picN, picT, r_exposure, g_exposure, b_exposure;
  long current_millis;
  long last_capture_millis = 0;
  int pic_counter = 0, i = 0;

  StaticJsonDocument<512> _newDocCmd;
  deserializeJson(_newDocCmd, _docCmd.as<String>());


  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) path = "/";
  else path = _docCmd["param"]["path"];
  if (_docCmd["param"]["picN"] == nullptr) picN = 1;
  else picN = _docCmd["param"]["picN"];
  if (_docCmd["param"]["picT"] == nullptr) picT = 2000; //default 2s
  else picT = _docCmd["param"]["picT"];
  if (_docCmd["param"]["r_exposure"] == nullptr) r_exposure = 100;
  else r_exposure = _docCmd["param"]["r_exposure"];
  if (_docCmd["param"]["g_exposure"] == nullptr) g_exposure = 100;
  else g_exposure = _docCmd["param"]["g_exposure"];
  if (_docCmd["param"]["b_exposure"] == nullptr) b_exposure = 100;
  else b_exposure = _docCmd["param"]["b_exposure"];
  if (_docCmd["param"]["type"] == nullptr) type = "whitePaper";
  else type = _docCmd["param"]["type"];

  //create the directory if it is different to "/"
  if ((String)path != "/")
    sd_createDir(SD_MMC, _newDocCmd);

  time(&now);
  localtime_r(&now,&timeinfo);

  Serial.print(F("takeRGB measurement started at "));
  misc_printLocalTime();

  //do it once, at start
  if (timeinfo.tm_year < (2021 - 1900)) { // if time not set
    _newDocCmd["param"]["path"] = path;
    _newDocCmd["param"]["type"] = type;
    _newDocCmd["param"]["aec_value"] = r_exposure;
    cam_config(_newDocCmd, camera_cfg_tmp,0);
    _newDocCmd["param"]["r"]= 255;
    _newDocCmd["param"]["g"]= 0;
    _newDocCmd["param"]["b"]= 0;
    //serializeJson(_newDocCmd,Serial);
    cam_saveJsonPicNumbered(SD_MMC, _newDocCmd);

    _newDocCmd["param"]["aec_value"] = g_exposure;
    cam_config(_newDocCmd, camera_cfg_tmp,0);
    _newDocCmd["param"]["r"] = 0;
    _newDocCmd["param"]["g"] = 255;
    _newDocCmd["param"]["b"] = 0;
    //serializeJson(_newDocCmd,Serial);
    cam_saveJsonPicNumbered(SD_MMC, _newDocCmd);

    _newDocCmd["param"]["aec_value"] = b_exposure;
    cam_config(_newDocCmd, camera_cfg_tmp,0);
    _newDocCmd["param"]["r"] = 0;
    _newDocCmd["param"]["g"] = 0;
    _newDocCmd["param"]["b"] = 255;
    //serializeJson(_newDocCmd,Serial);
    cam_saveJsonPicNumbered(SD_MMC, _newDocCmd);
  } else {
    _newDocCmd["param"]["path"] = path;
    _newDocCmd["param"]["type"] = type;
    _newDocCmd["param"]["aec_value"] = r_exposure;
    cam_config(_newDocCmd, camera_cfg_tmp,0);
    _newDocCmd["param"]["r"]= 255;
    _newDocCmd["param"]["g"]= 0;
    _newDocCmd["param"]["b"]= 0;
    //serializeJson(_newDocCmd,Serial);
    cam_saveJsonPicDated(SD_MMC, _newDocCmd);

    _newDocCmd["param"]["aec_value"] = g_exposure;
    cam_config(_newDocCmd, camera_cfg_tmp,0);
    _newDocCmd["param"]["r"] = 0;
    _newDocCmd["param"]["g"] = 255;
    _newDocCmd["param"]["b"] = 0;
    //serializeJson(_newDocCmd,Serial);
    cam_saveJsonPicDated(SD_MMC, _newDocCmd);

    _newDocCmd["param"]["aec_value"] = b_exposure;
    cam_config(_newDocCmd, camera_cfg_tmp,0);
    _newDocCmd["param"]["r"] = 0;
    _newDocCmd["param"]["g"] = 0;
    _newDocCmd["param"]["b"] = 255;
    //serializeJson(_newDocCmd,Serial);
    cam_saveJsonPicDated(SD_MMC, _newDocCmd);
  }
  last_capture_millis = millis();
  pic_counter++;

  while(pic_counter < picN){
    current_millis = millis();
    if (current_millis - last_capture_millis > picT) { // Take another picture
      pic_counter++;
      last_capture_millis = millis();
      if (timeinfo.tm_year < (2021 - 1900)) { // if time not set
        _newDocCmd["param"]["path"] = path;
        _newDocCmd["param"]["type"] = type;
        _newDocCmd["param"]["aec_value"] = r_exposure;
        cam_config(_newDocCmd, camera_cfg_tmp,0);
        _newDocCmd["param"]["r"]= 255;
        _newDocCmd["param"]["g"]= 0;
        _newDocCmd["param"]["b"]= 0;
        //serializeJson(_newDocCmd,Serial);
        cam_saveJsonPicNumbered(SD_MMC, _newDocCmd);

        _newDocCmd["param"]["aec_value"] = g_exposure;
        cam_config(_newDocCmd, camera_cfg_tmp,0);
        _newDocCmd["param"]["r"] = 0;
        _newDocCmd["param"]["g"] = 255;
        _newDocCmd["param"]["b"] = 0;
        //serializeJson(_newDocCmd,Serial);
        cam_saveJsonPicNumbered(SD_MMC, _newDocCmd);

        _newDocCmd["param"]["aec_value"] = b_exposure;
        cam_config(_newDocCmd, camera_cfg_tmp,0);
        _newDocCmd["param"]["r"] = 0;
        _newDocCmd["param"]["g"] = 0;
        _newDocCmd["param"]["b"] = 255;
        //serializeJson(_newDocCmd,Serial);
        cam_saveJsonPicNumbered(SD_MMC, _newDocCmd);
      } else {
        _newDocCmd["param"]["path"] = path;
        _newDocCmd["param"]["type"] = type;
        _newDocCmd["param"]["aec_value"] = r_exposure;
        cam_config(_newDocCmd, camera_cfg_tmp,0);
        _newDocCmd["param"]["r"]= 255;
        _newDocCmd["param"]["g"]= 0;
        _newDocCmd["param"]["b"]= 0;
        //serializeJson(_newDocCmd,Serial);
        cam_saveJsonPicDated(SD_MMC, _newDocCmd);

        _newDocCmd["param"]["aec_value"] = g_exposure;
        cam_config(_newDocCmd, camera_cfg_tmp,0);
        _newDocCmd["param"]["r"] = 0;
        _newDocCmd["param"]["g"] = 255;
        _newDocCmd["param"]["b"] = 0;
        //serializeJson(_newDocCmd,Serial);
        cam_saveJsonPicDated(SD_MMC, _newDocCmd);

        _newDocCmd["param"]["aec_value"] = b_exposure;
        cam_config(_newDocCmd, camera_cfg_tmp,0);
        _newDocCmd["param"]["r"] = 0;
        _newDocCmd["param"]["g"] = 0;
        _newDocCmd["param"]["b"] = 255;
        //serializeJson(_newDocCmd,Serial);
        cam_saveJsonPicDated(SD_MMC, _newDocCmd);
      }
    }
  }

  Serial.println();
  Serial.print(F("takeRGB calibration ended at "));
  misc_printLocalTime();
  Serial.print("{\"Npic\":\"");
  Serial.print((String)picN);
  Serial.print("\"}");
  return ESP_OK;
}

/*
 * Timelapse experiment
 * The function expect:
 * {picTimeLapse:"path",picN:np,picT:ti,optPar}
 * Where optPar is an array of:
 * [nled,brightness, r, g, b, send, comm_port]
 * send = 1: indicates if the file should be send through serialBT (comm_port=0)
 * or serial (comm_port=1) port. send=0 indicates thee file should be saved to
 * SD and not send through serial ports (serial nor serialBT).
 * */
esp_err_t exp_timeLapse(fs::FS &fs, const JsonDocument& _docCmd){
  const char * path;
  unsigned int picN, picT;
  long current_millis;
  long last_capture_millis = 0;
  int pic_counter = 0;

  //Set default value if not present
  if (_docCmd["param"]["path"] == nullptr) path = "/";
  else path = _docCmd["param"]["path"];
  if (_docCmd["param"]["picN"] == nullptr) picN = 1;
  else picN = _docCmd["param"]["picN"];
  if (_docCmd["param"]["picT"] == nullptr) picT = 10000; //default 10s
  else picT = _docCmd["param"]["picT"];

  //create the directory if it is different to "/"
  if ((String)path != "/")
    sd_createDir(SD_MMC, _docCmd);

  time(&now);
  localtime_r(&now,&timeinfo);

  Serial.print(F("Timelapse experiment started at "));
  misc_printLocalTime();

  //do it once, at start
  if (timeinfo.tm_year < (2021 - 1900)) { // if time not set
    cam_saveJsonPicNumbered(SD_MMC, _docCmd);
  } else {
    cam_saveJsonPicDated(SD_MMC, _docCmd);
  }
  last_capture_millis = millis();
  pic_counter++;

  while(pic_counter < picN){
    current_millis = millis();
    if (current_millis - last_capture_millis > picT) { // Take another picture
      pic_counter++;
      last_capture_millis = millis();
      if (timeinfo.tm_year < (2021 - 1900)) { // if time not set
        cam_saveJsonPicNumbered(SD_MMC, _docCmd);
      } else {
        cam_saveJsonPicDated(SD_MMC, _docCmd);
      }
    }
  }
  Serial.print(F("Timelapse experiment ended at "));
  misc_printLocalTime();
  Serial.print("{\"Npic\":\"");
  Serial.print((String)picN);
  Serial.print("\"}");
  return ESP_OK;
}

/*
 * exp_takeNsendBTPic (aka command 3 in the first version of the firmware)
 * This funcion take a photo and send it through the bluetooth interface
 * */
esp_err_t exp_takeNsendBTPic(const JsonDocument& _docCmd){
  uint8_t isNumbered;

  //Set default value if not present
  if (_docCmd["param"]["isNumbered"] == nullptr) isNumbered = 0;
  else isNumbered = _docCmd["param"]["isNumbered"];

  if (isNumbered)
  {
    cam_saveJsonPicNumbered(SD_MMC, _docCmd);
  }else{
    cam_saveJsonPicDated(SD_MMC, _docCmd);
  }
  return ESP_OK;
}

/*
 * Check if photo capture was successful
 */
bool exp_checkPhoto( fs::FS &fs , const char* photoName) {
  File f_pic = fs.open( photoName );
  unsigned int pic_sz = f_pic.size();
  return ( pic_sz > 600 );
}


