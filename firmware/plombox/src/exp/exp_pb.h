/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : exp_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20210105
*
* Purpose           : Header file for the experiment functions.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20210105    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _EXP_PB_H_
#define _EXP_PB_H_

#include <stdio.h>
#include <string.h>
#include "Arduino.h"
#include <ArduinoJson.h>
#include "FS.h"
#include "SD_MMC.h"
#include "time.h"
#include "esp_camera.h"

extern struct camConfig camera_cfg_tmp;
extern struct tm timeinfo;
extern time_t now;

extern int misc_printLocalTime();
extern int sd_createDir(fs::FS &fs, const JsonDocument& _docCmd);
extern esp_err_t cam_config(const JsonDocument& _docCmd, struct camConfig &actual_config, int init);
extern esp_err_t cam_savePicDated(fs::FS &fs, const JsonDocument& _docCmd);
extern esp_err_t cam_saveJsonPicDated(fs::FS &fs, const JsonDocument& _docCmd);
extern esp_err_t cam_savePicNumbered(fs::FS &fs, const char * path);
extern esp_err_t cam_saveJsonPicNumbered(fs::FS &fs, const JsonDocument& _docCmd);

esp_err_t exp_takeRGB(fs::FS &fs, const JsonDocument& _docCmd);
esp_err_t exp_timeLapse(fs::FS &fs, const JsonDocument& _docCmd);
esp_err_t exp_takeNsendBTPic(const JsonDocument& _docCmd);
bool exp_checkPhoto( fs::FS &fs , const char* photoName);

#endif
