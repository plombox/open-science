/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : misc_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : Header file for the miscellaneous functions.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _MISC_PB_H_
#define _MISC_PB_H_

#include "Arduino.h"
#include "BluetoothSerial.h"
#include <EEPROM.h>
#include <Adafruit_NeoPixel.h>
#include <sys/time.h>
#include <errno.h>

extern Adafruit_NeoPixel strip;
extern BluetoothSerial SerialBT;
extern const int led_pin;
extern char * cid; //chi ID

char *misc_getChipID();
int misc_blink(int led_pin, int ntimes, uint32_t msec);
int misc_printMsg(char* msg, uint8_t debug, uint8_t comm_port);
int misc_getTime(uint8_t debug, uint8_t comm_port);
unsigned long misc_getEpochTime(bool debug);
int misc_setTime(time_t epoch_time);
int misc_printLocalTime();

int misc_neoInit(int brightness);
int misc_neoClear();
int misc_neoSetColorGRBW(int pixel, int g, int r, int b, int w);
int misc_neoSetColorRGB(int pixel, int r, int g, int b);
int misc_neoColorWipe(uint32_t c, uint8_t wait);
int misc_neoRainbow(uint8_t wait);
int misc_neoRainbowCycle(uint8_t wait);
int misc_neoTheaterChase(uint32_t c, uint8_t wait);
int misc_neoTheaterChaseRainbow(uint8_t wait);
uint32_t misc_neoWheel(byte WheelPos);

#endif
