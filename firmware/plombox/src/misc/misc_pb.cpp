/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : misc_pb.cpp
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20202110
 *
 * Purpose           : Miscellaneous functions.
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20201021    lharnaldi   1.0    Initial version
 *
 * Copyright/License information.
 ***********************************************************************/

#include "misc_pb.h"

/*
 * Get the chip ID (MAC address) from the efuses
 * */
char *misc_getChipID(){
  char * cid = (char *) malloc(13);
  uint64_t chipid = ESP.getEfuseMac(); // The chip ID is essentially its MAC address(length: 6 bytes).
  uint16_t chip = (uint16_t)(chipid >> 32);
  snprintf(cid, 13, "%04X%08X", chip, (uint32_t)chipid);
  //Serial.print(cid);

  return cid;
}

/*
 * Blink the hardware LED from ESP32-CAM module
 **/
int misc_blink(int led_pin, int ntimes, uint32_t msec){
  int i=0;

  do
  {
    digitalWrite(led_pin, HIGH);
    delay(msec);
    digitalWrite(led_pin,LOW);
    delay(msec);
    i++;
  }while(i<ntimes);
  digitalWrite(led_pin,HIGH); //just for sure ;)
  return 0;
}

/*
 * Print message to the SerialBT (comm_port=0) or Serial (comm_port=1) port if degug
 * enabled.
 * */
int misc_printMsg(char* msg, uint8_t debug, uint8_t comm_port){
  if (debug){
    if (comm_port){
      Serial.println(msg);
    } else {
      SerialBT.println(msg);
    }
  }

  return 0;
}

/*
 *Get time 
 * from https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/system_time.html
 * */
int misc_getTime(uint8_t debug, uint8_t comm_port){
  time_t now;
  char strftime_buf[64];
  struct tm timeinfo;

  time(&now);
  // Example setting timezone to China Standard Time
  //setenv("TZ", "CST-8", 1);
  //tzset();

  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);

  if (debug){
    if (comm_port){
      Serial.print(F("The system time is (UTC): "));
      Serial.println(strftime_buf);
    } else {
      SerialBT.print(F("The system time is (UTC): "));
      SerialBT.println(strftime_buf);
    }
  }

  //If you need to obtain time with one microsecond resolution, use the code snippet below:

  //struct timeval tv_now;
  //gettimeofday(&tv_now, NULL);
  //int64_t time_us = (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;
  return 0;
}

/*
 * Function that gets current epoch time
 * */
unsigned long misc_getEpochTime(bool debug) {
  time_t now;
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    //Serial.println("Failed to obtain time");
    return 0;
  }
  time(&now);
  if (debug){
    Serial.print(F("Epoch time is: "));
    Serial.println(now);
  }

  return now;
}

/*
 * Set time in system
 * NOTE: it can be used the command: "date +%s" in a Linux machine to put into
 * the epoch_time input
 * */
int misc_setTime(time_t epoch_time){
  timeval epoch = {epoch_time, 0};
  int rc;

  rc=settimeofday((const timeval*)&epoch, 0);
  if(!rc) {
    Serial.println(F("Time setting...                OK"));
    return 0;
  }
  else {
    Serial.print(F("settimeofday() failed, errno = "));
    Serial.println(errno);
    return -1;
  }
}

/*
 * Print the local time
 * */
int misc_printLocalTime(){
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return 1;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  return 0;
}

/*
 * Initialize the LED ring comunication
 * */
int misc_neoInit(int brightness){
  strip.begin();
  strip.setBrightness(brightness);
  strip.show(); // Initialize all pixels to 'off'

  return 0;
}

/*
 * void misc_neoSetColorGRBW(int pixel, int brightness, int g, int r, int b, int w)
 * */
int misc_neoSetColorGRBW(int pixel, int g, int r, int b, int w){
  strip.clear(); // Set all pixel colors to 'off'
  //strip.setBrightness(brightness);
  // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
  // Here we're using a moderately bright green color:
  //strip.setPixelColor(pixel, strip.Color(r, g, b));
  strip.setPixelColor(pixel, g, r, b, w);

  strip.show();   // Send the updated pixel colors to the hardware.
  return 0;
  //delay(DELAYVAL); // Pause before next pass through loop
}

int misc_neoClear(){
  strip.clear();
  strip.show();   
  return 0;
}

/* 
 * void misc_neoSetColorRGB(int pixel, int brightness, int r, int g, int b)
 * */
int misc_neoSetColorRGB(int pixel, int r, int g, int b){
  strip.setPixelColor(pixel, r, g, b);
  strip.show();   
  return 0;
}

/*
 * Input a value 0 to 255 to get a color value.
 * The colours are a transition r - g - b - back to r.
 * */
uint32_t misc_neoWheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

/*
 * Fill the dots one after the other with a color
 * */
int misc_neoColorWipe(uint32_t c, uint8_t wait){
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
  return 0;
}

int misc_neoRainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, misc_neoWheel((i+j) & 255));
    }
    strip.show();
    delay(wait);
  }
  return 0;
}

/*
 * Slightly different, this makes the rainbow equally distributed throughout
 * */
int misc_neoRainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, misc_neoWheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
  return 0;
}

/*
 * Theatre-style crawling lights.
 **/
int misc_neoTheaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
  return 0;
}

/*
 * Theatre-style crawling lights with rainbow effect
 * */
int misc_neoTheaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, misc_neoWheel( (i+j) % 255));    //turn every third pixel on
      }
      strip.show();

      delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
  return 0;
}
