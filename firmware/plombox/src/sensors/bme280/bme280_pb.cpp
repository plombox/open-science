/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : bme280_pb.cpp
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : BME280 sensor related functions. 
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#include "bme280_pb.h"

BME280 bme;
 

// Get BME280 sensor readings and return them as a String variable
String bme280_getReadings(){
  float temperature, humidity;
  temperature = bme.readTempC();
  //temperature = bme.readTempF();
  humidity = bme.readFloatHumidity();
  String message = "Temperature: " + String(temperature) + " ºC \n";
  message += "Humidity: " + String (humidity) + " % \n";
  return message;
}

void bme280_init(){
  
  // Init BME280 sensor
  Wire.begin(bme280_sda_pin, bme280_scl_pin);
  bme.settings.commInterface = I2C_MODE;
  bme.settings.I2CAddress = 0x76;
  bme.settings.runMode = 3;
  bme.settings.tStandby = 0;
  bme.settings.filter = 0;
  bme.settings.tempOverSample = 1;
  bme.settings.pressOverSample = 1;
  bme.settings.humidOverSample = 1;
  bme.begin();
}
