/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : bme280_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : Header file for the BME280 sensor related functions.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _BME280_PB_H_
#define _BME280_PB_H_

#include "Arduino.h"
#include <Wire.h>
#include "SparkFunBME280.h"

extern const int bme280_sda_pin;
extern const int bme280_scl_pin;

void bme280_init();
String bme280_getReadings();

#endif
