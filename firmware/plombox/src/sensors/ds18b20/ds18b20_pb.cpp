/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : ds18b20_pb.cpp
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : DS18B20 temperature sensor related functions. 
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#include "ds18b20_pb.h"

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ds18b20_pin);

// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature ds18b20(&oneWire);


int ds18b20_init(bool debug)
{
  if (debug)
  {
    Serial.println(F("Initializing DS18B20 sensor... OK"));
    //Serial.println(ds18b20_pin);
  }

  ds18b20.begin();
  ds18b20.requestTemperatures(); // Send the command to get temperatures
  ds18b20.getTempCByIndex(0);
  return 0;
}

//retur temperature in the requested unit
//unit can be "C" for Celsius or "F" for Fahrenheit
float ds18b20_getTemperature(int unit) 
{
  float temperature = 0;

  ds18b20.requestTemperatures(); 
  
  if (unit == 0)
    temperature = ds18b20.getTempCByIndex(0);
  else if (unit == 1)
    temperature = ds18b20.getTempFByIndex(0);
  else
  {
    Serial.println(F("Please, specify the unit as '0' for 'Celsius' or '1' for 'Fahrenheit'"));
    return -1;
  }

  return temperature;
}
