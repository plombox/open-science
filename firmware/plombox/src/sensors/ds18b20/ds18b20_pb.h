/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : ds18b20_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20202110
*
* Purpose           : Header file for the DS18B20 temperature sensor related 
*                     functions. 
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _DS18B20_PB_H_
#define _DS18B20_PB_H_

#include "Arduino.h" //access to Serial, pinMode, etc
#include <OneWire.h>
#include <DallasTemperature.h>

extern const int ds18b20_pin;

int ds18b20_init(bool debug);
float ds18b20_getTemperature(int unit);

#endif
