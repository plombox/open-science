/***********************************************************************;
 * Project           : Plombox, WP2, ESP32-CAM sub-task
 *
 * Program name      : plombox.ino
 *
 * Author            : @lharnaldi
 *
 * Date created      : 20201021
 *
 * Purpose           : Main program for the Plombox device
 *
 * Revision History  :
 *
 * Date        Author      Ref    Revision (Date in YYYYMMDD format) 
 * 20201021    lharnaldi   1.00   Initial version
 * 20201216    lharnaldi   1.01   Change the base64 library to solve 
 *                                the newline issue in the JSON strings
 *
 * Copyright/License information.
 ***********************************************************************/

#include "config_pb.h"

void setup() {

  Serial.begin(115200);

  Serial.println();
  Serial.println(F("----------------------------------------"));
  Serial.print(F("--"));
  Serial.print(F("    Starting the Plombox device     "));
  Serial.println(F("--"));
  Serial.print(F("--"));
  Serial.print(F("    Firmware version is "));
  Serial.print(F("v"));
  Serial.print(currentVersion);
  Serial.print(F("r"));
  Serial.print(currentMayorRevision);
  Serial.print(currentMinorRevision);
  Serial.print(F("       "));
  Serial.println(F("--"));
  Serial.println(F("----------------------------------------"));
  Serial.println();

  //Configure LED pin as output
  pinMode(led_pin, OUTPUT);

  //blink led
  misc_blink(led_pin,2,500);

  //SD initialization
  sd_init(1);
  delay(300);

  //set system time to 1 May 2021
  misc_setTime((time_t)1619903000); //epoch time
  delay(200);

  //Camera initialization
  cam_init(1);

  //start DS18B20 sensor communication
  ds18b20_init(1);
  delay(200);

  misc_neoInit(255);

  // initialize EEPROM with predefined size
  //EEPROM.begin(EEPROM_SIZE);

  cid = misc_getChipID();

  //BT initialization
  bt_init(1);
  delay(200);
  // reserve 200 bytes for the inputString:
  //bt_inputString.reserve(200);

  misc_getTime(1,1);
  delay(200);
  //blink led
  misc_blink(led_pin,2,500);

  delay(1000);

}

void loop() {

  sys_readSerCommand();
  sys_readBTCommand();

  if(serStringComplete){
    cmdJson = serInputString;
    _docCmd["comm_port"] = 0; //use serial port as response
  }
  if(btStringComplete){
    cmdJson = btInputString;
    _docCmd["comm_port"] = 1; //use BT port as response
  }

  if(serStringComplete|btStringComplete){
    btStringComplete = false;
    serStringComplete = false;

    Serial.print(F("Received: "));
    Serial.println(cmdJson);
    cmdJson.trim();
    DeserializationError error = deserializeJson(_docCmd, cmdJson);
    if (error) {
    Serial.println(F("Failed to read command. Please insert a valid one"));
    return;
    }
    parser_json(_docCmd);
  }
}

int parser_json(const JsonDocument& _docCmd) {
  // Test if parsing succeeds.
  if (_docCmd.isNull()) {
    Serial.print(F("Please insert a valid command."));
    return 1;
  } else {
    Serial.println(F("A valid json case found!"));
    Serial.println(F("Now doing the loop through the keys"));

    if (_docCmd["cmd"] != nullptr)    {
      //do SD operations
      if (_docCmd["cmd"] == "sd"){ 
        if (_docCmd["arg"] != nullptr) {
          if (_docCmd["arg"] == "ls"){
            sd_fetchDMetaSerial(SD_MMC, _docCmd);
            //sd_listDir(SD_MMC, _docCmd);
          } else if (_docCmd["arg"] == "mkdir"){
            sd_createDir(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "rmdir"){
            sd_removeDir(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "rdf"){
            sd_readFile(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "wrf"){
            sd_writeFile(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "appendf"){
            sd_appendFile(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "renamef"){
            sd_renameFile(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "rmf"){
            sd_deleteFile(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "dnf"){
            int nof = sd_dirNumOfFiles(SD_MMC, _docCmd);
            Serial.print("Number of files in ");
            Serial.print(_docCmd["param"]["path"].as<String>());
            Serial.print(" : ");
            Serial.println(nof);
          }else if (_docCmd["arg"] == "fmd"){
            sd_fetchDMetaSerial(SD_MMC, _docCmd);
          }
        }
      }else if (_docCmd["cmd"] == "cam"){ 
        if (_docCmd["arg"] != nullptr) {
          Serial.println(F("A camera (cam) command received!."));
          if (_docCmd["arg"] == "cfgSet"){ //Set configuration
            cam_config(_docCmd, camera_cfg_tmp,0);
          } else {
            Serial.println(F("Please insert a valid system (sys) command."));
          }
        }
      }else if (_docCmd["cmd"] == "multi"){
         if (_docCmd["arg"] != nullptr) {
           for (int i=0;i<_docCmd["arg"].size();i++) {
              String repo = _docCmd["arg"][i];
              Serial.println(repo);
              StaticJsonDocument<512> _multiCmd;
               DeserializationError error = deserializeJson(_multiCmd, repo);  
              if (!error){
                parser_json(_multiCmd);
              }
              _multiCmd.clear();
            }
         }
      }else if (_docCmd["cmd"] == "sys"){
        if (_docCmd["arg"] != nullptr) {
          Serial.println(F("A system (sys) command received!."));
          if (_docCmd["arg"] == "sst"){ //set system time
            sys_setTime(_docCmd);
          } else if (_docCmd["arg"] == "restart"){
            Serial.println();
            Serial.println();
            Serial.println(F("***** RESTART CMD RECEIVED! *****"));
            Serial.println(F("Restarting the system..."));
            Serial.println();
            Serial.println();
            ESP.restart();
          } else {
            Serial.println(F("Please insert a valid system (sys) command."));
          }
        }
      }else if (_docCmd["cmd"] == "exp"){
        if (_docCmd["arg"] != nullptr) {
          if (_docCmd["arg"] == "picTimeLapse"){
            if (_docCmd["param"]["path"] != nullptr && _docCmd["param"]["path"] != "/"){
              String path = _docCmd["param"]["path"];
              if (path.endsWith("/")){
                  Serial.print("error: unless it's the root, path can't end with /");
                  return 1;
              }
             }
            exp_timeLapse(SD_MMC, _docCmd);
          }else if (_docCmd["arg"] == "picTakeNSend"){
            exp_takeNsendBTPic(_docCmd);
          }else if (_docCmd["arg"] == "takeRGB"){
            exp_takeRGB(SD_MMC, _docCmd);
          }else{
            Serial.println(F("exp: timelapse -> insert a valid command."));
          }
        }
      }else{
        Serial.println(F("Please provide a valid command."));
        Serial.println(F("Commands are:"));
        Serial.println(F("sys    -> for system operations."));
        Serial.println(F("multi  -> for multiple (loop) operations."));
        Serial.println(F("sd     -> for SD operations."));
        Serial.println(F("exp    -> experiment related operations."));
      }
    } else {
      Serial.print(F("cmd not received. Please insert a valid JSON string."));
      return 1;
    }
  }
  return 0;
}
