/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : config_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20201021
*
* Purpose           : Configuration and pin definitions.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20201021    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

/* Pinout definition/connections in the ESP32-CAM platform
 
Pin  Function   CAM       SD        PSRAM     Comments
1    GND        
2    VDD33  
3    CHP_PU                                   RESET_BTN
4    SENSOR_VP  CSI_D4
5    SENSOR_VN  SI_D5
6    GPIO34     CSI_D6
7    GPIO35     CSI_D7
8    GPIO32     CAM_PWR
9    GPIO33                                   LED
10   GPIO25     CSI_VSYNC
11   GPIO26     TWI_SDA
12   GPIO27     TWI_SCK
13   GPIO14               HS2_CLK
14   GPIO12               HS2_DATA2
15   GND
16   GPIO13               HS2_DATA3
17   SHD/SD2                        SD2
18   SWP/SD3                        SD3
19   SCS/CMD                        CMD
20   SCK/CLK                        CLK
21   SDO/SD0                        SD0
22   SDI/SD1                        SD1
23   GPIO15               HS2_CMD
24   GPIO2                HS2_DATA0
25   GPIO0      CSI_MCLK
26   GPIO4                HS2_DATA1           LED_FLASH
27   GPIO16                                   U2RXD
28   GPIO17                         PSRAM_CLK
29   GPIO5      CSI_D0
30   GPIO18     CSI_D1
31   GPIO19     CSI_D2
32   NC
33   GPIO21     CSI_D3
34   U0RXD                                    Serial RxD
35   U0TXD                                    Serial TxD
36   GPIO22     CSI_PCLK
37   GPIO23     CSI_HSYNC
38   GND
39   GND
******************************************************************************/

//******************************************************************************
//            CONFIGURATION
//******************************************************************************
#ifndef _CONFIG_PB_H_
#define _CONFIG_PB_H_

#include <EEPROM.h>            
#include "version_pb.h"       
#include "src/cam/cam_pb.h"
#include "src/sys/sys_pb.h"
#include "src/misc/misc_pb.h"
#include "src/bt/bt_pb.h"
#include "src/sd/sd_pb.h"
#include "src/exp/exp_pb.h"
#include "src/sensors/ds18b20/ds18b20_pb.h"
//#include "src/sensors/bme280/bme280_pb.h"

//To use new or old version of Plombox PCB
#define NEWPINS

#define JSON_SIZE       1024 //Needs to be here to be visible for all

#ifdef NEWPINS
#define SDA_PIN         13
#define SCL_PIN         15
#define NEOPIXEL_PIN    4 // Which pin on the Arduino is connected to the NeoPixels?
#else
#define SDA_PIN         12
#define SCL_PIN         13
#define NEOPIXEL_PIN    12 // Which pin on the Arduino is connected to the NeoPixels?
#endif

#define EEPROM_SIZE     1  // define the number of bytes you want to access
#define NUMPIXELS       6 // How many NeoPixels are attached to the Arduino?
#define ILLUMINATOR_PIN 0
#define FLASH_LED_PIN   4  //flash LED
#define FILE_LOG        "testlog.csv"   //File to save data

//Global variables
char * cid               = NULL; //Store Chip ID globally
uint8_t error            = 0;
int json_file_number     = 0;                       
int jsonDated_file_number= 0;                       
int jpg_file_number      = 0;                       
String cmdJson;

StaticJsonDocument<JSON_SIZE> _docCmd;
int parser_json(const JsonDocument& _docCmd);
// DS18B20 related variables
const int ds18b20_pin    = 13;

// LED builtin (the red one on ESP32-CAM) pin GPIO33
const int led_pin        = 33;

//Serial port related variables
String serInputString    = "";      // a String to hold incoming data
bool serStringComplete   = false;  // whether the string is complete
//Bluetooth related variables
BluetoothSerial SerialBT;
String btInputString    = "";     // a String to hold incoming data
bool btStringComplete   = false;  // whether the string is complete

//  Time related variables
uint8_t second, minute, hour, wday, day, month, year, ctrl;
char strftime_buf[64];                      
struct tm timeinfo;                              
time_t now;           

Adafruit_NeoPixel strip(NUMPIXELS, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);

struct pb_header {
	uint8_t version;
	uint8_t esp32;
	time_t date;
	struct user{
		char name[16];
		uint8_t ID;
	};
	struct meas{
		uint8_t ID;
		char location[64];
		char image [4096];
	};
  char hostname[64];
  int port;
};

struct camConfig camera_cfg_tmp;                     
const struct camConfig camera_cfg_default = {       
	.framesize = FRAMESIZE_UXGA, // maximum, was FRAMESIZE_VGA,       //0 to 10
	.pixformat = PIXFORMAT_JPEG,       //0 to 7
	.quality = 2,          // high quality, has automatic recovery if too high. 0 to 63
  .brightness = 0,       //-2 to 2
  .contrast = 0,         //-2 to 2
  .saturation = 0,      //-2 to 2
  .special_effect = 0,   // 0 to 6 (0-No Effect, 1-Negative, 2-Grayscale, 3-Red Tint, 4-Green Tint, 5-Blue Tint, 6-Sepia)
  .whitebal = 0,         // 0 = disable , 1 = enable
  .awb_gain = 0,         // 0 = disable , 1 = enable
  .wb_mode = 0,          // 0 to 4 - if awb_gain enabled (0 - Auto, 1 - Sunny, 2 - Cloudy, 3 - Office, 4 - Home)
  .exposure_ctrl = 0,    // 0 = disable , 1 = enable
  .aec2 = 0,             // 0 = disable , 1 = enable
  .ae_level = 0,         // -2 to 2
  .aec_value = 200,      // 0 to 1200
  .gain_ctrl = 0,        // 0 = disable , 1 = enable
  .agc_gain = 0,         // 0 to 30
  .gainceiling = 0,      // 0 to 6
  .bpc = 0,              // 0 = disable , 1 = enable
  .wpc = 0,              // 0 = disable , 1 = enable
  .raw_gma = 1,          // 0 = disable , 1 = enable
  .lenc = 0,             // 0 = disable , 1 = enable
  .hmirror = 0,          // 0 = disable , 1 = enable
  .vflip = 0,            // 0 = disable , 1 = enable
  .dcw = 1,              // 0 = disable , 1 = enable
  .colorbar = 0          // 0 = disable , 1 = enable
};

#endif
