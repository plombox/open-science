/***********************************************************************;
* Project           : Plombox, WP2, ESP32-CAM sub-task
*
* Program name      : version_pb.h
*
* Author            : @lharnaldi
*
* Date created      : 20211005
*
* Purpose           : Configuration and pin definitions.
*
* Revision History  :
*
* Date        Author      Ref    Revision (Date in YYYYMMDD format) 
* 20211005    lharnaldi   1.0    Initial version
*
* Copyright/License information.
***********************************************************************/

#ifndef _VERSION_PB_H_
#define _VERSION_PB_H_

int currentVersion       = 1;
int currentMayorRevision = 5;
int currentMinorRevision = 4;
int currentDataVersion   = 1;

#endif
