# PlomBox ESP32-cam firmware installation <img align="right" width="100" height="100" src="https://plombox.org/images/pbox400.jpg">

## Introduction ##

<img align="left" width="200" src="https://www.hwlibre.com/wp-content/uploads/2020/12/esp32-cam-1024x576.jpg">This living document details the necessary installation steps needed to build and operate from scratch the new official and stable release of the 
[plomBox](https://plombox.org/) device based on the [AI Thinker ESP32-cam](https://diyprojects.io/esp32-cam-which-model-choose-esp-eye-aithinker-ttgo-tcamera-m5stack/#.YKAfSnX7RNg) device. 

## Pinout and ESP32 connections

The ESP32-cam is a device based on the [ESP32-eye](https://www.espressif.com/en/products/devkits/esp-eye/overview) board by [Espressiff](https://www.espressif.com/en/). In the image below it can be seen the ESP32-cam pinout: 
<img align="center" width="500" src="https://www.hwlibre.com/wp-content/uploads/2020/12/esp-32-pinout-1024x576.jpg">

and the basic, minimal connection, to operate it in the ARDUINO IDE framework trough a standard programmer such as the FT232RL 3.3v-5.5v FTDI-A TTL:
<img align="center" width="500" src="https://www.hwlibre.com/wp-content/uploads/2020/12/esp32-cam-ftdi-1024x666.jpg">

***Very Important Notice***: *The green shortcut between `GPIO0` and `GND` is only to enable the programming mode. For normal operation it should not be used*. ***Please don't say we did not warn you.***

This is the minimal set needed to connect and programme the ESP32-cam device with the ARDUINO IDE as it is explained in the next section. 

## Pre-requisities

### Python version and python libraries

The PlomBox implementation requires python3 properly installed in your system. These are some useful python3 packages:
```bash
$ sudo apt install python3 python-is-python3 python3-numpy python3-scipy ipython3 python3-matplotlib python3-simpy libbluetooth-dev

$ sudo apt install jupyter-client jupyter-console jupyter-nbconvert jupyter-notebook jupyter-sphinx-theme-common

$ sudo apt install python-pandas python-geopandas
```
Please note these packages are for the [ubuntu](https://ubuntu.com/) OS. I am sure you can do the necessary adjustments if you are using a different distro. And if you own a Mac computer, please ask yourself if you are on the right job.

Then, to check that python3 is you base python environment, open a terminal and run:

```bash
$ sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1
```

To work with the ESP32 board, the `pyserial` and `pybluez` packages should be installed. You have several ways to install it, such as the typical way:
```bash
$ sudo apt install python3-serial
```
or by using the `pip` command installation tool:
```bash
$ sudo apt install python3-pip
$ pip3 install pyserial
$ pip3 install pybluez
```
and, in this case, please be sure that`/home/<username>/.local/bin` is added to your `$PATH` environment variable. If you don't know what that means please consider a law career as a good option for you.

[Anaconda](https://www.anaconda.com/products/individual) is also a good option:
```bash
(base) $ conda install pyserial
```
but you will need to do some additional steps in order to properly [run python and Arduino IDE](https://www.hwlibre.com/anaconda-python-arduino/#API_para_usar_con_Arduino) (don't tested, use at your own risk.)

### Arduino IDE

Arduino IDE can be downloaded from the [arduino.cc software download site](https://www.arduino.cc/en/software). The current version is ARDUINO IDE 1.8.15. After download, select the installation path and then:

```bash
$ ver=1.8.15
$ tar xfv arduino-${ver}-linux64.tar.xz
$ cd arduino-${ver}
$ sudo ./install.sh
```
and follow further instructions.

In case you are on a Windows OS and need to install the serial COM port drivers
for the programmer, you can download them from [https://www.usb-drivers.org/ft232r-usb-uart-driver.html](https://www.usb-drivers.org/ft232r-usb-uart-driver.html).

### ESP32-cam board for Arduino IDE

Start the Arduino ide in your system, and then: 

1. Go to `Menu -> File -> Preferences` and in the `Additional Boards Manager URLs` add: 
[https://dl.espressif.com/dl/package_esp32_index.json](https://dl.espressif.com/dl/package_esp32_index.json) 
and press OK.
   
2. Go to `Menu -> Tools -> Boards -> Boards manager...` and search for **esp32**. Then, install the **esp32** boards manager by **Espressif Systems**. Current version is **1.0.6**. Once it is installed, press close.

3. Again, go to `Menu -> Tools -> Boards` and now it should show a new `ESP32` menu. Open it and select the **AI-Thinker ESP32-CAM** board. 

4. Connect the programmer with the USB-mini (or USB-micro) cable to the PC. In the same `Tools` menú it should show `Port -> /dev/ttyUSB0`. If port appears in grey and no other options are shown, but you are sure the ESP32-cam is properly connected to the USB port, then it is highly probable you have a permission issue with the USBs port. In that case, open a terminal and execute:
```bash
$ sudo usermod -a -G dialout <username>
```
replace `<username>` by your actual `username`. ***And again, if you didn't know that, probably a law career is a better choice for you.*** And then, logout and login (or reboot) to enable the new permissions. There are some workarounds in StackOverflow to avoid it, but they never work. 

5. And, that is. If everything was correct, at the bottom right of the status line in the Arduino IDE main window, it should appear something like this:
![img.png](status.png)
   
### Arduino Libraries dependencies

There is a list of Arduino Libraries should be installed before to run the ESP32-cam firmware: 

1. **SparkFun BME280 Library**

The Adafruit BME280 conflicts with some of the ESP32-CAM libraries. So, to avoid modifying the library files, we used the `BME280 Sparkfun` library instead, which works well with the ESP32-CAM. To install it, please follow these typical steps:
    
* Go to `Sketch -> Include Library -> Manage Libraries`
* Search for “**SparkFun BME280**”.
* Install the library. Current version is 2.0.9.

2. **OneWire Library (by Jim Studt et al.)**

The DS18B20 temperature sensor work with the OneWire library.

* Go to `Sketch -> Include Library -> Manage Libraries`.
* Search for “**OneWire**”.
* Install the library. Current version is 2.3.5

3. **DallasTemperature Library (by Miles Burton et al.)**

The DS18B20 temperature sensor also works with the DallasTemperature library.

* Go to `Sketch -> Include Library -> Manage Libraries`.
* Search for “**DallasTemperature**”.
* Install the library. Current version is 3.9.0

4. **ArduinoJson Library (by Benoit Blanchon)**

This is the JSON parser for Arduino.

* Go to `Sketch -> Include Library -> Manage Libraries`.
* Search for “**ArduinoJson**”.
* Install the library. Current version is 6.18.0

5. **Adafruit NeoPixel (by Adafruit)**

The NeoPixel LED controller library.

* Go to `Sketch -> Include Library -> Manage Libraries`.
* Search for “**Adafruit NeoPixel**”.
* Install the library. Current version is 1.8.0

6. **DS3231 (by Andrew Wickert et al.)**

The DS3231 sensor library.

* Go to `Sketch -> Include Library -> Manage Libraries`.
* Search for “**DS3231**”.
* Install the library. Current version is 1.0.7

And we are ready, now close the Arduino IDE and open it again to be prepared for the real action. 

### Blink is the new 'Hello World!', an ESP32 approach 

If you are confident enough that everything is right, you can skip this subsection and go to the real world. Anyway, `blink` is always so cute that you can give it a try.

0. Connect the ESP32-cam programmer to one of the USB computer ports.
1. Open you Arduino Ide and then press CTRL-N (or `Menu -> File -> New`)
2. Copy and paste this basic blink code in the editor window:
```ino
/*
ESP32 Blink
  Turns an LED on for hald a second, then off for half a second
  , repeatedly. ESP32 builtin led is red, it is located in the
  lower face of the board, and it is connected to GPIO33.
  
  This example was prepared by @asoreyh and is based on the 
  arduino blink example code:
  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/
void setup() {
  pinMode(33, OUTPUT);
}
void loop() {
  digitalWrite(33, HIGH);
  delay(500);
  digitalWrite(33, LOW);
  delay(500);
}
```
Then, save it as `esp32-blink.ino` or something like that. Compile it by clicking on the check mark icon, but still does not upload it to the board. Check if no error messages appears after the compilers end.

2. In order to accept a new firmware, the ESP32 should be in the programming mode. To do that, shortcut `GPIO0` with `GND` (see the green wire the image above).

3. Press the ESP32 reset button, that is conveniently(!) located in the lower face(!?!) of the ESP32-CAM. I would like to thank the ESP32 designers for that. You can use a non-metallic stick to push it. You can also use a metallic stick, but be prepared for a shortcut that surely will burnout your board when you touch something you shouldn't with you metallic bin in a powered board. It's your choice, Neo.

4. We are ready. Now, you can transfer the firmware to the ESP32. If everything was fine, you will see the transferring process and at the end:
   ```
   Leaving... 
   Hard resetting via RTS pin...
   ```

5. Remove the `GPIO0` shortcut and press again (yes, again) the RESET button of the ESP32 (alternatively, you could unplug and plug again the USB)

If everything was right, you will see an hypnotic red blinking light below the ESP32. **Hint: try in a dark room to enjoy the complete nerdiness experience**.

## Firmware

Wait, before to continue, please be sure a MicroSD card is properly plugged in the corresponding slot in the ESP32. While the MicroSD capacity does not really matter, please be sure it is formatted using FAT32.

Okay, after all this fun, we are ready to work in the real world of the top level research. 

Connect the ESP32 to the USB port of your PC and run the ARDUINO IDE. Then, go to `Menu -> File -> Open` and open the [plombox.ino](plombox/plombox.ino) firmware.

Just to see what is happening, you should consider opening the ARDUINO Serial Monitor (press `CTRL+SHIFT+M` of go to `Menu -> Tools -> Serial Monitor`) and set the transfer speed to `115200` bauds. 

If you follow all the above steps, then everything should be ready for compile and upload the firmware to the ESP32 board. 

To do so, put the ESP32 in the programming mode (`GPIO0 -> GND`), press the `RESET` button, and click the `Upload` (the arrow pointing to the right) icon. Once the upload process ends, remove the `GPIO0->GND` shortcut and press again the ESP32 RESET button. 

After a short time you should see in the serial monitor something like this:

```
rst:0x1 (POWERON_RESET),boot:0x13 (SPI_FAST_FLASH_BOOT)
configsip: 0, SPIWP:0xee
clk_drv:0x00,q_drv:0x00,d_drv:0x00,cs0_drv:0x00,hd_drv:0x00,wp_drv:0x00
mode:DIO, clock div:1
load:0x3fff0018,len:4
load:0x3fff001c,len:1216
ho 0 tail 12 room 4
load:0x40078000,len:10944
load:0x40080400,len:6388
entry 0x400806b4

----------------------------------------
--    Starting the Plombox device     --
--    Firmware version is v1r42       --
----------------------------------------

Initializing SD card...        OK
Time setting...                OK
Initializing CAM module...     OK
Initializing DS18B20 sensor... OK
The system time is (UTC): Sat May  1 21:03:21 2021
```

Now, close the Arduino IDE (actually, theretically speaking, you only need to close the Serial Monitor instead of the whole IDE, but...), open a terminal, go to the [python](plombox/python) directory and type:

```bash
$ ./PlomBOXusb.py /dev/ttyUSB0
```
You should change `/dev/ttyUSB0` by the corresponding serial port where your device is connected. To find it out, you can run `sudo dmesg | grep -i usb` once the device is connected to your computer, and look for something like this: `FTDI USB Serial Device converter now attached to ttyUSB0` (of course it will strongly depend on your system, but you will find out). 

Once started and after some LED fireworks, you will find a beautiful timelapse with several images and json files in your current directory.

**And that's it, you did it**. Now you know your parents were actually right, and you are a real genius.

---
Written by @asoreyh on May 15th, 2021. It is based on a previous version written by @lharnaldi.
