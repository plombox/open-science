# ESP32-cam PlomBox firmware <img align="right" width="100" height="100" src="https://plombox.org/images/pbox400.jpg">

This is the last stable version of the [PlomBOX](https://plombox.org/) firmware for the ESP-32 cam processor working in the PlomBOX device. 

## Resources

### Installing

For detailed instructions please visit the [installation guide](docs/INSTALL.md)

### Usage

Once you have properly installed the firmware in your ESP32-cam device, take a look to the python example scripts located in the [python](plombox/python) directory. You can also try to build your own codes by just importing the `plomboxtalk` class: 

```python
import plomboxtalk as Pb
```

For example, the [PlomBOXusb.py](plombox/python/PlomBOXusb.py) performs a short time lapse and transfer and stores the json packages and the images in your computer. You will see a lot of fun in your terminal (and in your PlomBox device if you have access to the LEDs). Once it finishes, you will have several files in your working directory. You can see a json package example in this file:  [295F76334FC4_21_05_15_17_35_40.json](docs/295F76334FC4_21_05_15_17_35_40.json), corresponding to this beautiful image acquisition of the Martian atmosphere taken with my own PlomBox device:
![295F76334FC4_21_05_15_17_35_40.jpg](docs/295F76334FC4_21_05_15_17_35_40.jpg)

### Serial json server injection

While the official way to inject json packages to the server is by using the [PlomApp](https://gitlab.com/plombox/gcrf_android) Android App developed mainly by @AdrianaDias, there are also a way to inject json pacakges directly from your console prepared by @Deisting.

If you properly followed all the [installation steps](docs/INSTALL.md), and then you have installed your firmware and run, e.g., the [PlomBOXusb.py](plombox/python/PlomBOXusb.py), you will have in your working directory several json files and jpeg images. 

You can upload them directly to the server by using the scripts located in the bin directory of the [develop-server2](https://gitlab.com/plombox/plombox_acq/-/tree/develop-server2/bin) branch of the [plombox-acq](https://gitlab.com/plombox/plombox_acq) project. 

You can clone the whole project and locally checkout the corresponding branch in the remote:
```bash
$ git clone git@gitlab.com:plombox/plombox_acq.git 
$ git checkout -b develop-server2 origin/develop-server2
```
This last step is needed since when cloning you only transfer the default branch, usually the `master` branch. To see all the local and remote branches: `git branch -a`. Then, just to check:
and you should see something like: 
```bash
$ git branch
* develop-server2
  master
```
Now, go to the `bin` directory and run this command: 
```bash
$ cd bin
$ python3 sendtest_phone.py -m 2 -nG -d /path-to-your-images/pic.jpg
Connected with result code 0
Published, message id 2
Connected with result code 0
Published, message id 2
```
It tries two times (`-m 2`). After a couple of minutes, your picture will appear also in the [PlomBOX browser](https://plombox.org/browser/) (behind the standard PlomBox credentials).

Perhaps when you run the injector some complains about some missing modules. You probably will need to install the `paho-mqtt` and `geocoder` modules. In [Anaconda](https://www.anaconda.com/), just run: 
```bash
(base) $ conda install -c conda-forge paho-mqtt
(base) $ conda install -c conda-forge geocoder
```
Or, if you are using `pip3`, then 
```bash
$ pip3 install paho-mqtt
$ pip3 install geocoder
```
You can also find the best way to do that in your OS.

### Troubleshooting

If you have any doubts or issue please don't hesitate to contact the contributors.

## Contributors

Main contributors of the project: @lharnaldi,  @bertou, @ffavela, @jaimeguerra.

---
This document was written by @asoreyh.
