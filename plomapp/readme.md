## PlomApp

To use the PlomApp, download the `plomapp_v2.apk` file onto a mobile phone device. 

Follow the instructions shown in `PlomApp_User_Guide_2024.pdf` to use the PlomApp in conjunction with the PlomBOX. Please ensure the PlomBOX is connected to a power source before attempting to use it with the PlomApp.